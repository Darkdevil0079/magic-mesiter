using UnityEngine;
using UnityEditor;
using System.IO;
using PixelCrushers.DialogueSystem.Editors;

namespace PixelCrushers.DialogueSystem.NGUI {
	
	/// <summary>
	/// This class defines the NGUI menu items in the Dialogue System menu.
	/// </summary>
	static public class NGUIMenuItems {
		
		[MenuItem("Window/Dialogue System/Component/UI/NGUI/Dialogue UI", false, 100)]
		public static void AddComponentNGUIDialogueUI() {
			DialogueSystemMenuItems.AddComponentToSelection<NGUIDialogueUI>();
		}
		
		[MenuItem("Window/Dialogue System/Component/UI/NGUI/Response Button", false, 101)]
		public static void AddComponentNGUIResponseButton() {
			DialogueSystemMenuItems.AddComponentToSelection<NGUIResponseButton>();
		}
		
		[MenuItem("Window/Dialogue System/Component/UI/NGUI/Bark Root", false, 102)]
		public static void AddComponentUIBarkRoot() {
			DialogueSystemMenuItems.AddComponentToSelection<UIBarkRoot>();
		}
		
		[MenuItem("Window/Dialogue System/Component/UI/NGUI/Bark UI", false, 103)]
		public static void AddComponentNGUIBarkUI() {
			DialogueSystemMenuItems.AddComponentToSelection<NGUIBarkUI>();
		}
		
		[MenuItem("Window/Dialogue System/Component/UI/NGUI/Text Field UI", false, 104)]
		public static void AddComponentNGUITextFieldUI() {
			DialogueSystemMenuItems.AddComponentToSelection<NGUITextFieldUI>();
		}
		
	}
		
}
