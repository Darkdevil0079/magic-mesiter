﻿using UnityEngine;
using System.Collections;

namespace PixelCrushers.DialogueSystem.NGUI {

	/// <summary>
	/// NGUI UIRoot wrapper for AbstractUIRoot.
	/// </summary>
	[System.Serializable]
	public class NGUIUIRoot : AbstractUIRoot {
		
		private UIRoot uiRoot;
		
		public NGUIUIRoot(UIRoot uiRoot) {
			this.uiRoot = uiRoot;
		}
		
		/// <summary>
		/// Shows the root.
		/// </summary>
		public override void Show() {
			if (uiRoot != null) uiRoot.gameObject.SetActive(true);
		}
		
		/// <summary>
		/// Hides the root.
		/// </summary>
		public override void Hide() {
			if (uiRoot != null) uiRoot.gameObject.SetActive(false);
		}
		
	}

}
