/*
This folder contains NGUI 3.x integration files for the Dialogue System.

The sub-folders are:

- Editor: Adds menu items to Window > Dialogue System > Component > UI > NGUI.
- Example: An example scene using an NGUI bark UI and dialogue UI.
- NGUI Bark UI: Implements a bark UI using NGUI.
- NGUI Dialogue UI: Implements the dialogue UI system using NGUI.
- Prefabs: Various dialogue UIs built using the NGUI Dialogue UI system.


NOTE: If your project uses NGUI 2.x, please import the NGUI2 Support package, not this one.

See the documentation for more information.
*/