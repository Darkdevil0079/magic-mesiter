﻿using UnityEngine;
using System.Collections;

public class InputManager:MonoBehaviour{
	
	private static readonly InputManager _instance = new InputManager();
	public static InputManager Instance { get { return _instance; } }
	public Texture theImage;
	static InputManager(){}

	private InputManager() {}

	public enum Controllers {XBOX360,XBOX360Mac, PS3, PS3Mac, MouseKeyboard};
	public static Controllers Controller=Controllers.MouseKeyboard;
	public static KeyCode A, B, X, Y, Start, Select, R1, L1, L3, R3;
	public const KeyCode Up=KeyCode.W, Down=KeyCode.S, Left=KeyCode.A, Right=KeyCode.D;
	public static string MoveXAxis,MoveYAxis,LookXAxis,LookYAxis,LookZoomAxis;
	private static bool LeftKeyDown,RightKeyDown,UpKeyDown,DownKeyDown;

	public static bool chooseController=false;

	public static void changeController (Controllers newController){
		chooseController = false;
		Controller = newController;
		switch (newController){
			case Controllers.XBOX360:
				A = KeyCode.JoystickButton0; //360 A, PS3 X
				B = KeyCode.JoystickButton1; //360 B, PS3 O
				X = KeyCode.JoystickButton2; //360 X, PS3 Square
				Y = KeyCode.JoystickButton3; //360 X, PS3 Triangle
				L1 = KeyCode.JoystickButton4;
				R1 = KeyCode.JoystickButton5;
				L3 = KeyCode.JoystickButton8;
				R3 = KeyCode.JoystickButton9;
				Start = KeyCode.JoystickButton7;
				Select = KeyCode.JoystickButton6;
				MoveXAxis = "XBOX360 Left Stick X";
				MoveYAxis = "XBOX360 Left Stick Y";
				LookXAxis = "XBOX360 Right Stick X";
				LookYAxis = "XBOX360 Right Stick Y";
			LookZoomAxis = "Mouse ScrollWheel";
			break;
		case Controllers.XBOX360Mac:
			A = KeyCode.JoystickButton16; //360 A, PS3 X
			B = KeyCode.JoystickButton17; //360 B, PS3 O
			X = KeyCode.JoystickButton18; //360 X, PS3 Square
			Y = KeyCode.JoystickButton19; //360 X, PS3 Triangle
			L1 = KeyCode.JoystickButton13;
			R1 = KeyCode.JoystickButton14;
			L3 = KeyCode.JoystickButton11;
			R3 = KeyCode.JoystickButton12;
			Start = KeyCode.JoystickButton9;
			Select = KeyCode.JoystickButton10;
			MoveXAxis = "XBOX360 Left Stick X";
			MoveYAxis = "XBOX360 Left Stick Y";
			LookXAxis = "XBOX360 Mac Right Stick X";
			LookYAxis = "XBOX360 Mac Right Stick Y";
			LookZoomAxis = "Mouse ScrollWheel";
			break;
			case Controllers.PS3Mac:
				A = KeyCode.JoystickButton14; //360 A, PS3 X
				B = KeyCode.JoystickButton13; //360 B, PS3 O
				X = KeyCode.JoystickButton15; //360 X, PS3 Square
				Y = KeyCode.JoystickButton12; //360 X, PS3 Triangle
				L1 = KeyCode.JoystickButton10;
				R1 = KeyCode.JoystickButton11;
				L3 = KeyCode.JoystickButton1;
				R3 = KeyCode.JoystickButton2;
				Start = KeyCode.JoystickButton3;
				Select = KeyCode.JoystickButton0;
				MoveXAxis = "PS3 Left Stick X";
				MoveYAxis = "PS3 Left Stick Y";
				LookXAxis = "PS3 Mac Right Stick X";
				LookYAxis = "PS3 Mac Right Stick Y";
			LookZoomAxis = "Mouse ScrollWheel";
			break;
		case Controllers.PS3:
			A = KeyCode.JoystickButton2; //360 A, PS3 X
			B = KeyCode.JoystickButton1; //360 B, PS3 O
			X = KeyCode.JoystickButton3; //360 X, PS3 Square
			Y = KeyCode.JoystickButton0; //360 X, PS3 Triangle
			L1 = KeyCode.JoystickButton4;
			R1 = KeyCode.JoystickButton5;
			L3 = KeyCode.JoystickButton10;
			R3 = KeyCode.JoystickButton11;
			Start = KeyCode.JoystickButton9;
			Select = KeyCode.JoystickButton8;
			MoveXAxis = "PS3 Left Stick X";
			MoveYAxis = "PS3 Left Stick Y";
			LookXAxis = "PS3 Right Stick X";
			LookYAxis = "PS3 Right Stick Y";
			LookZoomAxis = "Mouse ScrollWheel";
			break;
		case Controllers.MouseKeyboard:
			A = KeyCode.Space; //360 A, PS3 X
			B = KeyCode.Escape; //360 B, PS3 O
			X = KeyCode.Alpha1; //360 X, PS3 Square
			Y = KeyCode.Alpha2; //360 X, PS3 Triangle
			L1 = KeyCode.Tab;
			R1 = KeyCode.LeftCommand;
			L3 = KeyCode.LeftShift;
			R3 = KeyCode.Mouse2;

				Start = KeyCode.Return;
				Select = KeyCode.LeftAlt;
					MoveXAxis = "PS3 Left Stick X";
					MoveYAxis = "PS3 Left Stick Y";
				LookXAxis = "Mouse X";
				LookYAxis = "Mouse Y";
				LookZoomAxis = "Mouse ScrollWheel";
			//Screen.showCursor=false;
			break;
		}
	}
	public static bool GetKey(KeyCode whichButton){
				if (!chooseController) {
						if (Controller != Controllers.MouseKeyboard && whichButton == Up) {
								return (GetAxis (MoveYAxis) > 0.7);
						} else if (Controller != Controllers.MouseKeyboard && whichButton == Down) {
								return (GetAxis (MoveYAxis) < -0.7);
						} else if (Controller != Controllers.MouseKeyboard && whichButton == Left) {
								return (GetAxis (MoveXAxis) < -0.7);
						} else if (Controller != Controllers.MouseKeyboard && whichButton == Right) {
								return (GetAxis (MoveXAxis) > 0.7);
						} else {
								return Input.GetKey (whichButton);
						}
		}else{return false;}
	}

	public static bool GetKeyDown(KeyCode whichButton){
				if (!chooseController) {
						if (Controller != Controllers.MouseKeyboard && whichButton == Up) {
								if ((!UpKeyDown) && GetKey (InputManager.Up)) {
										UpKeyDown = true;
										return true;
								} else {
										return false;
								}
						} else if (Controller != Controllers.MouseKeyboard && whichButton == Down) {
								if ((!DownKeyDown) && GetKey (InputManager.Down)) {
										DownKeyDown = true;
										return true;
								} else {
										return false;
								}
						} else if (Controller != Controllers.MouseKeyboard && whichButton == Left) {
								if ((!LeftKeyDown) && GetKey (InputManager.Left)) {
										LeftKeyDown = true;
										return true;
								} else {
										return false;
								}
						} else if (Controller != Controllers.MouseKeyboard && whichButton == Right) {
								if ((!RightKeyDown) && GetKey (InputManager.Right)) {
										RightKeyDown = true;
										return true;
								} else {
										return false;
								}
						} else {
								return Input.GetKeyDown (whichButton);
						}
		}else{return false;}
	}
	public static bool GetKeyUp(KeyCode whichButton){
				if (!chooseController) {
						return Input.GetKeyUp (whichButton);
		}else{return false;}
	}



	public static float GetAxis(string whichAxis){
				if (!chooseController) {
						if (Controller == Controllers.MouseKeyboard && whichAxis == MoveXAxis) { //WASD
				return (Input.GetKey (InputManager.Left) ? -1f : 0f) + (Input.GetKey (InputManager.Right) ? 1f : 0f);
						} else if (Controller == Controllers.MouseKeyboard && whichAxis == MoveYAxis) {
				return (Input.GetKey (InputManager.Down) ? -1f : 0f) + (Input.GetKey (InputManager.Up) ? 1f : 0f);
						} /*else if (Controller == Controllers.MouseKeyboard && whichAxis == LookXAxis) {
				return (Input.GetKey (KeyCode.LeftArrow) ? -1f : 0f) + (Input.GetKey (KeyCode.RightArrow) ? 1f : 0f);
						} else if (Controller == Controllers.MouseKeyboard && whichAxis == LookYAxis) {
				return (Input.GetKey (KeyCode.DownArrow) ? -1f : 0f) + (Input.GetKey (KeyCode.UpArrow) ? 1f : 0f);
						} */else {
								return Mathf.Clamp(Input.GetAxis (whichAxis),-1,1);
						}
				}
		else{return 0;}
	}

	void Awake(){
			changeController (Controllers.MouseKeyboard);
	}

	void Update(){
		if (Mathf.Abs (GetAxis (MoveXAxis)) < 0.4) {
			LeftKeyDown = false;
			RightKeyDown = false;
		}
		if (Mathf.Abs (GetAxis (MoveYAxis)) < 0.4) {
			UpKeyDown = false;
			DownKeyDown = false;
		}
		if (chooseController) {

			if (Input.GetKeyDown (KeyCode.JoystickButton14)) {
							changeController (Controllers.PS3Mac);//mac						
			} else if (Input.GetKeyDown (KeyCode.JoystickButton0)) {
							changeController (Controllers.XBOX360);
						}
			else if (Input.GetKeyDown (KeyCode.JoystickButton2)) {
							changeController (Controllers.PS3);
						} 
			else if (Input.GetKeyDown (KeyCode.JoystickButton16)) {
							changeController (Controllers.XBOX360Mac);
						} 
			else if (Input.GetKeyDown (KeyCode.Return)) {
								changeController (Controllers.MouseKeyboard);
						}
		} else {
				guiText.text = "";

	}

	}
	void OnGUI(){
		if (chooseController) {
			GUI.Box (new Rect (Screen.width/2-250, Screen.height/2-150, 500, 300),theImage);
		}
	}
	KeyCode FetchKey()
		{
				int e = System.Enum.GetNames (typeof(KeyCode)).Length;
				for (int i = 0; i < e; i++) {
						if (Input.GetKey ((KeyCode)i)) {
								return (KeyCode)i;
						}
				}

				return KeyCode.None;
		}
}
