﻿using UnityEngine;
using System.Collections;

public class ServerFoundGame : MonoBehaviour
{

    public UILabel Creator;
    public UILabel players;
    public UISprite ElementIcon;
    public string game;

    public void CreatorElement(int elType)
    {
        switch (elType)
        {
            case 0:
                ElementIcon.spriteName = "tab_emb";
                break;
            case 1:
                ElementIcon.spriteName = "tab_fro";
                break;
            case 2:
                ElementIcon.spriteName = "tab_vale";
                break;
            case 3:
                ElementIcon.spriteName = "tab_sha";
                break;
            case 4:
                ElementIcon.spriteName = "tab_spi";
                break;
            default:
                 ElementIcon.spriteName = "";
                break;
        }


    }

    public void SelectGame()
    {
        ServerLobby.Instance.SelectedRoom = game;
    }

}
