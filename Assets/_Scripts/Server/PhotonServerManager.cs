﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PhotonServerManager : Photon.MonoBehaviour
{

    public static PhotonServerManager Instance;
    public bool HasJoinLobby;
    public bool InARoom;
    public bool OfflineGame = false;
    public string CurrentState;

    public GameObject ConnectingMessage;

    private GameObject Message;

    void Awake()
    {
        DontDestroyOnLoad(this);
        Application.runInBackground = true;
        Instance = this;
    }

    public void ConnectToServer()
    {
        if (!PhotonServerManager.Instance.OfflineGame)
        {
            PhotonNetwork.ConnectUsingSettings(this.GetComponent<GameSettings>().GameVersionID);
        }
    }

    void Update()
    {
        if (PhotonNetwork.connected)
        {
            CurrentState = PhotonNetwork.networkingPeer.State.ToString();    
        }
        else
            ConnectToServer();
    }

    void OnConnectedToMaster()
    {
        Debug.Log("Connected To Master");
    }

    void OnConnectedToPhoton()
    {
    }

    void OnJoinedLobby()
    {
        HasJoinLobby = true;
    }

    void OnReceivedRoomListUpdate()
    {
    }

    void OnPhotonCreateGameFailed()
    {
        Debug.Log("Room Failed To Create");
    }

    void  OnPhotonJoinRoomFailed()
    {
        Debug.Log("Join Room Failed");
    }

    public void ShowConnectingMessage()
    {
        GameObject uiRoot = GameObject.FindObjectOfType<UIRoot>().gameObject as GameObject;
        Message = NGUITools.AddChild(uiRoot, ConnectingMessage);
    }

    public void CreateRoom()
    {
        PhotonNetwork.offlineMode = false;
        RoomOptions ro = new RoomOptions();
        ro.maxPlayers = 4;
        ro.isOpen = true;
        ro.isVisible = true;

        PhotonNetwork.CreateRoom(PlayerDataStorage.Instance.PlayerName, ro, TypedLobby.Default);

    }

    public void JoinSelectedGame()
    {
        PhotonNetwork.offlineMode = false;
        PhotonNetwork.JoinRoom(ServerLobby.Instance.SelectedRoom);
    }

    internal void SetServerOffline()
    {
        if (PhotonNetwork.connected)
        {
            PhotonNetwork.Disconnect();
        }

        OfflineGame = true;
        PhotonNetwork.offlineMode = true;
    }
}
