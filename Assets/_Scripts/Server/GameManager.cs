﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class GameManager : Photon.MonoBehaviour
{

    public static GameManager Instance;
    public static MeisterController LocalPlayer;
    public KGFMapSystem MiniMap;
    public MeisterUI LocalUI;
    public UITextList LocalChatList;
    public GameObject ChatWindow;
    public bool HasGameStarted;
    public GameObject BackToMainMenu;
    public UIPanel NPCSelector;
    public UIRoot MainUI;

    private string playerPrefabName = "Meister";

    void Awake()
    {

        if (PlayerDataStorage.Instance == null)
            Application.LoadLevel(GameSettings.MainMenu);

        Instance = this;
        HasGameStarted = false;
        CloseExitMenu();
        NPCSelector.gameObject.SetActive(false);
    }

    void Start()
    {
    }

    // this is a object name (must be in any Resources folder) of the prefab to spawn as player avatar.
    // read the documentation for info how to spawn dynamically loaded game objects at runtime (not using Resources folders)

    void OnJoinedRoom()
    {
        HasGameStarted = true;
        StartGame();
    }

    void Update()
    {
        if (!HasGameStarted)
        {
            if (PhotonServerManager.Instance.OfflineGame)
            {
                HasGameStarted = true;
                StartGameOffline();
            }

            if (LocalPlayer != null)
            {
                MiniMap.SetTarget(LocalPlayer.Player.gameObject);
            }


        }


        if (Input.GetKeyDown(KeyCode.Escape))
        {
            BackToMainMenu.SetActive(true);
        }
    }

    IEnumerator OnLeftRoom()
    {
        //Easy way to reset the level: Otherwise we'd manually reset the camera

        //Wait untill Photon is properly disconnected (empty room, and connected back to main server)
        while (PhotonNetwork.room != null || PhotonNetwork.connected == false)
            yield return 0;

        Application.LoadLevel(GameSettings.MainMenu);

    }

    void StartGame()
    {

        PlayerItemEquiped hat = PlayerDataStorage.Instance.WearingEquipment.Where(w => w.Slot == InvBaseItem.Slot.Hat).FirstOrDefault();
        PlayerItemEquiped cloths = PlayerDataStorage.Instance.WearingEquipment.Where(w => w.Slot == InvBaseItem.Slot.Clothes).FirstOrDefault();
        PlayerItemEquiped gloves = PlayerDataStorage.Instance.WearingEquipment.Where(w => w.Slot == InvBaseItem.Slot.Gloves).FirstOrDefault();
        PlayerItemEquiped boots = PlayerDataStorage.Instance.WearingEquipment.Where(w => w.Slot == InvBaseItem.Slot.Boots).FirstOrDefault();
        PlayerItemEquiped extra = PlayerDataStorage.Instance.WearingEquipment.Where(w => w.Slot == InvBaseItem.Slot.Extra).FirstOrDefault();
        PlayerItemEquiped wep = PlayerDataStorage.Instance.WearingEquipment.Where(w => w.Slot == InvBaseItem.Slot.Weapon).FirstOrDefault();

        object[] objArray = new object[] {
                                          PlayerDataStorage.Instance.Gender,
                                          PlayerDataStorage.Instance.WearingHat,
                                          PlayerDataStorage.Instance.SkinColourIndex,
                                          PlayerDataStorage.Instance.EyeStyleIndex,
                                          PlayerDataStorage.Instance.EyeColorIndex,
                                          PlayerDataStorage.Instance.HairStyleIndex, 
                                          PlayerDataStorage.Instance.HairColorIndex,
                                          hat != null ? hat.ItemID : -1,
                                          cloths != null ? cloths.ItemID : -1,
                                          gloves != null ? gloves.ItemID : -1,
                                          boots != null ? boots.ItemID : -1,
                                          extra != null ? extra.ItemID : -1,
                                          wep != null ? wep.ItemID : -1
                                          };


        LocalPlayer = PhotonNetwork.Instantiate(this.playerPrefabName, transform.position, Quaternion.identity, 0, objArray).GetComponent<MeisterController>();

    }

    void StartGameOffline()
    {
        LocalPlayer = (Instantiate(Resources.Load(this.playerPrefabName), transform.position, Quaternion.identity) as GameObject).GetComponent<MeisterController>();
        ChatWindow.gameObject.SetActive(false);
    }

    void OnDisconnectedFromPhoton()
    {
        Debug.LogWarning("OnDisconnectedFromPhoton");
    }

    public void CloseExitMenu()
    {
        BackToMainMenu.SetActive(false);
    }

    public void GotoMainMenu()
    {
        Application.LoadLevel(GameSettings.MainMenu);
    }

}
