﻿using UnityEngine;
using System.Collections;

public class ServerLobby : Photon.MonoBehaviour
{

    public static ServerLobby Instance;

    public UIGrid FoundGameGrid;
    public UISprite ConnectionIcon;

    public string SelectedRoom;


    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (!PhotonNetwork.connected)
            ConnectionIcon.color = Color.red;
        else
            ConnectionIcon.color = Color.green;
    }

    public void UpdateRoomListings()
    {
        RefreshRoomList();
    }

    private void RefreshRoomList()
    {
        foreach (ServerFoundGame room in FoundGameGrid.GetComponentsInChildren<ServerFoundGame>())
        {
            Destroy(room.gameObject);
        }
        FoundGameGrid.Reposition();

        foreach (RoomInfo game in PhotonNetwork.GetRoomList())
        {
            GameObject room = NGUITools.AddChild(FoundGameGrid.gameObject, Resources.Load("UI/GameFound") as GameObject);
            ServerFoundGame serverFoundGame = room.GetComponent<ServerFoundGame>();
            serverFoundGame.Creator.text = "World Created By : " + game.name;
            serverFoundGame.players.text = game.playerCount.ToString() + "/4";
            serverFoundGame.game = game.name;
            serverFoundGame.CreatorElement(Random.Range(0,6));
            FoundGameGrid.Reposition();
        }

        FoundGameGrid.Reposition();
    }

}
