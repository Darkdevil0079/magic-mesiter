﻿using UnityEngine;
using System.Collections;

public class ServerChat : Photon.MonoBehaviour {

    public Color NormalText;
    public Color AltText;

    private float NextInactiveTimer;
    private bool AltColor;

    public string chatInput;
    
	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}

    [RPC]
    public void SendChatMessage(string text, PhotonMessageInfo info)
    {
        AddMessage(text, info.sender.name , true);
    }

    [RPC]
    public void SendChatMessageBlank(string text, PhotonMessageInfo info)
    {
        AddMessage(text, info.sender.name , false);
    }

    public void SendChat(PhotonTargets target)
    {
        if (chatInput != "")
        {
            if (chatInput.Contains("/"))
                GameManager.LocalPlayer.NetworkController.SendEmoteRequest(chatInput);
            else
                photonView.RPC("SendChatMessage", target, chatInput);
            
            chatInput = "";
        }
    }

    public void SendChat(PhotonPlayer target)
    {
        if (chatInput != "")
        {
            chatInput = "[PM] " + chatInput;
            photonView.RPC("SendChatMessage", target, chatInput);
            chatInput = "";
        }
    }

    public void AddMessage(string chatMessage, string playerName , bool showPlayerName)
    {
        string TextColor;

        if (AltColor)
        {
            TextColor = "[" + NGUIText.EncodeColor(NormalText) + "]";
        }
        else
        {
            TextColor = "[" + NGUIText.EncodeColor(AltText) + "]";
        }

        GameManager.Instance.LocalChatList.Add(TextColor + (showPlayerName == true ? "[" + playerName + "]" : "") + chatMessage);

        AltColor = !AltColor;
        GameManager.LocalPlayer.IsLocked = false;
        
    }
}
