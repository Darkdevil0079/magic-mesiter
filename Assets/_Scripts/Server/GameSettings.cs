using UnityEngine;
using System.Collections;

public class GameSettings : MonoBehaviour
{
		public static GameSettings Instance;

        public string GameVersionID = "v0.812";
		public int LevelXPAmount = 2;
        public float WanderRandomAmout = 60f;

        public LayerMask RemoteLayer;
        public static string MainMenu = "MainMenu";
        public static string PlayerCreator = "Creator";
        public static string TestingLevel = "TestingLevel";

		void Awake ()
		{
				Instance = this;
		}
}

