﻿using UnityEngine;
using System.Collections;

public class QuestItem : MonoBehaviour {

    public int QuestIndex;
    public QuestLogSystem parentSystem;

       public void OnPress(bool isDown)
    {
        if (isDown)
        {
            parentSystem.ShowCurrentSelectedQuest(QuestIndex);
        }
    }
}
