﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class MeisterEquipment : MonoBehaviour {

    public void UpdateSlotItem(PlayerItemEquiped item)
    {
        InvBaseItem baseitem = InvDatabase.FindByID(item.ItemID);
        
        if (item.Slot != InvBaseItem.Slot.None)
        {
            PlayerItemEquiped Found;
            Found = PlayerDataStorage.Instance.WearingEquipment.Find(lookup => lookup.Slot == item.Slot);

            if (Found == null)
            {
                PlayerDataStorage.Instance.WearingEquipment.Add(item);
            }
            else
            {

                if (item != Found)
                {
                    Found.ItemID = item.ItemID;
                    Found.Grade = item.Grade;
                    Found.Level = item.Level;
                }
                else
                {
                    PlayerDataStorage.Instance.WearingEquipment.Remove(Found);
                }
            }

            if (!PhotonServerManager.Instance.OfflineGame)
            {
                GameManager.LocalPlayer.NetworkController.SendUpdatePlayerCloths("Base");
            }

        }
        else if (item != null)
        {
            Debug.LogWarning("Can't equip \"" + item.ItemID + "\" because it doesn't specify an item slot");
        }
    }

    internal PlayerItemEquiped GetItemBySlot(InvBaseItem.Slot slot)
    {
        return PlayerDataStorage.Instance.WearingEquipment.Find(lookup => lookup.Slot == slot);
    }

}
