﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class MeisterUI : MonoBehaviour
{
    public static MeisterUI Instance;

    private Vector2 BuffStart = new Vector3(418, 0, 0);

    public List<StatusEffect> StatusIcons;
    public UIProgressBar HealthBar;
    public UILabel HealthBarText;
    public UIProgressBar ManaBar;
    public UILabel ManaBarText;
    public UIProgressBar XpBar;
    public GameObject StatusIconHolder;

    private float NewHealthBarValue;
    private float NewManaBarValue;
    private float NewXPBarValue;

    public MeisterController LinkedPlayer;

    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        StatusIcons = new List<StatusEffect>();
        HealthBar.value = 0;
        ManaBar.value = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if (LinkedPlayer != null )
        {
            if (LinkedPlayer.CurrentHP > 0)
            {
                NewHealthBarValue = (float)LinkedPlayer.CurrentHP / (float)LinkedPlayer.MaxHP;
                NewManaBarValue = (float)LinkedPlayer.CurrentMana / (float)LinkedPlayer.MaxMana;
                NewXPBarValue = (float)PlayerDataStorage.Instance.CurrentXP / (float)Helper.GetLevelXP();

                if (NewHealthBarValue != 1 || NewHealthBarValue != 0)
                    HealthBar.value = Mathf.Lerp(HealthBar.value, NewHealthBarValue, Time.deltaTime * 1.5f);
                else
                    HealthBar.value = NewHealthBarValue;

                HealthBarText.text = LinkedPlayer.CurrentHP + " / " + LinkedPlayer.MaxHP;

                if (NewManaBarValue != 1 || NewManaBarValue != 0)
                    ManaBar.value = Mathf.Lerp(ManaBar.value, NewManaBarValue, Time.deltaTime * 1.5f);
                else
                    ManaBar.value = NewManaBarValue;

                ManaBarText.text = LinkedPlayer.CurrentMana + " / " + LinkedPlayer.MaxMana;

                XpBar.value = Mathf.Lerp(XpBar.value, NewXPBarValue, Time.deltaTime * 1.5f);
            }
        }
    }

    public void WelcomeToTheDemo()
    {

        string[] TextTester = new string[3];

        TextTester[0] = "Welcome to Alpha version of Meister Magic";
        TextTester[1] = "We hope that you enjoy the demo";

        DialogBox.Instance.LoadDialogText(TextTester);

    }

    public void AddStatusIcon(string statusIconName)
    {
        GameObject Icon = NGUITools.AddChild(StatusIconHolder, Resources.Load("UI/StatusIcon") as GameObject);
        Icon.transform.localPosition = BuffStart;

        UISprite NewIcon = Icon.GetComponent<UISprite>();
        NewIcon.spriteName = statusIconName;
        StatusEffect IconEffect = Icon.GetComponent<StatusEffect>();
        IconEffect.EffectLenght = Random.Range(2, 20);
        IconEffect.SetupEffectIcon();

        StatusIcons.Add(IconEffect);
        UpdateStatusLayout();
    }

    public void UpdateStatusLayout()
    {
        int cntID = 1;

        foreach (StatusEffect effect in StatusIcons.OrderBy(o => o.TimeLeft).ToList())
        {
            Vector3 pos = new Vector3(0 + (35 * cntID), 0, 0);
            SpringPosition.Begin(effect.gameObject, pos, 6);
            cntID++;
        }
    }

}
