﻿using UnityEngine;
using System.Collections;

public class PlayerSpirt : MonoBehaviour
{

    private Transform _spirt;
    private Vector3 locIdle = Vector3.zero;

    void Awake()
    {
        _spirt = this.transform;
    }

    // Use this for initialization
    void Start()
    {
        SpirtIdleNewLoc();

        this.GetComponent<AudioSource>().PlayDelayed(2f);

    }

    // Update is called once per frame
    void Update()
    {

        if (Vector3.Distance(_spirt.transform.localPosition , locIdle) < 0.1f)
            SpirtIdleNewLoc();

        _spirt.transform.localPosition = Vector3.Lerp(_spirt.transform.localPosition, locIdle, Time.deltaTime * 2f);

    }

    private void SpirtIdleNewLoc()
    {

        locIdle = new Vector3(Random.Range(1.9f, 2.1f), Random.Range(4f, 6f), Random.Range(0f, .7f));

    }

    
}
