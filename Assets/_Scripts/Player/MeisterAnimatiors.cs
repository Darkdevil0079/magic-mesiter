﻿using UnityEngine;
using System.Collections;

public class MeisterAnimatiors : MonoBehaviour
{

    private const string IDLE = "idle";

    private const string WALKING = "move_walk";
    private const string RUNNNING = "move_run";
    private const string JUMP = "jump";

    private const string DEATH = "death";
    private const string LOSE = "lose";

    private const string TALK1 = "talk1";
    private const string TALK2 = "talk2";
    private const string TALKNO = "talkno";

    private const string ONMOUNTIDLE = "mount_ride_idle_phorus";

    private const string COMBATBLOCKHANDS = "combat_block_hands";
    private const string COMBATBLOCKSTAFF = "combat_block_staff";

    private const string COMBATIDLEHANDS = "combat_idle_hands";
    private const string COMBATWALKHANDS = "combat_walk_hands_forward";
    private const string COMBATSTAFERIGHTHANDS = "combat_strafe_hands_right";
    private const string COMBATSTAFELEFTTHANDS = "combat_strafe_hands_left";

    private const string COMBATCASTAREAHANDS = "combat_cast_hands_area";
    private const string COMBATCASTIMPACTHANDS = "combat_cast_hands_impact";
    private const string COMBATCASTSPINHANDS = "";
    private const string COMBATCASTAOEHANDS = "combat_cast_hands_area";
    private const string COMBATCASTSPOTHANDS = "combat_cast_hands_spot";
    private const string COMBATCASTWAITINGHANDS = "combat_cast_hands_waiting";

    private const string COMBATIDLESTAFF = "combat_idle_staff";
    private const string COMBATWALKSTAFF = "combat_walk_staff_forward";
    private const string COMBATSTAFERIGHTSTAFF = "combat_strafe_staff_right";
    private const string COMBATSTAFELEFTTSTAFF = "combat_strafe_staff_left";

    private const string COMBATCASTAREASTAFF = "combat_cast_staff_area";
    private const string COMBATCASTIMPACTSTAFF = "combat_cast_staff_impact";
    private const string COMBATCASTSPINSTAFF = "combat_cast_staff_spin";
    private const string COMBATCASTAOESTAFF = "combat_cast_staff_aoe";
    private const string COMBATCASTSPOTSTAFF = "combat_cast_staff_spot";
    private const string COMBATCASTWAITINGSTAFF = "combat_cast_staff_waiting";

    private const string COMBATDODGEFORWARD = "combat_dodge_forward";
    private const string COMBATDODGEBACK = "combat_dodge_back";
    private const string COMBATDODGELEFT = "combat_dodge_left";
    private const string COMBATDODGERIGHT = "combat_dodge_right";

    private const string EMOTEWAVE = "idle_wave";


    public AudioClip Walk_SFX;
    public AudioClip Running_SFX;
    public AudioClip Dodge_SFX;
    public AudioClip Impact_SFX;

    public enum Direction
    {
        Stationary,
        Forward,
        Backward,
        Left,
        Right,
        LeftForward,
        RightForward,
        LeftBackward,
        RightBackward
    }

    public MeisterController AttachedController;
    public Animation PlayerAnimatior;
    public Direction MoveDirection;
    public MeisterCharacterState State;
    private MeisterCharacterState laststate { get; set; }
    public bool leftFoot = true;
    public bool LayFootPrints = false;
    public float NextPrint;

    private FloatingProgressBar FPB;

    public bool IsDead { get; set; }

    void Start()
    {
        if (AttachedController == null)
            return;

    }

    void Update()
    {
        if (AttachedController == null)
            return;

        if (!AttachedController.IsRemotePlayer)
            DetermineCurrentState();
        
        ProcessCurrentState();

        if (LayFootPrints)
        {
            DoFootprints();
        }

    }

    public void DetermineCurrentDirection()
    {
        bool forward = false;
        bool right = false;
        bool left = false;
        bool backwards = false;
        if (this.AttachedController.MoveVector.z > 0f)
        {
            forward = true;
        }
        if (this.AttachedController.MoveVector.z < 0f)
        {
            right = true;
        }
        if (this.AttachedController.MoveVector.x > 0f)
        {
            left = true;
        }
        if (this.AttachedController.MoveVector.x < 0f)
        {
            backwards = true;
        }
        if (forward)
        {
            if (left)
            {
                this.MoveDirection = MeisterAnimatiors.Direction.LeftForward;
            }
            else if (!backwards)
            {
                this.MoveDirection = MeisterAnimatiors.Direction.Forward;
            }
            else
            {
                this.MoveDirection = MeisterAnimatiors.Direction.RightForward;
            }
        }
        else if (right)
        {
            if (left)
            {
                this.MoveDirection = MeisterAnimatiors.Direction.LeftBackward;
            }
            else if (!backwards)
            {
                this.MoveDirection = MeisterAnimatiors.Direction.Backward;
            }
            else
            {
                this.MoveDirection = MeisterAnimatiors.Direction.RightBackward;
            }
        }
        else if (left)
        {
            this.MoveDirection = MeisterAnimatiors.Direction.Left;
        }
        else if (!backwards)
        {
            this.MoveDirection = MeisterAnimatiors.Direction.Stationary;
        }
        else
        {
            this.MoveDirection = MeisterAnimatiors.Direction.Right;
        }
    }

    void DetermineCurrentState()
    {
        if (State == MeisterCharacterState.Dead)
        {
            return;
        }

        if (!AttachedController.isGrounded)
        {
            if (State != MeisterCharacterState.Falling
                    && State != MeisterCharacterState.Jumping
                    && State != MeisterCharacterState.Landing)
            {
                // We Should be falling
            }
        }

        if (State != MeisterCharacterState.Falling && State != MeisterCharacterState.Jumping
                && State != MeisterCharacterState.Landing && State != MeisterCharacterState.Casting && State != MeisterCharacterState.ChargeCasting
                && State != MeisterCharacterState.Blocking && State != MeisterCharacterState.Swimming && State != MeisterCharacterState.Dodging
                && State != MeisterCharacterState.Emote && State != MeisterCharacterState.Talking)
        {


            switch (this.MoveDirection)
            {
                case MeisterAnimatiors.Direction.Stationary:
                    {
                        this.State = MeisterCharacterState.Idle;
                        break;
                    }
                case MeisterAnimatiors.Direction.Forward:
                    {
                        this.State = MeisterCharacterState.Walking;
                        break;
                    }
                case MeisterAnimatiors.Direction.Backward:
                    {
                        this.State = MeisterCharacterState.WalkingBackwards;
                        break;
                    }
                case MeisterAnimatiors.Direction.Left:
                    {
                        this.State = MeisterCharacterState.StrafingLeft;
                        break;
                    }
                case MeisterAnimatiors.Direction.Right:
                    {
                        this.State = MeisterCharacterState.StrafingRight;
                        break;
                    }
                case MeisterAnimatiors.Direction.LeftForward:
                    {
                        this.State = MeisterCharacterState.Walking;
                        break;
                    }
                case MeisterAnimatiors.Direction.RightForward:
                    {
                        this.State = MeisterCharacterState.Walking;
                        break;
                    }
                case MeisterAnimatiors.Direction.LeftBackward:
                    {
                        this.State = MeisterCharacterState.WalkingBackwards;
                        break;
                    }
                case MeisterAnimatiors.Direction.RightBackward:
                    {
                        this.State = MeisterCharacterState.WalkingBackwards;
                        break;
                    }
            }

        }
    }

    public void ProcessCurrentState()
    {
        switch (State)
        {
            case MeisterCharacterState.Idle:
                Idle();
                break;
            case MeisterCharacterState.Walking:
                Walking();
                break;
            case MeisterCharacterState.WalkingBackwards:
                BackWalking();
                break;
            case MeisterCharacterState.Running:
                break;
            case MeisterCharacterState.StrafingLeft:
                if (AttachedController.HasWeapon)
                    PlayerAnimatior.CrossFade(COMBATSTAFERIGHTSTAFF);
                else
                    PlayerAnimatior.CrossFade(COMBATSTAFERIGHTHANDS);
                break;
            case MeisterCharacterState.StrafingRight:
                if (AttachedController.HasWeapon)
                    PlayerAnimatior.CrossFade(COMBATSTAFELEFTTSTAFF);
                else
                    PlayerAnimatior.CrossFade(COMBATSTAFELEFTTHANDS);
                break;
            case MeisterCharacterState.Jumping:
                Jumping();
                break;
            case MeisterCharacterState.Customerizer:
                CastSkill();
                break;
            case MeisterCharacterState.Casting:
                CastSkill();
                break;
            case MeisterCharacterState.ChargeCasting:
                CheckChargeFinished();
                break;
            case MeisterCharacterState.Dodging:
                Dodge();
                break;
            case MeisterCharacterState.Blocking:
                Blocking();
                break;
            case MeisterCharacterState.Emote:
                Emote();
                    break;
            default:
                break;
        }
    }

    private void Blocking()
    {
        if (AttachedController.HasWeapon)
            PlayerAnimatior.CrossFade(COMBATBLOCKSTAFF);
        else
            PlayerAnimatior.CrossFade(COMBATBLOCKHANDS);
    }

    private void CheckChargeFinished()
    {
        if (AttachedController.Skills.IsUsingSkill)
        {
            if (!AttachedController.Skills.IsCastTimer)
            {
                //PlayCorrectCastAnim();
            }
        }
    }

    void Idle()
    {
        LayFootPrints = false;
        AttachedController.AudioPlayer.Stop();

        if (AttachedController.IsCustomizing)
        {
            PlayerAnimatior.Blend(IDLE);
        }
        else if (AttachedController.IsOnMount)
        {
            PlayerAnimatior.CrossFade(ONMOUNTIDLE);
        }
        else if (AttachedController.InCastingMode)
        {
            if (AttachedController.HasWeapon)
                PlayerAnimatior.CrossFade(COMBATIDLESTAFF);
            else
                PlayerAnimatior.CrossFade(COMBATIDLEHANDS);
        }
        else
        {
            if (AttachedController.HasWeapon)
                PlayerAnimatior.CrossFade(IDLE);
            else
                PlayerAnimatior.CrossFade(IDLE);
        }

    }

    void BackWalking()
    {
        if (AttachedController.InCastingMode)
        {
            if (AttachedController.HasWeapon)
                PlayerAnimatior.CrossFade(COMBATWALKSTAFF);
            else
                PlayerAnimatior.CrossFade(COMBATWALKHANDS);
        }
        else
        {
            PlayerAnimatior.CrossFade(WALKING);
        }
    }

    void Walking()
    {
        LayFootPrints = true;

        if (AttachedController.InCastingMode)
        {
            if (AttachedController.HasWeapon)
                PlayerAnimatior.CrossFade(COMBATWALKSTAFF);
            else
                PlayerAnimatior.CrossFade(COMBATWALKHANDS);
        }
        else
        {
            if (AttachedController.ToggleWalking)
            {
                if (!AttachedController.AudioPlayer.isPlaying)
                {
                    AttachedController.AudioPlayer.clip = Walk_SFX;
                    AttachedController.AudioPlayer.Play();
                }
                PlayerAnimatior.CrossFade(WALKING);
            }
            else
            {
                if (!AttachedController.AudioPlayer.isPlaying)
                {
                    AttachedController.AudioPlayer.clip = Running_SFX;
                    AttachedController.AudioPlayer.Play();
                }
                PlayerAnimatior.CrossFade(RUNNNING);
            }

        }
    }

    private void DoFootprints()
    {
        GameObject fprint;
        Vector3 footPos;

        if (Time.time > NextPrint)
        {
            if (AttachedController.ToggleWalking)
                NextPrint = Time.time + .6f;
            else
                NextPrint = Time.time + .2f;

            if (leftFoot)
            {
                footPos = AttachedController.Player.position;
                footPos += Vector3.left;
                leftFoot = false;
            }
            else
            {
                footPos = AttachedController.Player.position;
                footPos += -Vector3.left;
                leftFoot = true;
            }

            fprint = Instantiate(Resources.Load("FootPrint"), footPos, AttachedController.Player.localRotation) as GameObject;
            AttachedController.Skills.PlayerFootPrints.Add(fprint);

        }
    }

    public void Jumping()
    {
        if (!PlayerAnimatior.isPlaying && AttachedController.isGrounded
            || AttachedController.isGrounded)
        {
            State = MeisterCharacterState.Idle;
        }
    }

    public void Jump()
    {
        laststate = State;
        State = MeisterCharacterState.Jumping;
        PlayerAnimatior.CrossFade(JUMP);
    }

    public void Cast( float castTime, 
                      SkillBaseSkill.emCastAnimationType skillAnimationType,
                      SkillBaseSkill.emSkillType skillType
                     )
    {

        if (castTime > 0)
        {
            if (AttachedController.HasWeapon)
                PlayerAnimatior.CrossFade(COMBATCASTWAITINGHANDS);
            else
                PlayerAnimatior.CrossFade(COMBATCASTWAITINGSTAFF);

            AddCastingBar();
            GameObject objCastingEffect = Instantiate(Resources.Load("Skills/CastingEffect")) as GameObject;
            objCastingEffect.transform.parent = AttachedController.Player;
            objCastingEffect.transform.localPosition = new Vector3(0f, 0f, 0f);
            AttachedController.Skills.IsCastTimer = true;
        }
        else
        {
            PlayCorrectCastAnim(skillAnimationType , skillType);
        }

    }

    void PlayCorrectCastAnim(SkillBaseSkill.emCastAnimationType skillAnimationType , SkillBaseSkill.emSkillType skillType)
    {

        State = MeisterCharacterState.Casting;

        switch (skillAnimationType)
        {
            case SkillBaseSkill.emCastAnimationType.Area:
                if (AttachedController.HasWeapon)
                    PlayerAnimatior.CrossFade(COMBATCASTAOESTAFF);
                else
                    PlayerAnimatior.CrossFade(COMBATCASTAOEHANDS);
                break;

            case SkillBaseSkill.emCastAnimationType.Spin:
                if (AttachedController.HasWeapon)
                    PlayerAnimatior.CrossFade(COMBATCASTSPINSTAFF);
                else
                    PlayerAnimatior.CrossFade(COMBATCASTSPINHANDS);
                break;

            case SkillBaseSkill.emCastAnimationType.Impact:
                if (!AttachedController.AudioPlayer.isPlaying)
                {
                    AttachedController.AudioPlayer.clip = Impact_SFX;
                    AttachedController.AudioPlayer.Play();
                }
                if (AttachedController.HasWeapon)
                    PlayerAnimatior.CrossFade(COMBATCASTIMPACTSTAFF);
                else
                    PlayerAnimatior.CrossFade(COMBATCASTIMPACTHANDS);
                break;

            case SkillBaseSkill.emCastAnimationType.Spot:
                if (AttachedController.HasWeapon)
                    PlayerAnimatior.CrossFade(COMBATCASTSPOTSTAFF);
                else
                    PlayerAnimatior.CrossFade(COMBATCASTSPOTHANDS);
                break;

            case SkillBaseSkill.emCastAnimationType.DirectHeal:
                if (AttachedController.HasWeapon)
                    PlayerAnimatior.CrossFade(COMBATCASTSPOTSTAFF);
                else
                    PlayerAnimatior.CrossFade(COMBATCASTSPOTHANDS);
                break;
            default:
                break;
        }

        if (skillType == SkillBaseSkill.emSkillType.AOE)
        {
            PlayerCamera.Instance.TargetUI.gameObject.SetActive(false);
            AttachedController.AOECamera.enabled = true;
            AttachedController.AOECamera.AOEProjector.enabled = true;
        }
    }

    public void AddCastingBar()
    {
        GameObject CastingBar = Resources.Load("UI/CastingBar") as GameObject;

        if (CastingBar != null)
        {
            UICamera cam = UICamera.FindCameraForLayer(CastingBar.layer);

            if (cam != null)
            {
                GameObject go = cam.gameObject;
                UIAnchor anchor = go.GetComponent<UIAnchor>();
                if (anchor != null) go = anchor.gameObject;

                GameObject FPBObj = GameObject.Instantiate(CastingBar) as GameObject;
                Transform t = FPBObj.transform;
                t.gameObject.name = "ProgressBar" + t.gameObject.name;
                t.parent = go.transform;
                t.localPosition = Vector3.zero;
                t.localRotation = Quaternion.identity;
                t.localScale = new Vector3(.8f, .8f, .8f);

                FPB = FPBObj.GetComponent<FloatingProgressBar>();
                if (FPB != null)
                {
                    FPB.target = AttachedController.MeisterFoward.transform;
                    FPB.Offset = new Vector3(-.18f, -1.78f, 0);
                    FPB.Timer = 10.2f;
                }
            }
            else
            {
                Debug.LogWarning("No camera found for layer " + LayerMask.LayerToName(CastingBar.layer), gameObject);
            }
        }
    }

    void CastSkill()
    {
        if (!PlayerAnimatior.isPlaying)
        {
            AttachedController.Skills.IsUsingSkill = false;
            State = MeisterCharacterState.Idle;
        }

    }

    public void PerformDodge()
    {
        State = MeisterCharacterState.Dodging;
        float DodgeAmount = 20f;

       
        if (!AttachedController.AudioPlayer.isPlaying)
        {
            AttachedController.AudioPlayer.clip = Dodge_SFX;
            AttachedController.AudioPlayer.Play();    
        }
        

        switch (AttachedController.DodgeMove)
        {
            case 1:
                PlayerAnimatior.CrossFade(COMBATDODGELEFT);
                break;
            case 2:
                PlayerAnimatior.CrossFade(COMBATDODGERIGHT);
                break;
            case 3:
                PlayerAnimatior.CrossFade(COMBATDODGEFORWARD);
                break;
            case 4:
                PlayerAnimatior.CrossFade(COMBATDODGEBACK);
                break;
            default:
                break;
        }



    }

    void Dodge()
    {
        if (!PlayerAnimatior.isPlaying)
        {
            AttachedController.DodgeMove = 0;
            State = MeisterCharacterState.Idle;
            Idle();
        }
    }

    public void StartEmote(string cmd)
    {
        State = MeisterCharacterState.Emote;
        AttachedController.IsLocked = true;
        switch (cmd)
        {
            case "wave":
                PlayerAnimatior.CrossFade(EMOTEWAVE);
                break;
            default:
                break;
        }

    }

    void Emote()
    {
        if (!PlayerAnimatior.isPlaying)
        {
            State = MeisterCharacterState.Idle;
            AttachedController.IsLocked = false;
            Idle();
        }
    }



    internal void StartTalking()
    {
        State = MeisterCharacterState.Talking;
        AttachedController.IsLocked = true;
        PlayerCamera.Instance.InDialogMode(true);

        int TalkAnimId = Random.Range(1, 3);

        switch (TalkAnimId)
        {
            case 1:
            case 2:
                PlayerAnimatior.CrossFade(IDLE);
                break;
            default:
                PlayerAnimatior.CrossFade(IDLE);
                break;
        }


    }

    internal void EndTalking()
    {
        State = MeisterCharacterState.Idle;
        AttachedController.IsLocked = false;
        PlayerCamera.Instance.InDialogMode(false);
    }
}

