﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MeisterSkills : MonoBehaviour
{

    public List<GameObject> PlayerFootPrints;
    public UISkillIcon CurrentCastingSkill;
    public bool IsUsingSkill;
    public bool IsCastTimer;

    public float AnimationTime = 0f;

    /// <summary>
    /// The last cast skill
    /// </summary>
    public UISkillIcon lastCastSkill;
    public MeisterController AttachedController;

    void Start()
    {       

    }

    void Update()
    {
        if (Input.GetMouseButtonDown(0) && AttachedController.CanCast)
        {
            if (AttachedController.InCastingMode && AttachedController.CurrentMana != 0 && IsUsingSkill == false)
            {
                CurrentCastingSkill = SkillPanel.Instance.SelectedSkill;

                if (!CurrentCastingSkill.OnCoolDown)
                {
                    IsUsingSkill = true;
                    AttachedController.CurrentMana -= CurrentCastingSkill.SkillData.GetManaCost();
                    
                    lastCastSkill = CurrentCastingSkill;
                    CurrentCastingSkill.StartCoolDown();
                    AttachedController.NetworkController.SendSpellCastRequest(CurrentCastingSkill.SkillData, PlayerCamera.Instance.TargetAim.position);
                    AttachedController.Animatior.Cast(CurrentCastingSkill.SkillData.Casttime, CurrentCastingSkill.SkillData.skillAnimationType, CurrentCastingSkill.SkillData.skillType);
                    UseCurrentSkill(CurrentCastingSkill.SkillData.id16 , PlayerCamera.Instance.TargetAim.position);
                    IsUsingSkill = false;
                }
            }
        }
    }

    public void UseCurrentSkill(int skillID , Vector3 targetPos)
    {
        SkillBaseSkill CurrentSkill = SkillDatabase.FindByID(skillID);

        switch (CurrentSkill.skillType)
        {
            case SkillBaseSkill.emSkillType.Impact:
                SpawnSelectedProjectileSkill(CurrentSkill, targetPos);
                break;
            case SkillBaseSkill.emSkillType.Spot:
            case SkillBaseSkill.emSkillType.Buff:
                SpawnSelectedBuffSkill();
                break;
            default:
                break;
        }
    }

    private void SpawnSelectedBuffSkill()
    {

        GameObject CurrentSBuffSpell;

        CurrentSBuffSpell = Instantiate(lastCastSkill.SkillData.skilleffect) as GameObject;
        CurrentSBuffSpell.transform.parent = AttachedController.BuffSpawn;
        CurrentSBuffSpell.transform.localPosition = Vector3.zero;
        CurrentSBuffSpell.transform.LookAt(PlayerCamera.Instance.TargetAim);

        OneTimeEffects oneTimeEffect = CurrentSBuffSpell.GetComponent<OneTimeEffects>();

        if (oneTimeEffect != null)
        {
            if (lastCastSkill.SkillData.skillAnimationType == SkillBaseSkill.emCastAnimationType.DirectHeal)
            {
                oneTimeEffect.ApplyHealEffect(lastCastSkill.SkillData.GeDamageValue());
            }
        }

    }

    private void SpawnSelectedProjectileSkill(SkillBaseSkill skill , Vector3 pos)
    {

        GameObject CurrentSpawnedSpell;
        
        CurrentSpawnedSpell = Instantiate(skill.skilleffect, Vector3.zero, Quaternion.identity) as GameObject;
        CurrentSpawnedSpell.transform.localPosition = AttachedController.SkillSpawn.position;
        CurrentSpawnedSpell.transform.LookAt(pos);

        SkillHitTest SHT = CurrentSpawnedSpell.GetComponent<SkillHitTest>();
        //if (SHT != null)
        //    SHT.SkillElement = lastCastSkill.SkillData.skillElement;
    }

    public void SpawnSelectedAOESkill(Transform spawnFrom)
    {
        GameObject CurrentSpawnedSpell;

        CurrentSpawnedSpell = Instantiate(lastCastSkill.SkillData.skilleffect) as GameObject;
        CurrentSpawnedSpell.transform.position = spawnFrom.position;
        AttachedController.AOECamera.AOEProjector.enabled = false;
        PlayerCamera.Instance.TargetUI.gameObject.SetActive(true);
        AttachedController.AOECamera.enabled = false;
    }



}

