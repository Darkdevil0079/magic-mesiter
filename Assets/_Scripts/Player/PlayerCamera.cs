using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour
{

    public static PlayerCamera Instance;
    private Transform _Camera;

    public UITexture TargetUI;
    public Transform TargetAim;
    public Transform MeisterFacingDirection;
    public SSAOEffect SSAOEffect;

    public bool ResetCameraPos = false;

    public float Distance = 5f;
    public float DistanceMin = 3f;
    public float DistanceMax = 10f;
    public float DistanceSmooth = 0.05f;
    public float DistanceResumeSmooth = 1f;

    public float X_MouseSensitivity = 5f;
    public float Y_MouseSensitivity = 5f;
    public float CombatCameraSensitivityX = 5f;
    public float CombatCameraSensitivityY = 5f;
    public float MouseWheelSensitivity = 5f;

    public float X_Smooth = 0.05f;
    public float Y_Smooth = 0.1f;

    public float Y_MinLimit = -40f;
    public float Y_MaxLimit = 80f;

    public float OcclusionDistanceStep = 0.5f;
    public int MaxOcclusionChecks = 10;

    public float walkDistance;
    public float RunDistance;
    public float height;

    public float heightDamping = 2.0f;
    public float rotationDamping = 3.0f;

    private float velX = 0f;
    private float velY = 0f;
    private float velZ = 0f;

    public float mouseX = 0f;
    public float mouseY = 0f;
    private float velDistance = 0f;
    public float startDistance = 0f;
    private float desiredDistance = 0f;

    private Vector3 desiredPostion = Vector3.zero;
    private Vector3 position = Vector3.zero;
    private float distanceSmooth = 0f;
    private float preOccludedDistance = 0f;

    public float CombatFadeTime = 3f;

    public float CombatCameraMinY = -20f;
    public float CombatCameraMaxY = 20f;

    private float rotationY = 0F;

    private Transform TargetLookAt;
    private Transform MeisterFoward;
    private Transform ResetPos;

    private bool _camButtonDown = false;

    void Awake()
    {
        Instance = this;
        _Camera = this.transform;
    }

    void Start()
    {

        TargetUI.gameObject.SetActive(false);
        Distance = Mathf.Clamp(Distance, DistanceMin, DistanceMax);
        startDistance = Distance;

        if (GameManager.LocalPlayer == null)
            return;

    }

    void LateUpdate()
    {
        if (Input.GetKeyDown(KeyCode.F5))
            SSAOEffect.enabled = !SSAOEffect.enabled;

        if (TargetLookAt == null)
            return;

        if (GameManager.LocalPlayer.InCastingMode)
        {

            _Camera.parent = GameManager.LocalPlayer.CombatRightHolder;
            _Camera.position = Vector3.Lerp(_Camera.position, GameManager.LocalPlayer.CombatRightHolder.position, Time.deltaTime * 3f);
            GameManager.LocalPlayer.Player.Rotate(0, Input.GetAxis("Mouse X") * Time.deltaTime * GameManager.LocalPlayer.TurnSpeed, 0);

            rotationY += Input.GetAxis("Mouse Y") * CombatCameraSensitivityY;
            rotationY = Mathf.Clamp(rotationY, CombatCameraMinY, CombatCameraMaxY);

            //_Camera.localEulerAngles = new Vector3(-rotationY, 0, 0);
            //mouseX += Input.GetAxis ("Mouse X") * X_MouseSensitivity;

        }
        else
        {

            if (!Charactor.Instance.isOpen && !SkillTree.Instance.isOpen)
            {
                HandlePlayerInput();
            }

            if (GameManager.LocalPlayer != null)              
                GameManager.LocalPlayer.ResetPoint.transform.localPosition = new Vector3(0, 0, -Distance);

            var count = 0;
            do
            {
                CalculateDesiredPosition();
                count++;
            } while (CheckIfOccluded(count));


            if (_camButtonDown)
            {

                mouseX += Input.GetAxis("Mouse X") * X_MouseSensitivity * 0.02f;
                mouseY -= Input.GetAxis("Mouse Y") * Y_MouseSensitivity * 0.02f;

                RotateCamera();

            }
            else
            {
                float wantedRotationAngle = TargetLookAt.eulerAngles.y;
                float wantedHeight = TargetLookAt.position.y + height;

                float currentRotationAngle = transform.eulerAngles.y;
                float currentHeight = transform.position.y;

                //Damp the rotation around the y-axis
                currentRotationAngle = Mathf.LerpAngle(currentRotationAngle, wantedRotationAngle, rotationDamping * Time.deltaTime);

                //Damp the Height
                currentHeight = Mathf.Lerp(currentHeight, wantedHeight, heightDamping * Time.deltaTime);

                //Convert the angle into a rotation
                Quaternion currentRotation = Quaternion.Euler(0, currentRotationAngle, 0);

                //Set the position of the camera on the x-y plane to:
                //distance meters behind the target
                _Camera.position = MeisterFoward.position;

                _Camera.position -= currentRotation * Vector3.forward * Distance;

                //Set the height of the camera
                _Camera.position = new Vector3(_Camera.position.x, currentHeight, _Camera.position.z);

                // Always loot at the target
                _Camera.LookAt(MeisterFoward);
            }


        }
    }

    void HandlePlayerInput()
    {

        var deadZone = 0.01f;

        if (Input.GetMouseButtonDown(2))
        {
            _camButtonDown = true;
        }

        if (Input.GetMouseButtonUp(2))
        {
            _camButtonDown = false;
            mouseX = 0;
            mouseY = 0;
        }

        //  This is where we will limit mouse Y
        mouseY = Helper.ClampAngle(mouseY, Y_MinLimit, Y_MaxLimit);

        if (Input.GetAxis("Mouse ScrollWheel") < -deadZone || Input.GetAxis("Mouse ScrollWheel") > deadZone)
        {
            desiredDistance = Mathf.Clamp(Distance - Input.GetAxis("Mouse ScrollWheel") * MouseWheelSensitivity,
                              DistanceMin, DistanceMax);

            preOccludedDistance = desiredDistance;
            distanceSmooth = DistanceSmooth;
        }


    }

    void CalculateDesiredPosition()
    {
        //Evaluate distance
        ResetDesiredDistance();
        Distance = Mathf.SmoothDamp(Distance, desiredDistance, ref velDistance, distanceSmooth);

        // Calculate desired position
        desiredPostion = CalculatePosition(mouseY, mouseX, Distance);
    }

    Vector3 CalculatePosition(float rotationX, float rotationY, float distance)
    {
        Vector3 direction = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(rotationX, rotationY, 0);

        return TargetLookAt.position + rotation * direction;

    }

    bool CheckIfOccluded(int count)
    {
        var isOccluded = false;

        var nearestDistance = CheckCameraPoints(TargetLookAt.position, desiredPostion);

        if (nearestDistance != -1)
        {
            if (count < MaxOcclusionChecks)
            {
                isOccluded = true;
                Distance -= OcclusionDistanceStep;

                if (Distance < 0.25f)
                    Distance = 0.25f;
            }
            else
                Distance = nearestDistance - Camera.main.nearClipPlane;

            desiredDistance = Distance;
            distanceSmooth = DistanceResumeSmooth;
        }

        return isOccluded;
    }

    float CheckCameraPoints(Vector3 from, Vector3 to)
    {
        var nearDistance = -1f;

        RaycastHit hitInfo;

        Helper.ClipPlanePoints clipPlanePoints = Helper.ClipPlaneAtNear(to);

        // Draw lines in the editor to make it easier to visualize
        Debug.DrawLine(from, to + (transform.forward * -camera.nearClipPlane), Color.red);

        Debug.DrawLine(from, clipPlanePoints.UpperLeft);
        Debug.DrawLine(from, clipPlanePoints.LowerLeft);
        Debug.DrawLine(from, clipPlanePoints.LowerRight);
        Debug.DrawLine(from, clipPlanePoints.UpperRight);

        Debug.DrawLine(clipPlanePoints.UpperLeft, clipPlanePoints.UpperRight);
        Debug.DrawLine(clipPlanePoints.UpperRight, clipPlanePoints.LowerRight);
        Debug.DrawLine(clipPlanePoints.LowerRight, clipPlanePoints.LowerLeft);
        Debug.DrawLine(clipPlanePoints.LowerLeft, clipPlanePoints.UpperLeft);

        if (Physics.Linecast(from, clipPlanePoints.UpperLeft, out hitInfo) && hitInfo.collider.tag != "Player")
            nearDistance = hitInfo.distance;

        if (Physics.Linecast(from, clipPlanePoints.LowerLeft, out hitInfo) && hitInfo.collider.tag != "Player")
            if (hitInfo.distance < nearDistance || nearDistance == -1)
                nearDistance = hitInfo.distance;

        if (Physics.Linecast(from, clipPlanePoints.LowerRight, out hitInfo) && hitInfo.collider.tag != "Player")
            if (hitInfo.distance < nearDistance || nearDistance == -1)
                nearDistance = hitInfo.distance;

        if (Physics.Linecast(from, clipPlanePoints.UpperRight, out hitInfo) && hitInfo.collider.tag != "Player")
            if (hitInfo.distance < nearDistance || nearDistance == -1)
                nearDistance = hitInfo.distance;

        if (Physics.Linecast(from, to + (transform.forward * -camera.nearClipPlane), out hitInfo) && hitInfo.collider.tag != "Player")
            if (hitInfo.distance < nearDistance || nearDistance == -1)
                nearDistance = hitInfo.distance;

        return nearDistance;

    }

    void ResetDesiredDistance()
    {
        if (desiredDistance < preOccludedDistance)
        {
            var pos = CalculatePosition(mouseY, mouseX, preOccludedDistance);

            var nearestDistance = CheckCameraPoints(GameManager.LocalPlayer.Player.position, pos);

            if (nearestDistance == -1 || nearestDistance > preOccludedDistance)
            {
                desiredDistance = preOccludedDistance;
            }

        }
    }

    public void Reset()
    {
        mouseX = 0;
        mouseY = 10;
        Distance = startDistance;
        desiredDistance = Distance;
        preOccludedDistance = Distance;
        _Camera.position = new Vector3(TargetLookAt.position.x, TargetLookAt.position.y + height, TargetLookAt.position.z - Distance);
        _Camera.LookAt(TargetLookAt.position);
    }

    public float AddAngel(float CurrentAngel, float Amount, bool IsAdd)
    {
        if (IsAdd)
        {
            if (CurrentAngel == 360)
            {
                return 0 + Amount;
            }
            else if (CurrentAngel + Amount > 360)
            {
                return 0 + (CurrentAngel + Amount) - 360;
            }

        }
        else
        {
            if (CurrentAngel == 0)
            {
                return 360 - Amount;
            }
            else if (CurrentAngel - Amount < 0)
            {
                return 360 - (Amount - CurrentAngel);
            }
        }

        return IsAdd == true ? CurrentAngel + Amount : CurrentAngel - Amount;

    }

    public void SetupCombatCamera()
    {
        Screen.showCursor = false;
        TargetUI.gameObject.SetActive(false);
        mouseY = 10f;
        mouseX = 0f;
        rotationY = 0f;
    }

    public void ResetCombatCamera()
    {
        ResetCameraPos = true;
        TargetUI.gameObject.SetActive(false);
        _Camera.parent = null;
        Screen.showCursor = true;
    }

    private void RotateCamera()
    {
        //y = ClampAngle(y, yMinLimit, yMaxLimet);

        Quaternion rotation = Quaternion.Euler(mouseY, mouseX, 0);
        Vector3 position = rotation * new Vector3(0.0f, 0.0f, -Distance) + TargetLookAt.position;

        _Camera.rotation = rotation;
        _Camera.position = position;
    }

    public void CameraSetup(MeisterController localPlayer)
    {
        TargetLookAt = localPlayer.TargetLookAt.transform;
        MeisterFoward = localPlayer.MeisterFoward.transform;
        ResetPos = localPlayer.ResetPoint.transform;

        Reset();

    }

    public void InDialogMode(bool Active)
    {
        this.enabled = !Active;
	}

}

