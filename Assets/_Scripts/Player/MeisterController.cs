﻿using PixelCrushers.DialogueSystem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


public delegate void JumpDelegate();

public class MeisterController : BaseMeister
{
    private const float inputDeadZone = 0.01f;
    public float ForwardSpeed = 40f;
    public float ForwardWalkingSpeed = 10f;
    public float BackwardSpeed = 2f;
    public float StrafingSpeed = 5f;
    public float DodgeDistance = 20f;
    public float JumpSpeed = 6f;
    public float TurnSpeed = 140f;

    private Vector3 dodgeDirection;

    public Turn PlayerTurn { get; set; }

    public float Gravity = 21f;
    public float TerminalVelocity = 20f;
    public Vector3 MoveVector;
    public float VerticalVelocity;

    public void SetIsRemotePlayer(bool val)
    {
        IsRemotePlayer = val;
    }

    public bool isGrounded
    {
        get
        {
            return PlayerController.isGrounded;
        }
    }

    void Setup()
    {
        IsLocked = false;
        SetupMeisterFramework();
        Animatior = this.GetComponent<MeisterAnimatiors>();
        AudioPlayer = this.GetComponent<AudioSource>();
        Animatior.AttachedController = this;
        PlayerController = this.GetComponent<CharacterController>();
        MeisterChat = this.GetComponent<ServerChat>();
        NetworkController = this.GetComponent<MeisterNetwork>();
        Equipment = this.GetComponent<MeisterEquipment>();
        dialogNameDisplay = this.GetComponent<OverrideActorName>();

        Skills = this.GetComponent<MeisterSkills>();
        Skills.AttachedController = this;

        if (!IsCustomizer)
	    {
            MapIcon = Player.GetComponent<KGFMapIcon>();

            if (IsRemotePlayer)
            {
                MapIcon.SetTextureIcon(Resources.Load("UI/Friendly") as Texture2D);
            }
            else // Local Only Code
            {
                GameManager.Instance.MiniMap.SetTarget(Player.gameObject);
                BasePlayer.FindChild("CustCamera").GetComponent<Camera>().enabled = true;

            }
                
	    }

    }

    void Start()
    {
        Setup();

        HasWeapon = false;

        if (CustomizerManager.Instance != null)
            CustomizerManager.Instance.PlayerName.value = PlayerDataStorage.Instance.PlayerName;

        if (PlayerCamera.Instance != null && !IsRemotePlayer)
            PlayerCamera.Instance.CameraSetup(this);

        SetupPlayerAppearance();
        SetupPlayerBaseStats();
    }

    new void Update()
    {
        if (IsRemotePlayer)
        { 
            if (DodgeMove > 0)
            {
                Animatior.PerformDodge();
            }
            return;
        }

        if (!IsLocked)
        {
            ResetMotion();

            GetLocomotionInput();
            HandleActionInput();

            ProcessMotion();
        }

        base.Update();
    }

    /// <summary>
    /// This handles all the player input commands
    /// </summary>
    void HandleActionInput()
    {
        if (Input.GetKeyDown(KeyCode.Space) && DodgeMove == 0)
        {
            Jump();
        }

        if (Input.GetKeyDown(KeyCode.F1))
        {
            NetworkController.SendEmoteRequest("wave");
            Animatior.StartEmote("wave");
        }

        // Right Mouse Button Down
        if (Input.GetMouseButtonDown(1))
            EnterCombatMode();

        // Right Mouse Button Up
        if (Input.GetMouseButtonUp(1))
        {
            InCastingMode = false;
            if (PlayerCamera.Instance != null)
                PlayerCamera.Instance.ResetCombatCamera();
            Screen.showCursor = true;
            NextMagicRecharge = Time.time + 5f;
        }

        if (Input.GetKeyDown(KeyCode.LeftShift))
        {
            ToggleWalking = !ToggleWalking;
        }


        if (Input.GetKeyDown(KeyCode.E) && DodgeMove == 0)
        {
            if (InCastingMode)
            {
                IsBlocking = true;
               
                BlockEffect = Instantiate(Resources.Load("Skills/Block")) as GameObject;
                BlockEffect.transform.parent = BuffSpawn;
                BlockEffect.transform.localPosition = new Vector3(0, 2, 0);
                BlockEffect.transform.localScale = new Vector3(.3f, .3f, .3f);

                Animatior.State = MeisterCharacterState.Blocking;
            }
        }

        if (Input.GetKeyUp(KeyCode.E))
        {
            IsBlocking = false;
            GameObject.Destroy(BlockEffect);
            Animatior.State = MeisterCharacterState.Idle;
        }


        if (Input.GetKeyDown(KeyCode.C))
        { // Open the Charactor Screen
            if (Charactor.Instance.isOpen)
                Charactor.Instance.CloseWindow();
            else
                Charactor.Instance.ShowCharactorWindow();
        }

        if (Input.GetKeyDown(KeyCode.N))
        { // Open the QuestLogSystem Screen
            if (QuestLogSystem.Instance.isOpen)
                QuestLogSystem.Instance.CloseWindow();
            else
                QuestLogSystem.Instance.ShowQuestLogWindow();
        }

        if (Input.GetKeyDown(KeyCode.P))
        { // Open the Skills Screen
            if (SkillTree.Instance.isOpen)
                SkillTree.Instance.CloseWindow();
            else
                SkillTree.Instance.OpenSkillTree();

        }

        if (Input.GetKeyDown(KeyCode.O))
        { // Open the Options Screen

        }

        if (Input.GetKeyDown(KeyCode.Tab))
        { // Open the Player Online / Offline Screen

        }

        if (Input.GetKeyDown(KeyCode.B)) // Summon the Player Mount or Send away
        {
            if (!IsOnMount)
            {
                SummmonMount();
            }
        }
    }

    void GetLocomotionInput()
    {
        if (Animatior.State != MeisterCharacterState.Dead)
        {
            var deadZone = 0.1f;

            if (Input.GetAxis("Vertical") > deadZone || Input.GetAxis("Vertical") < -deadZone)
            {
                MoveVector += new Vector3(0, 0, Input.GetAxis("Vertical"));
                PlayerCamera.Instance.ResetCameraPos = false;
            }


            if (InCastingMode)
            {
                if (Input.GetKeyDown(KeyCode.LeftArrow) || Input.GetKeyDown(KeyCode.A))
                {
                    if (ButtonCooler > 0 && ButtonCountLeft == 1/*Number of Taps you want Minus One*/)
                    {
                        DodgeMove = 1;
                        Animatior.PerformDodge();
                    }
                    else
                    {
                        ButtonCooler = .5f;
                        ButtonCountLeft += 1;
                    }
                }

                if (Input.GetKeyDown(KeyCode.RightArrow) || Input.GetKeyDown(KeyCode.D))
                {
                    if (ButtonCooler > 0 && ButtonCountRight == 1/*Number of Taps you want Minus One*/)
                    {
                        DodgeMove = 2;
                        Animatior.PerformDodge();
                    }
                    else
                    {
                        ButtonCooler = .5f;
                        ButtonCountRight += 1;
                    }
                }

                if (Input.GetKeyDown(KeyCode.UpArrow) || Input.GetKeyDown(KeyCode.W))
                {
                    if (ButtonCooler > 0 && ButtonCountForward == 1/*Number of Taps you want Minus One*/)
                    {
                        DodgeMove = 3;
                        Animatior.PerformDodge();
                    }
                    else
                    {
                        ButtonCooler = .5f;
                        ButtonCountForward += 1;
                    }
                }

                if (Input.GetKeyDown(KeyCode.DownArrow) || Input.GetKeyDown(KeyCode.S))
                {
                    if (ButtonCooler > 0 && ButtonCountBack == 1/*Number of Taps you want Minus One*/)
                    {
                        DodgeMove = 4;
                        Animatior.PerformDodge();
                    }
                    else
                    {
                        ButtonCooler = .5f;
                        ButtonCountBack += 1;
                    }
                }

                if (ButtonCooler > 0)
                    ButtonCooler -= 1 * Time.deltaTime;
                else
                {
                    ButtonCountLeft = 0;
                    ButtonCountRight = 0;
                    ButtonCountForward = 0;
                    ButtonCountBack = 0;

                }

                if (Input.GetAxis("Horizontal") > deadZone || Input.GetAxis("Horizontal") < -deadZone)
                {
                    MoveVector += new Vector3(Input.GetAxis("Horizontal"), 0, 0);
                }

                PlayerTurn = Turn.none;
            }
            else
            {
                if (Input.GetButtonUp("Horizontal"))
                    PlayerTurn = Turn.none;
                else
                {
                    if (Input.GetButton("Horizontal"))
                    {
                        PlayerCamera.Instance.ResetCameraPos = false;
                        if (Input.GetAxis("Horizontal") > 0)
                            PlayerTurn = Turn.right;
                        else
                            PlayerTurn = Turn.left;
                    }
                }

            }

            Animatior.DetermineCurrentDirection();
            PlayerDataStorage.Instance.LastKnowPos = Player.position;
        }


    }

    void ProcessMotion()
    {
        if (PlayerTurn != Turn.none)
            Player.Rotate(0, (int)PlayerTurn * Time.deltaTime * TurnSpeed, 0);

        // Transform MoveVector to Worlds Space
        MoveVector = Player.TransformDirection(MoveVector);

        // Normalize MoveVector if Magnitude > 1
        if (MoveVector.magnitude > 1)
            MoveVector = Vector3.Normalize(MoveVector);

        // Multiply MoveVector by MoveSpeed
        MoveVector *= MoveSpeed();

        // Reapply VerticalVelocity MoveVector.y
        MoveVector = new Vector3(MoveVector.x, VerticalVelocity, MoveVector.z);

        // Apply Gravity
        ApplyGravity();

        ApplyDodge();

        PlayerController.Move(MoveVector * Time.deltaTime);

    }

    public void ResetMotion()
    {
        VerticalVelocity = MoveVector.y;
        MoveVector = Vector3.zero;
    }

    private void ApplyDodge()
    {
        if (DodgeMove > 0)
        {
            switch (DodgeMove)
            {
                case 1:
                    MoveVector += -Player.right * 20f;
                    break;

                case 2:
                    MoveVector += Player.right * 20f;
                    break;

                case 3:
                    MoveVector += Player.forward * 40f;
                    break;

                case 4:
                    MoveVector += -Player.forward * 40f;
                    break;

            }

            CanCast = false;
        }
        else
            CanCast = true;
    }

    void ApplyGravity()
    {
        if (MoveVector.y > -TerminalVelocity)
            MoveVector = new Vector3(MoveVector.x, MoveVector.y - Gravity * Time.deltaTime, MoveVector.z);

        if (isGrounded && MoveVector.y < -1)
            MoveVector = new Vector3(MoveVector.x, -1, MoveVector.z);
    }

    public void Jump()
    {
        if (isGrounded)
        {
            VerticalVelocity = JumpSpeed;
        }

        Animatior.Jump();
    }

    public void LockCameraOff()
    {
        PlayerCamera.Instance.ResetCameraPos = PlayerCamera.Instance.ResetCameraPos == false ? true : false;
    }

    float MoveSpeed()
    {
        float Movespeed = 0f;

        switch (Animatior.MoveDirection)
        {
            case MeisterAnimatiors.Direction.Stationary:
                Movespeed = 0;
                break;
            case MeisterAnimatiors.Direction.Forward:
            case MeisterAnimatiors.Direction.LeftForward:
            case MeisterAnimatiors.Direction.RightForward:
                if (InCastingMode)
                {
                    Movespeed = ForwardWalkingSpeed;
                }
                else
                {
                    if (ToggleWalking)
                    {
                        Movespeed = ForwardSpeed / ForwardWalkingSpeed;
                    }
                    else
                        Movespeed = ForwardSpeed;
                }
                break;
            case MeisterAnimatiors.Direction.Backward:
            case MeisterAnimatiors.Direction.LeftBackward:
            case MeisterAnimatiors.Direction.RightBackward:
                Movespeed = 3;
                break;
            case MeisterAnimatiors.Direction.Left:
            case MeisterAnimatiors.Direction.Right:
                Movespeed = StrafingSpeed;
                break;
        }

        return Movespeed;
    }
}


