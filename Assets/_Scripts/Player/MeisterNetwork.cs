﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class MeisterNetwork : Photon.MonoBehaviour
{

    private bool appliedInitialUpdate;
    private MeisterController AttachedController;

    public Vector3 correctPlayerPos = Vector3.zero; //We lerp towards this
    public Quaternion correctPlayerRot = Quaternion.identity; //We lerp towards this

    void Awake()
    {
        AttachedController = GetComponent<MeisterController>();
    }

    // Use this for initialization
    void Start()
    {

        if (Application.loadedLevelName != GameSettings.PlayerCreator)
        {
            if (photonView.isMine)
            {
                //MINE: Local player, simply enable the local scripts
                gameObject.name = PlayerDataStorage.Instance.PlayerName;
                PhotonNetwork.playerName = PlayerDataStorage.Instance.PlayerName;

                AttachedController.enabled = true;
                AttachedController.SetIsRemotePlayer(false);
                MeisterUI.Instance.LinkedPlayer = AttachedController;
               
                photonView.RPC("SendChatMessageBlank", PhotonTargets.OthersBuffered, PhotonNetwork.playerName + " has joined the game");

            }
            else
            {
                gameObject.name = "Remote" + photonView.viewID;
                PhotonNetwork.playerName = photonView.owner.name;
                AttachedController.NetworkMeisterData = photonView.instantiationData;

                Debug.Log("WearHat :" + AttachedController.NetworkMeisterData[0] + ",WeapID" + AttachedController.NetworkMeisterData[12]);

                AttachedController.enabled = true;
            }

            AttachedController.SetIsRemotePlayer(!photonView.isMine);
            if (!PhotonNetwork.offlineMode)
                SendUpdatePlayerCloths("Network");
        }
        else
        {
            AttachedController.enabled = true;
            AttachedController.SetIsRemotePlayer(true);
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.isWriting)
        {
            //We own this player: send the others our data

            stream.SendNext(AttachedController.Player.position);
            stream.SendNext(AttachedController.Player.rotation);
            stream.SendNext((bool)AttachedController.InCastingMode);
            stream.SendNext((int)AttachedController.DodgeMove);
            if (AttachedController.Animatior != null)
                stream.SendNext((int)AttachedController.Animatior.State);

        }
        else
        {
            correctPlayerPos = (Vector3)stream.ReceiveNext();
            correctPlayerRot = (Quaternion)stream.ReceiveNext();
            AttachedController.InCastingMode = (bool)stream.ReceiveNext();
            AttachedController.DodgeMove = (int)stream.ReceiveNext();
            if (AttachedController.Animatior != null)
            {
                AttachedController.Animatior.State = (MeisterCharacterState)stream.ReceiveNext();
                Debug.Log(AttachedController.Animatior.State);
            }



            if (!appliedInitialUpdate)
            {
                appliedInitialUpdate = true;
                AttachedController.Player.position = correctPlayerPos;
                AttachedController.Player.rotation = correctPlayerRot;
            }
        }
    }

    void Update()
    {
        if (!photonView.isMine && Application.loadedLevelName != GameSettings.PlayerCreator)
        {
            //Update remote player (smooth this, this looks good, at the cost of some accuracy)
            AttachedController.Player.position = Vector3.Lerp(AttachedController.Player.position, correctPlayerPos, Time.deltaTime * 5);
            AttachedController.Player.rotation = Quaternion.Lerp(AttachedController.Player.rotation, correctPlayerRot, Time.deltaTime * 5);

        }
    }

    void OnPhotonInstantiate(PhotonMessageInfo info)
    {

    }

    internal void SendUpdatePlayerCloths(string calledBy)
    {

        PlayerItemEquiped ItemToLookUp;
        int HatIndex = -1;
        int ClothsIndex = -1;
        int GlovesIndex = -1;
        int BootsIndex = -1;
        int ExtraIndex = -1;

        ItemToLookUp = PlayerDataStorage.Instance.WearingEquipment.Find(lookup => lookup.Slot == InvBaseItem.Slot.Hat);
        if (ItemToLookUp != null)
            HatIndex = ItemToLookUp.ItemID;

        ItemToLookUp = PlayerDataStorage.Instance.WearingEquipment.Find(lookup => lookup.Slot == InvBaseItem.Slot.Clothes);
        if (ItemToLookUp != null)
            ClothsIndex = ItemToLookUp.ItemID;

        ItemToLookUp = PlayerDataStorage.Instance.WearingEquipment.Find(lookup => lookup.Slot == InvBaseItem.Slot.Gloves);
        if (ItemToLookUp != null)
            GlovesIndex = ItemToLookUp.ItemID;

        ItemToLookUp = PlayerDataStorage.Instance.WearingEquipment.Find(lookup => lookup.Slot == InvBaseItem.Slot.Boots);
        if (ItemToLookUp != null)
            BootsIndex = ItemToLookUp.ItemID;

        ItemToLookUp = PlayerDataStorage.Instance.WearingEquipment.Find(lookup => lookup.Slot == InvBaseItem.Slot.Extra);
        if (ItemToLookUp != null)
            ExtraIndex = ItemToLookUp.ItemID;


        object[] objArray = new object[] {
                                    PlayerDataStorage.Instance.WearingHat,
                                    HatIndex,
                                    ClothsIndex,
                                    GlovesIndex,
                                    BootsIndex,
                                    ExtraIndex
                                    };

        photonView.RPC("UpdateRemotePlayerCloths", PhotonTargets.AllViaServer, objArray, calledBy);
    }

    internal void SendEmoteRequest(string emoteCmd)
    {
        photonView.RPC("SendEmote", PhotonTargets.Others, emoteCmd);
    }

    internal void SendSpellCastRequest(SkillBaseSkill skillData, Vector3 AimPos)
    {
        photonView.RPC("SendSpellCast", PhotonTargets.Others, skillData.id16, skillData.Casttime, (int)skillData.skillAnimationType, (int)skillData.skillType, AimPos.x, AimPos.y, AimPos.z);
    }

    [RPC]
    internal void UpdateRemotePlayerCloths(object[] wearingCloths, string calledby)
    {

        if (AttachedController.IsRemotePlayer)
        {
            Debug.Log(calledby + " - Got Cloths Update");

            if (AttachedController.NetworkMeisterData != null)
            {
                Debug.Log("Hat Wearing " + wearingCloths[0] + " " + "Hat " + wearingCloths[1] + " " + "Cloths" + wearingCloths[2] + " ");

                AttachedController.NetworkMeisterData[1] = (bool)wearingCloths[0];
                AttachedController.NetworkMeisterData[7] = (int)wearingCloths[1];
                AttachedController.NetworkMeisterData[8] = (int)wearingCloths[2];
                AttachedController.NetworkMeisterData[9] = (int)wearingCloths[3];
                AttachedController.NetworkMeisterData[10] = (int)wearingCloths[4];
                AttachedController.NetworkMeisterData[11] = (int)wearingCloths[5];
            }
        }

        AttachedController.SetupPlayerAppearance();
    }

    [RPC]
    public void SendEmote(string cmd, PhotonMessageInfo info)
    {
        AttachedController.Animatior.StartEmote(cmd);
    }

    [RPC]
    public void SendSpellCast(  int skillId,
                                float castTime,
                                int skillAnimationType,
                                int skillType,
                                float TargetX,
                                float TargetY,
                                float TargetZ,
                                PhotonMessageInfo info)
    {



        AttachedController.Animatior.Cast(castTime, (SkillBaseSkill.emCastAnimationType)skillAnimationType, (SkillBaseSkill.emSkillType)skillType);
        AttachedController.Skills.UseCurrentSkill(skillId, new Vector3(TargetX, TargetY, TargetZ));
    }
}
