﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using System;

public class MeisterDialog : ActOnDialogueEvent {

    private MeisterController AttactedController;

    void Start()
    {
        AttactedController = this.GetComponent<MeisterController>();
    }

    public override void TryStartActions(Transform actor)
    {
        GameManager.Instance.MiniMap.gameObject.SetActive(false);
        GameManager.Instance.MainUI.gameObject.SetActive(false);
        AttactedController.Animatior.StartTalking();
    }

    public override void TryEndActions(Transform actor)
    {
        GameManager.Instance.MiniMap.gameObject.SetActive(true);
        GameManager.Instance.MainUI.gameObject.SetActive(true);
         AttactedController.Animatior.EndTalking();
    }
    

}
