﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class LootItem : MonoBehaviour {

    public int ItemID;
    public Chest SpawnChest;

    void OnPress(bool isDown)
    {
        if (isDown)
        {
            if (PlayerDataStorage.Instance.PlayerInv.Where(w => w.ItemID == ItemID).FirstOrDefault() == null)
            {
                PlayerInventory ItemToAdd = new PlayerInventory();
                ItemToAdd.ItemID = ItemID;
                ItemToAdd.ItemLevel = 1;
                ItemToAdd.ItemGrade = InvGameItem.Quality.Common;
                ItemToAdd.Slot = ItemStorage.LocalItemStorage .FindFirstFreeSlot();

                PlayerDataStorage.Instance.PlayerInv.Add(ItemToAdd);
                Charactor.Instance.RefreshPlayerInv();
            }
        }
    }
}
