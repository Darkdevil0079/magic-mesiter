﻿using UnityEngine;
using System.Collections;

public class FloatingWindow : MonoBehaviour
{
    /// <summary>
    /// Target object this UI element should follow.
    /// </summary>

    public Transform target;

    private Transform mTrans;
    private Camera mGameCam;
    private Camera mUICam;
    private Vector3 mPos;
    private bool mVisible = true;

    void Start()
    {
        if (target == null) { Destroy(gameObject); return; }
        mTrans = transform;
        mGameCam = NGUITools.FindCameraForLayer(target.gameObject.layer);
        mUICam = NGUITools.FindCameraForLayer(gameObject.layer);
    }

    void LateUpdate()
    {
        if (target == null) { Destroy(gameObject); return; }

        mPos = mGameCam.WorldToViewportPoint(target.position);

        bool visible = (mPos.z > 0f && mPos.x > 0f && mPos.x < 1f && mPos.y > 0f && mPos.y < 1f);
        
        if (mVisible != visible)
        {
            mVisible = visible;
            UIWidget[] widgets = gameObject.GetComponentsInChildren<UIWidget>();
            foreach (UIWidget w in widgets) w.enabled = mVisible;
        }

        if (mVisible)
        {
            mPos = mUICam.ViewportToWorldPoint(mPos);
            mPos.z = 0f;
            mTrans.position = new Vector3(mPos.x - .14f, mPos.y, mPos.z);
        }
    }

    public void CloseWindow()
    {
        DestroyImmediate(this.gameObject);
        
    }

   
}
