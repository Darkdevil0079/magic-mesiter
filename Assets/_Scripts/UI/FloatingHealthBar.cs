﻿using UnityEngine;
using System.Collections;

public class FloatingHealthBar : MonoBehaviour
{

    /// <summary>
    /// Target object this UI element should follow.
    /// </summary>

    public Transform target;
    public UIProgressBar HealthBar;
    public UILabel HealthBarText;

    public int MaxHP { get; set; }
    public int CurrentHP { get; set; }

    private Transform mTrans;
    private Camera mGameCam;
    private Camera mUICam;
    private Vector3 mPos;
    private bool mVisible = true;
    private float NewHealthBarValue;

    void Start()
    {
        if (target == null) { Destroy(gameObject); return; }
        mTrans = transform;
        mGameCam = NGUITools.FindCameraForLayer(target.gameObject.layer);
        mUICam = NGUITools.FindCameraForLayer(gameObject.layer);
    }

    void Update()
    {
        NewHealthBarValue = (float)CurrentHP / (float)MaxHP;
        HealthBar.value = Mathf.Lerp(HealthBar.value, NewHealthBarValue, Time.deltaTime * 3f);
        HealthBarText.text = CurrentHP + " / " + MaxHP;
    }

    void LateUpdate()
    {
        if (target == null) { Destroy(gameObject); return; }

        mPos = mGameCam.WorldToViewportPoint(target.position);

        bool visible = (mPos.z > 0f && mPos.x > 0f && mPos.x < 1f && mPos.y > 0f && mPos.y < 1f);

        RaycastHit hit;

        if (GameManager.LocalPlayer != null)
        {
            Physics.Linecast(target.position, PlayerCamera.Instance.transform.position, out hit);

            if (hit.collider != null)
                visible = false;

            if (Vector3.Distance(target.position, GameManager.LocalPlayer.Player.position) > 25)
                visible = false;
            
        }

        if (mVisible != visible)
        {
            mVisible = visible;
            UIWidget[] widgets = gameObject.GetComponentsInChildren<UIWidget>();
            foreach (UIWidget w in widgets) w.enabled = mVisible;
        }

        if (mVisible)
        {
            mPos = mUICam.ViewportToWorldPoint(mPos);
            mPos.z = 0f;
            mTrans.position = new Vector3(mPos.x - .14f, mPos.y, mPos.z);
        }
    }

}
