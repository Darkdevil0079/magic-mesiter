﻿using System;
using UnityEngine;


public class FloatingSpeech : FloatingElement
{
    public UILabel displayText;
    
    new void Awake()
    {
        base.Awake();

        _widget = GetComponent<UISprite>();
        if (_widget == null)
            Debug.LogError("Could not find the UISprite for the floating sprite.");

    }

    public void Init(string speechText)
    {
        displayText.text = speechText;
    }

    public void DisplayAnswers()
    {
        Destroy(this.gameObject);
    }


}

