using UnityEngine;

/// <summary>
/// Very simple example of how to use a TextList with a UIInput for chat.
/// </summary>

[RequireComponent(typeof(UIInput))]
public class ServerChatInput : MonoBehaviour
{
	public UITextList textList;

	UIInput mInput;

	/// <summary>
	/// Add some dummy text to the text list.
	/// </summary>

	void Start ()
	{
		mInput = GetComponent<UIInput>();
		mInput.label.maxLineCount = 1;

	}

	/// <summary>
	/// Submit notification is sent by UIInput when 'enter' is pressed or iOS/Android keyboard finalizes input.
	/// </summary>

	public void OnSubmit ()
	{
		if (textList != null)
		{
			// It's a good idea to strip out all symbols as we don't want user input to alter colors, add new lines, etc
			string text = NGUIText.StripSymbols(mInput.value);

			if (!string.IsNullOrEmpty(text))
			{
                GameManager.LocalPlayer.MeisterChat.chatInput = text;
                GameManager.LocalPlayer.MeisterChat.SendChat(PhotonTargets.All);
				mInput.value = "";
				mInput.isSelected = false;
                GameManager.LocalPlayer.InChat = false;
			}
		}
	}



}
