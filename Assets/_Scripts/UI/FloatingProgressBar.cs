﻿using UnityEngine;
using System.Collections;

public class FloatingProgressBar : MonoBehaviour
{
    /// <summary>
    /// Target object this UI element should follow.
    /// </summary>

    public Transform target;
    public Vector3 Offset;
    public UIProgressBar progress { get; set; }
    public UILabel progressText;
    public float Timer;

    private Transform mTrans;
    private Camera mGameCam;
    private Camera mUICam;
    private Vector3 mPos;
    private bool mVisible = true;
    private float EndTime;
    private float TimeLeft = 1f;
    

    void Start()
    {
        if (target == null) { Destroy(gameObject); return; }
        progress = GetComponent<UIProgressBar>();
        mTrans = transform;
        mGameCam = NGUITools.FindCameraForLayer(target.gameObject.layer);
        mUICam = NGUITools.FindCameraForLayer(gameObject.layer);
        EndTime = Time.time + Timer;
    }

    void LateUpdate()
    {
        if (target == null) { Destroy(gameObject); return; }

        mPos = mGameCam.WorldToViewportPoint(target.position + Offset);

        bool visible = (mPos.z > 0f && mPos.x > 0f && mPos.x < 1f && mPos.y > 0f && mPos.y < 1f);
        
        if (mVisible != visible)
        {
            mVisible = visible;
            UIWidget[] widgets = gameObject.GetComponentsInChildren<UIWidget>();
            foreach (UIWidget w in widgets) w.enabled = mVisible;
        }

        if (mVisible)
        {
            mPos = mUICam.ViewportToWorldPoint(mPos);
            mPos.z = 0f;
            mTrans.position = new Vector3(mPos.x - .14f, mPos.y, mPos.z);
        }

        Debug.Log(TimeLeft / Timer);

        TimeLeft = EndTime - Time.time;
        progress.value = TimeLeft / Timer;
        progressText.text = "CASTING - " + TimeLeft.ToString("0.##") + " SECS";


        if (TimeLeft < 0)
        {
            GameManager.LocalPlayer.Skills.IsCastTimer = false;
            DestroyImmediate(this.gameObject);
        }
    }
    
}
