﻿using System;
using UnityEngine;


public class FloatingText : FloatingElement
{
    private UILabel _lbl;

    public float Alpha
    {
        get
        {
            return this._lbl.alpha;
        }
        set
        {
            this._lbl.alpha = value;
        }
    }

    public string Text
    {
        get
        {
            return this._lbl.text;
        }
        set
        {
            this._lbl.text = value;
        }
    }

    public FloatingText()
    {
    }

    private new void Awake()
    {
        base.Awake();
        this._lbl = base.GetComponent<UILabel>();
        this._widget = this._lbl;
        if (this._widget == null)
        {
            Debug.LogError("Could not find the UILabel for the floating text.");
        }
    }

    public void Init(string text, Color clr)
    {
        this.Text = text;
        this.ElementColour = clr;
    }
}

