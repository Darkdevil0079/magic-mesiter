﻿using System;
using UnityEngine;


public class FloatingSprite : FloatingElement
{   
 
    new void Awake()
    {
        base.Awake();

        _widget = GetComponent<UISprite>();
        if (_widget == null)
            Debug.LogError("Could not find the UISprite for the floating sprite.");

    }

    public void Init(Color clr)
    {
        ElementColour = clr;
    }

}

