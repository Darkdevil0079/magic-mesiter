using UnityEngine;
using System.Collections;

public class FloatingElement : MonoBehaviour {

    protected UIWidget _widget;

    private bool _followTarget;

    private GameObject target;

    private Vector3 _pos;

    private Camera worldCamera;

    private Camera guiCamera;

    private Transform _t;

    public Vector3 Offset = Vector3.zero;

    public Color ElementColour
    {
        get
        {
            return this._widget.color;
        }
        set
        {
            this._widget.color = value;
        }
    }

    public Vector3 ElementSize
    {
        get
        {
            return this._widget.transform.localScale;
        }
        set
        {
            this._widget.transform.localScale = value;
        }
    }

    public bool FollowTarget
    {
        get
        {
            return this._followTarget;
        }
        set
        {
            this._followTarget = value;
        }
    }

    public GameObject Target
    {
        get
        {
            return this.target;
        }
        set
        {
            this.target = value;
            this.worldCamera = GameObject.FindGameObjectWithTag("MainCamera").GetComponent<Camera>();
            this.Following();
        }
    }

    public TweenColor TweenColor
    {
        get
        {
            TweenColor component = base.GetComponent<TweenColor>();
            if (component == null)
            {
                component = base.gameObject.AddComponent<TweenColor>();
            }
            return component;
        }
    }

    public TweenPosition TweenPosition
    {
        get
        {
            TweenPosition component = base.GetComponent<TweenPosition>();
            if (component == null)
            {
                component = base.gameObject.AddComponent<TweenPosition>();
            }
            return component;
        }
    }

    public TweenRotation TweenRotation
    {
        get
        {
            TweenRotation component = base.GetComponent<TweenRotation>();
            if (component == null)
            {
                component = base.gameObject.AddComponent<TweenRotation>();
            }
            return component;
        }
    }

    public TweenScale TweenScale
    {
        get
        {
            TweenScale component = base.GetComponent<TweenScale>();
            if (component == null)
            {
                component = base.gameObject.AddComponent<TweenScale>();
            }
            return component;
        }
    }

    public int ZIndex
    {
        get
        {
            return this._widget.depth;
        }
        set
        {
            this._widget.depth = value;
        }
    }

    public FloatingElement()
    {
    }

    public void Awake()
    {
        this._t = transform;
    }

    public void DestoryMe()
    {
        Object.Destroy(this.gameObject);
    }

    public void Following()
    {
        if (Target != null)
        {
            _pos = worldCamera.WorldToViewportPoint(Target.transform.position);

            if (guiCamera == null)
                guiCamera = NGUITools.FindCameraForLayer(gameObject.layer);

            _pos = guiCamera.ViewportToWorldPoint(_pos);
            _pos.z = 0f;
            transform.position = new Vector3(_pos.x + Offset.x, _pos.y + Offset.y, _pos.z);
        }

    }

    private void LateUpdate()
    {
        if (this.FollowTarget)
        {
            this.Following();
        }
    }

    public void SpawnAt(GameObject target, Vector3 size)
    {
        this.Target = target;
        this.ElementSize = size;
    }

    public void SpawnAt(GameObject target , int size)
    {
        this.Target = target;
        (this._widget as UILabel).fontSize = size;
    }

    public void SpawnAt(GameObject target)
    {
        this.Target = target;
    }

    private void Start()
    {
        if (this.guiCamera == null)
        {
            this.guiCamera = NGUITools.FindCameraForLayer(gameObject.layer);
        }
    }
}