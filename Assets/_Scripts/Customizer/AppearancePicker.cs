﻿using UnityEngine;
using System.Collections;

public class AppearancePicker : MonoBehaviour
{

		public UILabel PickerValues;
		public UILabel PickerLable;
		public string PickerUpdate;

		public int MaxValue;
		public int CurrentValue;

		// Use this for initialization
		void Start ()
		{	
		}
	
		// Update is called once per frame
		void FixedUpdate ()
		{
				SetupAppearancePicker ();
		}

		private void UpdatePickerValue ()
		{
				PickerValues.text = CurrentValue + " / " + MaxValue;

				switch (PickerUpdate) {
				case "SKIN":
                        PlayerDataStorage.Instance.SkinColourIndex = CurrentValue;
						break;
				case "HAIRS":
                        PlayerDataStorage.Instance.HairStyleIndex = CurrentValue;
						break;
				case "HAIRC":
                        PlayerDataStorage.Instance.HairColorIndex = CurrentValue;
						break;
				case "EYES":
                        PlayerDataStorage.Instance.EyeStyleIndex = CurrentValue;
						break;
				case "EYEC":
                        PlayerDataStorage.Instance.EyeColorIndex = CurrentValue;
						break;
				case "CLOTHSC":
                        CustomizerManager.Instance.CurrentClothsPreview = CurrentValue;
						break;
				}

				CustomizerManager.Instance.PlayerMeister.SetupPlayerAppearance ();
		}

		public void SelectPreValue ()
		{
				if (CurrentValue == 1) {
						CurrentValue = MaxValue;
				} else
						CurrentValue --;
				UpdatePickerValue ();
		}

		public void SelectNextValue ()
		{
				if (CurrentValue == MaxValue) {
						CurrentValue = 1;
				} else
						CurrentValue ++;
				UpdatePickerValue ();
		}

		private void SetupAppearancePicker ()
		{
				if (CustomizerResource.Instance == null) {
						return;
				}

				CustomizerResource _cm = CustomizerResource.Instance;
				this.name = "AP" + PickerUpdate;
			
				switch (PickerUpdate) {
				case "SKIN":
						PickerLable.text = "SKIN COLOR"; 
						MaxValue = _cm.PlayerSkins.Count;
                        CurrentValue = PlayerDataStorage.Instance.SkinColourIndex;
						break;
				case "HAIRS":
						PickerLable.text = "HAIR STYLES";
						if (PlayerDataStorage.Instance.Gender == "m") 
								MaxValue = _cm.PlayerHairMale.Count;
						else
								MaxValue = _cm.PlayerHairFemale.Count;
                        CurrentValue = PlayerDataStorage.Instance.HairStyleIndex;
						break;
				case "HAIRC":
						PickerLable.text = "HAIR COLOR";
						MaxValue = _cm.PlayerHairColours;
                        CurrentValue = PlayerDataStorage.Instance.HairColorIndex;
						break;
				case "EYES":
						PickerLable.text = "EYE STYLES";

                        if (PlayerDataStorage.Instance.Gender == "m") 
								MaxValue = _cm.PlayerEyesMale.Count;
						else
								MaxValue = _cm.PlayerEyesFemale.Count;
                        CurrentValue = PlayerDataStorage.Instance.EyeStyleIndex;
						break;
				case "EYEC":
						PickerLable.text = "EYE COLOR";
						MaxValue = _cm.PlayerEyeColours;
                        CurrentValue = PlayerDataStorage.Instance.EyeColorIndex;
						break;
				case "CLOTHSC":
						PickerLable.text = "PREVIEW";
						MaxValue = _cm.PlayerCloths;
						CurrentValue = CustomizerManager.Instance.CurrentClothsPreview;
						break;
				}
				
				PickerValues.text = CurrentValue + " / " + MaxValue;
                CustomizerManager.Instance.PlayerMeister.SetupPlayerAppearance();
		}
}
