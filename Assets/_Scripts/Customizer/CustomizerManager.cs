﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomizerManager : MonoBehaviour
{
    public static CustomizerManager Instance;

    public MeisterController PlayerMeister;
    public Transform Stage;
    public float StageSpeed = 100f;

    public UIInput PlayerName;
    public UILabel MageTitle;
    public UILabel MageDescription;
    public UILabel MainSkillName;
    public UILabel SecendSkillName;

    public UISprite MainSkillIcon;
    public UISprite SecondSkillIcon;

    public UISprite EmberIcon;
    public UISprite FrostIcon;
    public UISprite ValeIcon;
    public UISprite ShadowIcon;
    public UISprite SpirtIcon;

    public int CurrentClothsPreview = 1;
    public UIToggle HatCheck;

    public GameObject EnterWorld;
    public UILabel DownloadProgress;

    public string PlayerElement;
    public bool IsMale;

    private bool RotateStageLeft;
    private bool RotateStageRight;
    private bool LockRotatation;


    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        LockRotatation = false;
        EnterWorld.SetActive(false);
        IsMale = true;
        PlayerDataStorage.Instance.LoadPlayerData();
    }

    // Update is called once per frame
    void Update()
    {
        if (!LockRotatation)
        {
            if (RotateStageLeft)
        {
            Stage.Rotate(0, StageSpeed * Time.deltaTime, 0);
        }

        if (RotateStageRight)
        {
            Stage.Rotate(0, -StageSpeed * Time.deltaTime, 0);
        }
        }
        
        if (EnterWorld.active)
        {
            float percentageLoaded = Application.GetStreamProgressForLevel(GameSettings.TestingLevel) * 100;
            DownloadProgress.text = "Building ....";
        }
    }

    public void SwitchGender(GameObject sender)
    {
        SwitchGender(sender.name);
    }

    public void SwitchGender(string value)
    {
        if (value == "Male")
            PlayerDataStorage.Instance.Gender = "m";
        else
            PlayerDataStorage.Instance.Gender = "f";

        PlayerMeister.SetupPlayerAppearance();
    }

    public void TurnLeftPress()
    {
        RotateStageLeft = true;
    }

    public void TurnLeftRelease()
    {
        RotateStageLeft = false;
    }

    public void TurnRightPress()
    {
        RotateStageRight = true;
    }

    public void TurnRightRelease()
    {
        RotateStageRight = false;
    }

    public void ResetPlayer()
    {
        Stage.rotation = Quaternion.Euler(new Vector3(0, 325.1487f, 0));
    }

    public void UpdateMageDetails(emElementType Type)
    {
        switch (Type)
        {
            case emElementType.Ember:
                MageTitle.text = "EMBER MEISTERS";
                MageDescription.text = "Strong willed and ferocious, The Ember Meisters take on their opponents directly and prefer causing damage and burns that last over time.";
                MainSkillName.text = SkillDatabase.FindByID(0).name;
                MainSkillIcon.spriteName = SkillDatabase.FindByID(0).iconName;
                SecendSkillName.text = SkillDatabase.FindByID(6).name;
                SecondSkillIcon.spriteName = SkillDatabase.FindByID(6).iconName;
                EmberIcon.spriteName = "ember_icon_selected";
                FrostIcon.spriteName = "frost_icon";
                ValeIcon.spriteName = "vale_icon";
                ShadowIcon.spriteName = "shadow_icon";
                SpirtIcon.spriteName = "spirit_icon";
                break;
            case emElementType.Frost:
                MageTitle.text = "FROST MEISTERS";
                MageDescription.text = "Calculating and cold, The Frost Meisters are formidable foes at locking up their opponents and keeping their distance while casting their spells.";
                MainSkillName.text = SkillDatabase.FindByID(15).name;
                MainSkillIcon.spriteName = SkillDatabase.FindByID(15).iconName;
                SecendSkillName.text = SkillDatabase.FindByID(18).name;
                SecondSkillIcon.spriteName = SkillDatabase.FindByID(18).iconName;
                EmberIcon.spriteName = "ember_icon";
                FrostIcon.spriteName = "frost_icon_selected";
                ValeIcon.spriteName = "vale_icon";
                ShadowIcon.spriteName = "shadow_icon";
                SpirtIcon.spriteName = "spirit_icon";
                break;
            case emElementType.Vale:
                MageTitle.text = "VALE MEISTERS";
                MageDescription.text = "Using the spirit of nature to strengthen their magic, The Vale Meisters know how to conjure up spells that cause immense knockback and direct damage.";
                MainSkillName.text = SkillDatabase.FindByID(30).name;
                MainSkillIcon.spriteName = SkillDatabase.FindByID(30).iconName;
                SecendSkillName.text = SkillDatabase.FindByID(35).name;
                SecondSkillIcon.spriteName = SkillDatabase.FindByID(35).iconName;
                EmberIcon.spriteName = "ember_icon";
                FrostIcon.spriteName = "frost_icon";
                ValeIcon.spriteName = "vale_icon_selected";
                ShadowIcon.spriteName = "shadow_icon";
                SpirtIcon.spriteName = "spirit_icon";
                break;
            case emElementType.Shadow:
                MageTitle.text = "SHADOW MEISTERS";
                MageDescription.text = "The shadow meisters of the world use powerful dark magic to cause strange and dangerous effects!";
                MainSkillName.text = "Death Touch";
                MainSkillIcon.spriteName = "death touch";
                SecendSkillName.text = "Shadow Ball";
                SecondSkillIcon.spriteName = "shadow ball";
                EmberIcon.spriteName = "ember_icon";
                FrostIcon.spriteName = "frost_icon";
                ValeIcon.spriteName = "vale_icon";
                ShadowIcon.spriteName = "shadow_icon_selected";
                SpirtIcon.spriteName = "spirit_icon";
                break;
            case emElementType.Spirt:
                MageTitle.text = "SPIRIT MEISTERS";
                MageDescription.text = "The spirit Meisters fight by paralyzing their enemies and using their magic to heal themselves";
                MainSkillName.text = "ReDirect";
                MainSkillIcon.spriteName = "redirect";
                SecendSkillName.text = "Holy Light";
                SecondSkillIcon.spriteName = "holy light";
                EmberIcon.spriteName = "ember_icon";
                FrostIcon.spriteName = "frost_icon";
                ValeIcon.spriteName = "vale_icon";
                ShadowIcon.spriteName = "shadow_icon";
                SpirtIcon.spriteName = "spirit_icon_selected";
                break;
            default:
                MageTitle.text = "";
                MageDescription.text = "";
                MainSkillName.text = "";
                MainSkillIcon.spriteName = "";
                SecendSkillName.text = "";
                SecondSkillIcon.spriteName = "";
                EmberIcon.spriteName = "ember_icon";
                FrostIcon.spriteName = "frost_icon";
                ValeIcon.spriteName = "vale_icon";
                ShadowIcon.spriteName = "shadow_icon";
                SpirtIcon.spriteName = "spirt_icon";
                break;
        }
        PlayerDataStorage.Instance.PlayerElement = Type;
    }

    public void CreateRandomAppearance(bool FullCustomize)
    {

        if (FullCustomize)
        {

            int ElementID = Random.Range(1, 5);

            switch (ElementID)
            {
                case 1:
                    UpdateMageDetails(emElementType.Ember);
                    break;
                case 2:
                    UpdateMageDetails(emElementType.Frost);
                    break;
                case 3:
                    UpdateMageDetails(emElementType.Vale);
                    break;
                case 4:
                    UpdateMageDetails(emElementType.Shadow);
                    break;
                case 5:
                    UpdateMageDetails(emElementType.Spirt);
                    break;
            }

        }

        CustomizerResource _cm = CustomizerResource.Instance;

        PlayerDataStorage.Instance.SkinColourIndex = Random.Range(1, _cm.PlayerSkins.Count + 1);

        if (PlayerDataStorage.Instance.Gender == "m")
        {
            PlayerDataStorage.Instance.HairStyleIndex = Random.Range(1, _cm.PlayerHairMale.Count + 1);
            PlayerDataStorage.Instance.EyeStyleIndex = Random.Range(1, _cm.PlayerEyesMale.Count + 1);
        }
        else
        {
            PlayerDataStorage.Instance.HairStyleIndex = Random.Range(1, _cm.PlayerHairFemale.Count + 1);
            PlayerDataStorage.Instance.EyeStyleIndex = Random.Range(1, _cm.PlayerEyesFemale.Count + 1);
        }

        PlayerDataStorage.Instance.EyeColorIndex = Random.Range(1, _cm.PlayerEyeColours + 1);
        PlayerDataStorage.Instance.HairColorIndex = Random.Range(1, _cm.PlayerHairColours + 1);

        PlayerMeister.SetupPlayerAppearance();
    }

    public void RandomAppearance()
    {
        CreateRandomAppearance(false);
    }

    public void FullRandomAppearance()
    {
        CreateRandomAppearance(true);
    }

    public void GoToGame()
    {
        LockRotatation = true;
        EnterWorld.SetActive(true);
        PlayerDataStorage.Instance.PlayerName = PlayerName.value;
        PlayerDataStorage.Instance.SetupNewPlayerAccount();
        Application.LoadLevel(GameSettings.TestingLevel);
    }
    
    public void HatCheckChange()
    {
        PlayerMeister.ShowHatPreview = !PlayerMeister.ShowHatPreview;
    }

}
