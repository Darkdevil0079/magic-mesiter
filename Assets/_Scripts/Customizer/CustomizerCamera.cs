using UnityEngine;
using System.Collections;

public class CustomizerCamera : MonoBehaviour
{
		public float distance = 10.0f;
	
		public float xSpeed = 250.0f;
		public float ySpeed = 120.0f;
	
		public float yMinLimit = -20f;
		public float yMaxLimit = 80f;

		public float CameraOffset = 0f;
	
		private float x = 0.0f;
		private float y = 0.0f;
		private Vector3 StartPos;
		private Quaternion StartRot;	

		void Start ()
		{
				StartPos = transform.position;
				StartRot = transform.rotation;

				var angles = transform.eulerAngles;
				x = angles.y;
				y = angles.x;
		
				// Make the rigid body not change rotation
				if (rigidbody)
						rigidbody.freezeRotation = true;
		}
	
		void LateUpdate ()
		{
            if (CustomizerManager.Instance.PlayerMeister.TargetLookAt)
            {
						if (Input.GetMouseButton (2)) {
								x += Input.GetAxis ("Mouse X") * xSpeed * 0.02f;
								y -= Input.GetAxis ("Mouse Y") * ySpeed * 0.02f;
				
								y = Helper.ClampAngle (y, yMinLimit, yMaxLimit);
				
								var rotation = Quaternion.Euler (y, x, 0);
                                var position = rotation * new Vector3(0.0f, 0.0f + CameraOffset, -distance) + CustomizerManager.Instance.PlayerMeister.TargetLookAt.transform.position;
				
								transform.rotation = rotation;
								transform.position = position;			

						}
					
				}

				
		}

		public void ResetCamera ()
		{
				transform.position = StartPos;
				transform.rotation = StartRot;
		}
}

