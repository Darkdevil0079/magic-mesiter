﻿using UnityEngine;
using System.Collections;

public class CharactorItem : MonoBehaviour
{

		private UISprite ItemIcon;
		private UIDragDropContainer DropTarget;
	
		public UILabel NoItemText;
		

		// Use this for initialization
		void Start ()
		{
				ItemIcon = GetComponent<UISprite> ();
				DropTarget = GetComponent<UIDragDropContainer> ();
		}
	
		// Update is called once per frame
		void Update ()
		{
	
		}
		
		public void SetupItemIcon (string spriteName)
		{
				ItemIcon.spriteName = spriteName;
		}
}
