using UnityEngine;
using System.Collections;
using System.Xml;
using System.Collections.Generic;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

public class PlayerDataStorage : MonoBehaviour
{

    public static PlayerDataStorage Instance;

    public const string PLAYERINVENTORY = "playerInventory";
    public const string PLAYEREQUIPMENT = "playerEquipment";
    private const string PLAYERSKILLS = "playerSkilles";
    private const string PLAYERSPELLPOINTS = "spellPoints";
    private const string PLAYERABILITYPOINTS = "abilityPoints";

    private const string PLAYERCURRENTLEVEL = "currentLevel";
    private const string PLAYERCURRENTXP = "currentXP";

    public string PlayerName;
    public emElementType PlayerElement;
    public string Gender;
    public bool SetupPlayer;

    public int SkinColourIndex;
    public int EyeStyleIndex;
    public int EyeColorIndex;
    public int HairColorIndex;
    public int HairStyleIndex;

    public bool WearingHat;

    public int CurrentXP;
    public int CurrentLevel;
    public float XPLeft;

    public int SpellPoints;
    public int AbilityPoints;

    public int CurrentCoins;

    public Vector3 LastKnowPos;

    public Vector3 BackpackWindowPos = new Vector3(-429.7852f,-48.17959f,0);
    public Vector3 CharactorWindowPos = new Vector3(391.1133f,100.0767f,0);
    public Vector3 SkillTreeWindowPos = new Vector3(0,0,0);

    public List<PlayerSkill> PlayerSkills = new List<PlayerSkill>();
    public List<PlayerItemEquiped> WearingEquipment = new List<PlayerItemEquiped>();
    public List<PlayerInventory> PlayerInv = new List<PlayerInventory>();


    void Awake()
    {
        Instance = this;
        LoadPlayerData();
    }

    void Start()
    {
        
    }

    public void LoadPlayerData()
    {

        SetupPlayer = Convert.ToBoolean(PlayerPrefs.GetInt("SetupPlayer", 0));
        PlayerName = PlayerPrefs.GetString("PlayerName", "");
        PlayerElement = (emElementType)System.Enum.Parse(typeof(emElementType), PlayerPrefs.GetString("PlayerElement", "None"));
        Gender = PlayerPrefs.GetString("PlayerGender", "m");

        SkinColourIndex = PlayerPrefs.GetInt("SkinColourIndex", 1);

        EyeStyleIndex = PlayerPrefs.GetInt("EyeStyleIndex", 1);
        EyeColorIndex = PlayerPrefs.GetInt("EyeColorIndex", 1);
        HairColorIndex = PlayerPrefs.GetInt("HairColorIndex", 1);
        HairStyleIndex = PlayerPrefs.GetInt("HairStyleIndex", 1);

        WearingHat = Convert.ToBoolean(PlayerPrefs.GetInt("WearingHat", 0));
        
        CurrentCoins = PlayerPrefs.GetInt("PlayerCurrentCoins", 0);

        LoadHiddenStringFromPlayerPrefs(PLAYERABILITYPOINTS);
        LoadHiddenStringFromPlayerPrefs(PLAYERSPELLPOINTS);
        LoadHiddenStringFromPlayerPrefs(PLAYERCURRENTLEVEL);
        LoadHiddenStringFromPlayerPrefs(PLAYERCURRENTXP);

        LoadPlayerEquipment();
        LoadPlayerItemStorage();
        LoadPlayerSkills();
    }

    internal void LoadPlayerEquipment()
    {
        var data = PlayerPrefs.GetString(PlayerDataStorage.PLAYEREQUIPMENT);

        if (!string.IsNullOrEmpty(data))
        {
            var b = new BinaryFormatter();
            var m = new MemoryStream(Convert.FromBase64String(data));

            WearingEquipment = (List<PlayerItemEquiped>)b.Deserialize(m);

        }
    }

    internal void LoadPlayerSkills()
    {
        var data = PlayerPrefs.GetString(PlayerDataStorage.PLAYERSKILLS);

        if (!string.IsNullOrEmpty(data))
        {
            var b = new BinaryFormatter();
            var m = new MemoryStream(Convert.FromBase64String(data));

            PlayerSkills = (List<PlayerSkill>)b.Deserialize(m);

        }
    }

    internal void LoadPlayerItemStorage()
    {
        var data = PlayerPrefs.GetString(PlayerDataStorage.PLAYERINVENTORY);

        if (!string.IsNullOrEmpty(data))
        {
            var b = new BinaryFormatter();
            //Create a memory stream with the data
            var m = new MemoryStream(Convert.FromBase64String(data));
            PlayerInv = (List<PlayerInventory>)b.Deserialize(m);
        }
    }

    public void SavePlayerLevelInfo()
    {
        SaveListDataToPlayerPrefs(CurrentLevel, PLAYERCURRENTLEVEL);
        SaveListDataToPlayerPrefs(CurrentXP, PLAYERCURRENTXP);
        SaveListDataToPlayerPrefs(SpellPoints, PLAYERSPELLPOINTS);
        SaveListDataToPlayerPrefs(AbilityPoints, PLAYERABILITYPOINTS);
        PlayerPrefs.SetInt("PlayerCurrentCoins", CurrentCoins);

      
    }

    public void SavePlayerCustomizer()
    {
        PlayerPrefs.SetString("PlayerName", PlayerName);
        PlayerPrefs.SetInt("SkinColourIndex", SkinColourIndex);
        PlayerPrefs.SetInt("EyeStyleIndex", EyeStyleIndex);
        PlayerPrefs.SetInt("EyeColorIndex", EyeColorIndex);
        PlayerPrefs.SetInt("HairStyleIndex", HairStyleIndex);
        PlayerPrefs.SetInt("HairColorIndex", HairColorIndex);

        PlayerPrefs.SetInt("SetupPlayer", Convert.ToInt16(SetupPlayer));
        PlayerPrefs.SetInt("WearingHat", Convert.ToInt16(WearingHat));
        PlayerPrefs.SetString("PlayerElement", PlayerElement.ToString());
        PlayerPrefs.SetString("PlayerGender", Gender);

    }

    private void SavePlayerEquipment()
    {
        SaveListDataToPlayerPrefs(WearingEquipment, PLAYEREQUIPMENT);
    }

    private void SavePlayerItemStorage()
    {
        SaveListDataToPlayerPrefs(PlayerInv, PLAYERINVENTORY);
    }

    private void SavePlayerSkills()
    {
        SaveListDataToPlayerPrefs(PlayerSkills, PLAYERSKILLS);
    }

    /// <summary>
    /// Saves the list data to player prefs.
    /// </summary>
    /// <param name="objectToSave">The object to save.</param>
    /// <param name="prefName">Name of the preference.</param>

    internal static void SaveListDataToPlayerPrefs(object objectToSave , string prefName)
    {
        //Get a binary formatter
        var b = new BinaryFormatter();
        //Create an in memory stream
        var m = new MemoryStream();
        b.Serialize(m, objectToSave);
        //Add it to player prefs
        PlayerPrefs.SetString(prefName,
            Convert.ToBase64String(
                m.GetBuffer()
            )
        );
    }
    
    internal void LoadListDateFromPlayerPrefs(string prefName)
    {
        var data = PlayerPrefs.GetString(prefName);

        if (!string.IsNullOrEmpty(data))
        {
            var b = new BinaryFormatter();
            //Create a memory stream with the data
            var m = new MemoryStream(Convert.FromBase64String(data));

            switch (prefName)
            {
                case PLAYERSKILLS:
                    PlayerSkills = (List<PlayerSkill>)b.Deserialize(m);
                    break;
            }

        }
    }

    private void LoadHiddenStringFromPlayerPrefs(string prefName)
    {
        var data = PlayerPrefs.GetString(prefName);

        if (!string.IsNullOrEmpty(data))
        {
            var b = new BinaryFormatter();
            //Create a memory stream with the data
            var m = new MemoryStream(Convert.FromBase64String(data));

            switch (prefName)
            {
                case PLAYERSPELLPOINTS:
                    SpellPoints = (int)b.Deserialize(m);
                    break;
                case PLAYERABILITYPOINTS:
                    AbilityPoints = (int)b.Deserialize(m);
                    break;
                case PLAYERCURRENTLEVEL:
                    CurrentLevel = (int)b.Deserialize(m);
                    break;
                case PLAYERCURRENTXP:
                    CurrentXP = (int)b.Deserialize(m);
                    break;
            }

        }
    }

    internal void SaveAll()
    {
        SavePlayerCustomizer();
        SavePlayerLevelInfo();
        SavePlayerEquipment();
        SavePlayerItemStorage();
        SavePlayerSkills();
        PlayerPrefs.Save();
    }

    public void SetupNewPlayerAccount()
    {
        WearingEquipment.Clear();

        WearingEquipment.Add(new PlayerItemEquiped { Grade = InvGameItem.Quality.Common, ItemID = 0, Level = 1, Slot = InvBaseItem.Slot.Hat });
        WearingEquipment.Add(new PlayerItemEquiped { Grade = InvGameItem.Quality.Common, ItemID = 4, Level = 1, Slot = InvBaseItem.Slot.Clothes });
        WearingEquipment.Add(new PlayerItemEquiped { Grade = InvGameItem.Quality.Common, ItemID = 3, Level = 1, Slot = InvBaseItem.Slot.Gloves });
        WearingEquipment.Add(new PlayerItemEquiped { Grade = InvGameItem.Quality.Common, ItemID = 2, Level = 1, Slot = InvBaseItem.Slot.Boots });

        PlayerInv.Clear();

        PlayerInventory ItemToAdd = new PlayerInventory();
        ItemToAdd.ItemID = 1;
        ItemToAdd.ItemLevel = 1;
        ItemToAdd.ItemGrade = InvGameItem.Quality.Common;
        ItemToAdd.Slot = 0;
        PlayerInv.Add(ItemToAdd);

        WearingHat = true;
        SetupPlayer = true;

        CurrentXP = 0;
        CurrentLevel = 0;
        CurrentCoins = 0;
        SpellPoints = 5;
        AbilityPoints = 5;

        PlayerSkills.Clear();

        switch (PlayerElement)
        {
            case emElementType.Ember:
                PlayerSkills.Add(new PlayerSkill { SkillId = 0, SkillLevel = 1, HotBarId = 1, PosId = 1 });
                break;
            case emElementType.Frost:
                PlayerSkills.Add(new PlayerSkill { SkillId = 16, SkillLevel = 1, HotBarId = 1, PosId = 1 });
                break;
            case emElementType.Vale:
                PlayerSkills.Add(new PlayerSkill { SkillId = 30, SkillLevel = 1, HotBarId = 1, PosId = 1 });
                break;
            case emElementType.Shadow:

                break;
            case emElementType.Spirt:

                break;
            case emElementType.None:
                break;
            default:
                break;
        }

        SavePlayerCustomizer();
        SavePlayerLevelInfo();
    }

    void OnApplicationQuit()
    {
        SaveAll();
    }
}

