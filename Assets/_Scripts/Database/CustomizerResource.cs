using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CustomizerResource : MonoBehaviour
{
		public static CustomizerResource Instance;

		public List<Texture2D> PlayerSkins;
		public List<Texture2D> PlayerHeadSkins;
		public List<GameObject> PlayerGloves;
		public List<GameObject> PlayerBoots;
		public List<Texture2D> PlayerEyesMale;
		public List<Texture2D> PlayerEyesFemale;
		public List<GameObject> PlayerHairMale;
		public List<GameObject> PlayerHairFemale;
	
		public int PlayerHairColours;
		public int PlayerEyeColours;
		public int PlayerCloths;

		void Awake ()
		{
                if (Instance != null && Instance != this)
                {
                    Destroy(gameObject);
                }
                else
                {
                    DontDestroyOnLoad(gameObject);
                    Instance = this;
                }

		}
}

