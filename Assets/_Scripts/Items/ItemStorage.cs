﻿using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using System;

/// <summary>
/// Storage container that stores items.
/// </summary>

public class ItemStorage : MonoBehaviour
{
    public static ItemStorage LocalItemStorage;

    public int maxItemCount = 8;
    public int maxRows = 4;
    public int maxColumns = 4;
    public GameObject template;
    public UIWidget background;
    public int spacing = 128;
    public int padding = 10;

    private Transform storageHolder;

    void Awake()
    {
        LocalItemStorage = this;
        storageHolder = this.transform;
    }

    public void AddItemToSlot(PlayerInventory Item)
    {
        PlayerDataStorage.Instance.PlayerInv.Add(Item);
        UpdateInvGrid();
    }

    public void BuildInvGird()
    {

        foreach (Transform clear in transform)
        {
            NGUITools.SetActive(clear.gameObject, false);
            DestroyObject(clear.gameObject);
        }

        if (template != null)
        {
            int count = 0;
            Bounds b = new Bounds();

            for (int y = 0; y < maxRows; ++y)
            {
                for (int x = 0; x < maxColumns; ++x)
                {
                    GameObject go = NGUITools.AddChild(gameObject, template);
                    Transform t = go.transform;
                    t.localPosition = new Vector3(padding + (x + 0.5f) * spacing, -padding - (y + 0.5f) * spacing, 0f);
                    go.name = "StorageSlot" + count;

                    ItemStorageSlot slot = go.GetComponent<ItemStorageSlot>();

                    if (slot != null)
                    {
                        slot.slot = count;
                        slot.slotItem = PlayerDataStorage.Instance.PlayerInv.Where(w => w.Slot == slot.slot).FirstOrDefault();
                    }

                    b.Encapsulate(new Vector3(padding * 2f + (x + 1) * spacing, -padding * 2f - (y + 1) * spacing, 0f));

                    if (++count >= maxItemCount)
                    {
                        if (background != null)
                        {
                            background.transform.localScale = b.size;
                        }
                        return;
                    }
                }
            }
            if (background != null)
                background.transform.localScale = b.size;
        }
    }

    internal void RemoveItemFromStorage(PlayerInventory Item)
    {
        PlayerDataStorage.Instance.PlayerInv.Remove(Item);
        UpdateInvGrid();
    }

    internal void UpdateInvGrid()
    {
        foreach (Transform InvItem in transform)
        {
            ItemStorageSlot slot = InvItem.GetComponent<ItemStorageSlot>();

            if (slot != null)
            {
                PlayerInventory foundItem = PlayerDataStorage.Instance.PlayerInv.Where(w => w.Slot == slot.slot).FirstOrDefault();
                if (foundItem != null)
                    slot.slotItem = foundItem;
                else
                    slot.slotItem = null;
            }
        }

        GameManager.LocalPlayer.SetupPlayerAppearance();

    }

    internal void SwapStorageSlot(PlayerInventory SlotItem , ItemStorageSlot StorageSlot)
    {

        PlayerInventory oldItem;

        if (StorageSlot.slotItem != null)
        {
            oldItem = PlayerDataStorage.Instance.PlayerInv.Single(s => s == StorageSlot.slotItem);
            PlayerDataStorage.Instance.PlayerInv.Single(s => s == oldItem).Slot = SlotItem.Slot;
        }

        PlayerDataStorage.Instance.PlayerInv.Single(s => s == SlotItem).Slot = StorageSlot.slot;

    }

    internal int FindFirstFreeSlot()
    {
        for (int i = 0; i < storageHolder.GetChildCount() - 1; i++)
        {
            
            Transform foundSlot = storageHolder.FindChild("StorageSlot" + i);
            if (foundSlot != null)
	        {
                ItemStorageSlot slot = foundSlot.GetComponent<ItemStorageSlot>();
                if (slot.slotItem == null)
                {
                    return slot.slot;
                }
	        }
        }

        return -1;
    }
}