﻿using UnityEngine;
using System.Collections;

public class CoinSack : MonoBehaviour {

    private int CoinValue;

    public void SetRandomCoinValue()
    {
        CoinValue = UnityEngine.Random.Range(1, 10) * PlayerDataStorage.Instance.CurrentLevel;
    }

    public void SetCoinValue(int Value)
    {
        CoinValue = Value;
    }

    public void CollectCoins()
    {

    }
}
