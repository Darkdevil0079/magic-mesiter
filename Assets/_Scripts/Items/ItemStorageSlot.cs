﻿//----------------------------------------------
//            NGUI: Next-Gen UI kit
// Copyright © 2011-2014 Tasharen Entertainment
//----------------------------------------------

using UnityEngine;

/// <summary>
/// A UI script that keeps an eye on the slot in a storage container.
/// </summary>

public class ItemStorageSlot : DragDropItem
{
    public PlayerInventory slotItem { get; set; }
	public int slot;

    public UISprite slotIcon;

    // Use this for initialization
    new void Start()
    {
        cloneOnDrag = true;
        ItemToClone = slotIcon.gameObject;
        base.Start();
    }

    void Update()
    {
        if (slotItem != null)
        {
            InvBaseItem baseItem = InvDatabase.FindByID(slotItem.ItemID);

            if (slotIcon != null)
            {
                if (baseItem == null || baseItem.iconAtlas == null)
                {
                    slotIcon.enabled = false;
                }
                else
                {
                    slotIcon.atlas = baseItem.iconAtlas;
                    slotIcon.spriteName = baseItem.iconName;
                    slotIcon.enabled = true;
                }
            }
        }        
        else
        {
            slotIcon.enabled = false;
        }

        ItemData = slotItem;
    }

    internal void MoveItemToStorage(PlayerItemEquiped equipedItem )
    {
        PlayerInventory ItemToAdd = new PlayerInventory();

        ItemToAdd.ItemID = equipedItem.ItemID;
        ItemToAdd.ItemLevel = equipedItem.Level;
        ItemToAdd.ItemGrade = equipedItem.Grade;
        ItemToAdd.Slot = slot;

        ItemStorage.LocalItemStorage.AddItemToSlot(ItemToAdd);
    }
}