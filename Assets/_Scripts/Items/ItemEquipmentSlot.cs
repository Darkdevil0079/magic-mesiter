﻿using UnityEngine;
using System.Linq;

/// <summary>
/// A UI script that keeps an eye on the slot in character equipment.
/// </summary>

public class ItemEquipmentSlot : DragDropItem
{
		public InvBaseItem.Slot slot;
		public UILabel SlotLable;
        public UISprite SlotIcon;

        private GameObject dragIcon;
        public PlayerItemEquiped SlotItem { get; set; }

        // Use this for initialization
        new void Start()
        {
            cloneOnDrag = true;
            ItemToClone = SlotIcon.gameObject;
            base.Start();
        }
		
		void Update()
        {
            SlotItem = PlayerDataStorage.Instance.WearingEquipment.Where(w => w.Slot == slot).FirstOrDefault();
            if (SlotItem != null)
            {
                SlotIcon.enabled = true;
                SlotIcon.atlas = InvDatabase.FindByID(SlotItem.ItemID).iconAtlas;
                SlotIcon.spriteName = InvDatabase.FindByID(SlotItem.ItemID).iconName;
                SlotLable.enabled = false;
            }
            else
            {
                SlotIcon.enabled = false;
                SlotLable.enabled = true;

            }

            ItemData = SlotItem;
        }
        
}