﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Chest : MonoBehaviour {

    public AudioClip SFX_OpenChest;
    public AudioClip SFX_ShakeChest;
    public AudioClip SFX_CloseChest;

    public enum emChestState
    {
        open,
        closed,
        between
    }

    public emChestState state;

    private Transform _chest;
    private ChestLootViewer FW;
    private AudioSource SFX;
    public List<int> ChestItems;

	// Use this for initialization
	void Start () {

        SFX = GetComponent<AudioSource>();
        _chest = this.transform;
        state = emChestState.closed;

        if (ChestItems == null)
        {
            GenerateChestItems();
        }

	}

    private void GenerateChestItems()
    {
        
    }
	
	// Update is called once per frame
	void Update () {
	
	}

    public void OnMouseUp()
    {

        if (Vector3.Distance(_chest.position , GameManager.LocalPlayer.Player.position) < 11)
        {
            switch (state)
            {
                case emChestState.open:
                    state = emChestState.between;
                    StartCoroutine(CloseChest());
                    break;
                case emChestState.closed:
                    state = emChestState.between;
                    StartCoroutine(OpenChest());

                    break;
                case emChestState.between:
                    break;
            }
        }

       

    }

    private IEnumerator OpenChest()
    {
        if (!SFX.isPlaying)
        {
            SFX.clip = SFX_OpenChest;
            SFX.Play();
        }

        animation["OpenChest"].speed = 1;
        animation.Play("OpenChest");

        yield return new WaitForSeconds(animation["OpenChest"].length);
        state = emChestState.open;

        OpenChestLootWindow();

    }

    private void OpenChestLootWindow()
    {
        GameObject LootWindow = Resources.Load("UI/ChestLootWindow") as GameObject;

        if (LootWindow != null)
        {
            UICamera cam = UICamera.FindCameraForLayer(LootWindow.layer);

            if (cam != null)
            {
                GameObject go = cam.gameObject;
                UIAnchor anchor = go.GetComponent<UIAnchor>();
                if (anchor != null) go = anchor.gameObject;

                GameObject FHBObj = GameObject.Instantiate(LootWindow) as GameObject;
                Transform t = FHBObj.transform;
                t.gameObject.name = "LootWindow" + t.gameObject.name;
                t.parent = go.transform;
                t.localPosition = Vector3.zero;
                t.localRotation = Quaternion.identity;
                t.localScale = new Vector3(.8f, .8f, .8f);

                FW = FHBObj.GetComponent<ChestLootViewer>();
                if (FW != null)
                {
                    FW.ChestObject = this;
                    FW.target = _chest.FindChild("InfoHolder").transform;
                    FW.IconList = ChestItems;
                }
            }
            else
            {
                Debug.LogWarning("No camera found for layer " + LayerMask.LayerToName(LootWindow.layer), gameObject);
            }

        }
    }

    private IEnumerator CloseChest()
    {
        if (!SFX.isPlaying)
        {
            SFX.clip = SFX_CloseChest;
            SFX.Play();
        }

        animation["OpenChest"].speed = -1;
        animation.Play("OpenChest");

        yield return new WaitForSeconds(animation["OpenChest"].length);
        state = emChestState.closed;

        if (FW != null)
            Destroy(FW.gameObject);
        
    }

    private void DestoryChest()
    {

    }


    internal void ForceCloseChest()
    {
        StartCoroutine(CloseChest());
    }
}

public class ChestItem
{
    int ItemId;
    int ItemQty;
}
