﻿using UnityEngine;
using System.Collections;

public class InGameMenu : MonoBehaviour
{
    public UISprite QuestLog;
    public UISprite SkillBook;
    public UISprite Settings;
    public UISprite Inventory;

    public GameObject MiniMapController;
    public GameObject MiniMap;


    void Update()
    {

        if (Input.GetKeyDown(KeyCode.M) && !GameManager.LocalPlayer.IsLocked)
        {
            MiniMap.SetActive(!MiniMap.active);
            MiniMapController.SetActive(MiniMap.active);
        }

    }

    public void GotoCustomizer()
    {
        Application.LoadLevel("character creator");
    }

    public void ShowCharactor()
    {
        if (Charactor.Instance.isOpen)
        {
            Charactor.Instance.CloseWindow();
            Inventory.spriteName = "screen_inventory_closed";
        }
        else
        {
            Charactor.Instance.ShowCharactorWindow();
            Inventory.spriteName = "screen_inventory_open";
        }
    }

    public void ShowSkillTree()
    {
        if (SkillTree.Instance.isOpen)
        {
            SkillTree.Instance.CloseWindow();
            SkillBook.spriteName = "screen_spellbook_closed";
        }
        else
        {
            SkillTree.Instance.OpenSkillTree();
            SkillBook.spriteName = "screen_spellbook_open";
        }
    }

    public void ShowQuestSystem()
    {
        if (QuestLogSystem.Instance.isOpen)
        {
            QuestLogSystem.Instance.CloseWindow();
            QuestLog.spriteName = "screen_quest_closed";
        }
        else
        {
            QuestLogSystem.Instance.ShowQuestLogWindow();
            QuestLog.spriteName = "screen_quest_open";
        }
    }

    public void ShowSettings()
    {
        Application.LoadLevel(GameSettings.MainMenu);
    }

    


}
