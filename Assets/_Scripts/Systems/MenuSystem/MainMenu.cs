﻿using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour
{
    private Vector3 OffFindWindow = new Vector3(1500, -96, 0);

    public UIPanel MainMenuPanel;
    public UIPanel SoloPanel;
    public UIPanel MultiplayerPanel;
    public UISprite FindServerWindow;

    public GameObject EnterWorld;
    public GameObject ResetWarning;
    public UILabel DownloadProgress;

    void Awake()
    {

        if (PlayerDataStorage.Instance == null)
        {
            Instantiate(Resources.Load("_ServerManager"));
        }

    }

    void Start()
    {
        CancelNewGame();
        EnterWorld.SetActive(false);
        LoadMainPanel();
        FindServerWindow.transform.localPosition = OffFindWindow;
    }

    void Update()
    {
        if (EnterWorld.active)
        {

            if (Application.GetStreamProgressForLevel(GameSettings.TestingLevel) == 1)
            {
                if (PlayerDataStorage.Instance.SetupPlayer == false)

                    Application.LoadLevel(GameSettings.PlayerCreator);

                else
                    Application.LoadLevel(GameSettings.TestingLevel);
            }
            else
            {
                float percentageLoaded = Application.GetStreamProgressForLevel(GameSettings.TestingLevel) * 100;
                DownloadProgress.text = "Building ....";
            }

           
        }
    }

    public void LoadSoloPanel()
    {
        TweenRotation trMain = MainMenuPanel.GetComponent<TweenRotation>();
        trMain.PlayForward();
        
        TweenRotation trSolo = SoloPanel.GetComponent<TweenRotation>();
        trSolo.PlayReverse();
        
    }

    public void LoadMultiplayerPanel()
    {
        if (!PhotonNetwork.connected)
        {
            PhotonServerManager.Instance.ConnectToServer();
        }
        
        TweenRotation trMain = MainMenuPanel.GetComponent<TweenRotation>();
        trMain.PlayForward();

        TweenRotation trMultiplayer = MultiplayerPanel.GetComponent<TweenRotation>();
        trMultiplayer.PlayReverse();

        SpringPosition.Begin(FindServerWindow.gameObject, new Vector3(296, -96, 0), 6f);
        ServerLobby.Instance.UpdateRoomListings();
    }

    public void LoadMainPanel()
    {
        TweenRotation trMain = MainMenuPanel.GetComponent<TweenRotation>();

        if (trMain != null)
        {
            trMain.PlayReverse();
        }

        TweenRotation trSolo = SoloPanel.GetComponent<TweenRotation>();

        if (trSolo != null)
        {
            trSolo.PlayForward();
        }

        TweenRotation trMulti = MultiplayerPanel.GetComponent<TweenRotation>();

        if (trMulti != null)
        {
            trMulti.PlayForward();
        }

        SpringPosition.Begin(FindServerWindow.gameObject, OffFindWindow, 6f);
    }

	public void CreateNewGame ()
	{
        ResetWarning.SetActive(true);
	}

    public void ContinueGame()
    {
        PhotonServerManager.Instance.SetServerOffline();
        EnterWorld.SetActive(true);
       
    }

    public void ExitGame()
    {
        Application.Quit();
    }

    public void OpenFacebookPage()
    {
        Application.OpenURL("https://www.facebook.com/Quickfixinc?fref=ts");
    }

    public void OpenTwitterPage()
    {
        Application.OpenURL("https://twitter.com/Quickfixinc");
    }

    public void CreateNewMultiplayerRoom()
    {
        EnterWorld.SetActive(true);
        PhotonServerManager.Instance.CreateRoom();
    }

    public void JoinSelectedGame()
    {
        if (ServerLobby.Instance.SelectedRoom != "")
        {
            EnterWorld.SetActive(true);
            PhotonServerManager.Instance.JoinSelectedGame();
        }

    }

    public void StartNewGame()
    {
        PhotonServerManager.Instance.SetServerOffline();
        Application.LoadLevel(GameSettings.PlayerCreator);
    }

    public void CancelNewGame()
    {
        ResetWarning.SetActive(false);
    }
}
