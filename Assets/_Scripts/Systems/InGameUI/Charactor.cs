﻿using UnityEngine;
using System.Collections;

public class Charactor : BaseInGameWindow
{
		public static Charactor Instance;

        public ItemStorage _PlayerStorage;

		public UILabel PlayerName;
		public UILabel MeisterType;
        public UILabel MeisterLevel;

        public ItemEquipmentSlot Hat;
        public ItemEquipmentSlot Cloths;
        public ItemEquipmentSlot Gloves;
        public ItemEquipmentSlot Boots;
        public ItemEquipmentSlot Extra;
        public ItemEquipmentSlot Weapon;

        public GameObject PlayerEquipment;
        public GameObject PlayerStats;

        public UILabel HpValue;
        public UILabel MpValue;
        public UILabel AttackValue;
        public UILabel DefenceValue;

		private GameObject CurrentHair;
		
		void Awake ()
		{
            if (PlayerDataStorage.Instance == null)
                return;

			Instance = this;
            CurrentWindowPosition = PlayerDataStorage.Instance.CharactorWindowPos;
		}
	
		// Use this for initialization
		new void Start ()
		{
			PlayerName.text = PlayerDataStorage.Instance.PlayerName.ToUpper();
            MeisterLevel.text = "LEVEL " + PlayerDataStorage.Instance.CurrentLevel;
            MeisterType.text = PlayerDataStorage.Instance.PlayerElement.ToString().ToUpper() + " AFFINITY";

            ShowPlayerEquipment();

			base.Start ();
		}

        public void ShowCharactorWindow()
        {
            GetPlayerCurrentStats();
            OpenWindow();
        }

        private void GetPlayerCurrentStats()
        {
            HpValue.text = GameManager.LocalPlayer.MaxHP.ToString();
            MpValue.text = GameManager.LocalPlayer.MaxMana.ToString();
            AttackValue.text = GameManager.LocalPlayer.Attack.ToString();
            DefenceValue.text = GameManager.LocalPlayer.Defence.ToString();
        }

        public void ShowPlayerStats()
        {
            PlayerEquipment.SetActive(false);
            PlayerStats.SetActive(true);
        }

        public void ShowPlayerEquipment()
        {
            PlayerEquipment.SetActive(true);
            PlayerStats.SetActive(false);
            ItemStorage.LocalItemStorage.BuildInvGird();
        }


        internal void RefreshPlayerInv()
        {
            ItemStorage.LocalItemStorage.UpdateInvGrid();
        }
}
