﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class UISkillTreeDetail : DragDropItem
{
    public UILabel SkillNameLable;
    public UILabel SkillSpCost;
    public UISprite SkillIcon;
    public UISprite SkillUpgrade1;
    public UISprite SkillUpgrade2;
    public UISprite SkillUpgrade3;
    public UISprite SkillUpgrade4;
    public UISprite SkillUpgradeMax;
    public UISprite SkillBg;
    public UISprite SkillLocked;
    public UISprite SkillNotLeared;
    public UISprite SkillOverlay;

    public int SkillID16 { get; set; }
    public string SkillIconname { get; set; }
    public string SkillName { get; set; }
    public string SkillDescription { get; set; }
    public int SkillLevel { get; set; }
    public bool IsSkillLocked { get; set; }
    public emElementType SkillElement { get; set; }
    private int Cost;


    private Camera uiCamera;
    private GameObject dragIcon;

    // Use this for initialization
    new void Start()
    {
        cloneOnDrag = false;
        ItemToClone = SkillIcon.gameObject;
        base.Start();
    }

    // Update is called once per frame
    void Update()
    {
        ItemData = SkillID16;          
    }

    void OnTooltip(bool show)
    {
        if (show)
        {
            string t = "[" + NGUIText.EncodeColor(Color.white) + "]" + SkillName + "[-]\n";
            t += "[A60006]" + (SkillLevel == 0 ? "NOT LEARNED" : "Level" + SkillLevel.ToString()) + "\n\n";
            t += "[FF9900]" + SkillDescription + "\n\n";


            UITooltip.ShowText(t);
        }
        else
            UITooltip.ShowText(null);

    }

    internal void UpdateLayout()
    {
        SkillNameLable.text = SkillName;
        SkillIcon.spriteName = SkillIconname;
        SkillLocked.gameObject.SetActive(IsSkillLocked);
        SkillNotLeared.gameObject.SetActive(false);
        SkillOverlay.gameObject.SetActive(false);

        switch (SkillElement)
        {
            case emElementType.Ember:
                SkillBg.color = NGUIText.ParseColor("ee1c24", 0);
                break;
            case emElementType.Frost:
                SkillBg.color = NGUIText.ParseColor("6dd0f7", 0);
                break;
            case emElementType.Vale:
                SkillBg.color = NGUIText.ParseColor("007236", 0);
                break;
            case emElementType.Shadow:
                SkillBg.color = NGUIText.ParseColor("662d91", 0);
                break;
            case emElementType.Spirt:
                SkillBg.color = NGUIText.ParseColor("fff568", 0);
                break;
            case emElementType.None:
                SkillBg.color = NGUIText.ParseColor("FFFFFF", 0);
                break;
            default:
                break;
        }

        if (IsSkillLocked)
        {
            SkillLocked.gameObject.SetActive(true);
            
        }

        switch (SkillLevel)
        {
            case 0:

                SkillSpCost.text = "SP Cost : 1";
                SkillUpgrade1.enabled = false;
                SkillUpgrade2.enabled = false;
                SkillUpgrade3.enabled = false;
                SkillUpgrade4.enabled = false;
                SkillUpgradeMax.enabled = false;
                Cost = 1;
                SkillNotLeared.gameObject.SetActive(true);
                SkillOverlay.gameObject.SetActive(true);
                break;

            case 1:

                SkillSpCost.text = "SP Cost : 2";
                SkillUpgrade1.enabled = true;
                SkillUpgrade2.enabled = false;
                SkillUpgrade3.enabled = false;
                SkillUpgrade4.enabled = false;
                SkillUpgradeMax.enabled = false;
                Cost = 2;
                break;

            case 2:

                SkillSpCost.text = "SP Cost : 2";
                SkillUpgrade1.enabled = true;
                SkillUpgrade2.enabled = true;
                SkillUpgrade3.enabled = false;
                SkillUpgrade4.enabled = false;
                SkillUpgradeMax.enabled = false;
                Cost = 2;
                break;

            case 3:

                SkillSpCost.text = "SP Cost : 2";
                SkillUpgrade1.enabled = true;
                SkillUpgrade2.enabled = true;
                SkillUpgrade3.enabled = true;
                SkillUpgrade4.enabled = false;
                SkillUpgradeMax.enabled = false;
                Cost = 2;
                break;

            case 4:

                SkillSpCost.text = "SP Cost : 2";
                SkillUpgrade1.enabled = true;
                SkillUpgrade2.enabled = true;
                SkillUpgrade3.enabled = true;
                SkillUpgrade4.enabled = true;
                SkillUpgradeMax.enabled = false;
                Cost = 2;
                break;

            case 5:

                SkillSpCost.text = "Maxed";
                SkillUpgrade1.enabled = true;
                SkillUpgrade2.enabled = true;
                SkillUpgrade3.enabled = true;
                SkillUpgrade4.enabled = true;
                SkillUpgradeMax.enabled = true;
                Cost = 0;
                break;

            default:
                break;
        }

    }

    public void BuySkillUpgrade()
    {
        if (PlayerDataStorage.Instance.SpellPoints >= Cost && !IsSkillLocked)
        {
            List<PlayerSkill> PlayerSkills = PlayerDataStorage.Instance.PlayerSkills;

            if (PlayerSkills.Where(w => w.SkillId == SkillID16).FirstOrDefault() == null)
            {
                PlayerSkills.Add(new PlayerSkill { SkillId = SkillID16, SkillLevel = 1, HotBarId = -1, PosId = -1 });
            }
            else
                PlayerSkills.Where(w => w.SkillId == SkillID16).FirstOrDefault().SkillLevel++;

            PlayerDataStorage.Instance.SpellPoints -= Cost;
            
        }
        else
        {
            if (PlayerDataStorage.Instance.SpellPoints >= Cost)
                DialogBox.Instance.LoadDialogText("Your level is not high enough");
            else
                DialogBox.Instance.LoadDialogText("You don't have enough skill points");
        }

        SkillTree.Instance.LoadSelectedTab();
    }

}
