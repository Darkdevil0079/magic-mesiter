﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SkillTree : BaseInGameWindow
{
    public static SkillTree Instance;
    public UILabel SkillTabTitle;
    public UIGrid SkillTreeGrid;

    public UILabel SkillPointLeft;
    public UILabel AbilityPointLeft;
    public UILabel PageID;

    private string CurrentTab;             // Tabs = 1 - Ember , 2 - Frost , 3 - Vale , 4 - Shadow , 5 - Sprit
    private int CurrentPageID;


    void Awake()
    {
        Instance = this;
        if (PlayerDataStorage.Instance != null)
        {
            CurrentWindowPosition = PlayerDataStorage.Instance.SkillTreeWindowPos;
        }
        else
            CurrentWindowPosition = Vector3.zero;
    }

    // Use this for initialization
    new void Start()
    {
        base.Start();
    }

    // Update is called once per frame
    new void Update()
    {
        SkillPointLeft.text = PlayerDataStorage.Instance.SpellPoints.ToString();
        AbilityPointLeft.text = PlayerDataStorage.Instance.AbilityPoints.ToString();
        base.Update();
    }

    public void OpenSkillTree()
    {
        CurrentPageID = 1;
        CurrentTab = "EmberTab";
        SkillPointLeft.text = PlayerDataStorage.Instance.SpellPoints.ToString();
        AbilityPointLeft.text = PlayerDataStorage.Instance.AbilityPoints.ToString(); ;
        LoadSelectedTab();
        OpenWindow();
    }

    public void OpenSelectedTab(GameObject sender)
    {
        CurrentPageID = 1;
        CurrentTab = sender.name;
        LoadSelectedTab();
    }

    public void LoadSelectedTab()
    {
        List<SkillBaseSkill> TabSkillList = null;

        switch (CurrentTab)
        {
            case "EmberTab":
                TabSkillList = SkillDatabase.FindAllSkillByElement(emElementType.Ember);
                SkillTabTitle.text = "Ember Spells";
                break;
            case "FrostTab":
                TabSkillList = SkillDatabase.FindAllSkillByElement(emElementType.Frost);
                SkillTabTitle.text = "Frost Spells";
                break;
            case "ValeTab":
                TabSkillList = SkillDatabase.FindAllSkillByElement(emElementType.Vale);
                SkillTabTitle.text = "Vale Spells";
                break;
            case "ShadowTab":
                TabSkillList = SkillDatabase.FindAllSkillByElement(emElementType.Shadow);
                SkillTabTitle.text = "Shadow Spells";
                break;
            case "SpritTab":
                TabSkillList = SkillDatabase.FindAllSkillByElement(emElementType.Spirt);
                SkillTabTitle.text = "Sprit Spells";
                break;
            default:
                break;
        }

        if (TabSkillList != null)
        {

            foreach (Transform clear in SkillTreeGrid.transform)
            {
                NGUITools.SetActive(clear.gameObject, false);
                DestroyObject(clear.gameObject);
            }

            List<SkillBaseSkill> TabSkillListPaged = null;

            if (CurrentPageID == 1)
            {
                TabSkillListPaged = TabSkillList.Take(12).ToList();
            }
            else
                TabSkillListPaged = TabSkillList.Skip(12).ToList();

            foreach (SkillBaseSkill TreeSkill in TabSkillListPaged.OrderBy(o => o.tier).ThenBy(n => n.tierPlaceID))
            {
                GameObject SkillTreeHolder = NGUITools.AddChild(SkillTreeGrid.gameObject, Resources.Load("UI/SkillTreeSkillHolder") as GameObject);
                UISkillTreeDetail SkillData = SkillTreeHolder.GetComponent<UISkillTreeDetail>();
                PlayerSkill PlayerTreeSkill = PlayerDataStorage.Instance.PlayerSkills.Where(w => w.SkillId == TreeSkill.id16).FirstOrDefault();
                
                SkillData.SkillName = TreeSkill.name;
                SkillData.SkillID16 = TreeSkill.id16;
                SkillData.SkillDescription = TreeSkill.description;

                if (PlayerTreeSkill != null)
                {
                    SkillData.SkillLevel = PlayerTreeSkill.SkillLevel;
                }

                SkillData.IsSkillLocked = TreeSkill.unLockedLevel > 0 ? true : false;
                SkillData.SkillElement = TreeSkill.skillElement;
                SkillData.SkillIconname = TreeSkill.iconName;
                SkillData.UpdateLayout();
            }

            SkillTreeGrid.Reposition();

        }

    }

    public void ViewNextPage()
    {
        if (CurrentPageID == 2)
        {
            CurrentPageID = 1;
        }
        else
            CurrentPageID++;

        PageID.text = "PAGE " + CurrentPageID + "/2";
        LoadSelectedTab();
    }

    public void ViewPrePage()
    {
        if (CurrentPageID == 1)
        {
            CurrentPageID = 2;
        }
        else
            CurrentPageID--;

        PageID.text = "PAGE " + CurrentPageID + "/2";
        LoadSelectedTab();
    }
}
