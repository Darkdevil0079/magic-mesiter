﻿using UnityEngine;
using System.Collections;

public class DialogBox : MonoBehaviour
{
    public static DialogBox Instance;
    public UILabel DialogLabel;
    public UIButton DialogAction;

    private TypewriterEffect DialogEffect;
    private int CurrentTextCnt;
    private string[] TextStore;
    private float FadeOutTimer;
    private float TimeoutTime = 5f;

    void Awake()
    {
        Instance = this;
        
    }

	// Use this for initialization
	void Start () {

        DialogAction.gameObject.SetActive(false);
        HideDialogBox();
	
	}
	
	// Update is called once per frame
	void Update () {

        if (Time.time > FadeOutTimer )
        {
            HideDialogBox();
        }

	}

    public void HideDialogBox()
    {
        SpringPanel.Begin(gameObject, new Vector3(500f, 0, 0), 6f);
    }

    public void ShowDialogBox()
    {
        SpringPanel.Begin(gameObject, new Vector3(-138f, 0, 0), 6f);
    }

    public void LoadDialogText(string[] Text)
    {
        ShowDialogBox();
        TextStore = Text;
        CurrentTextCnt = 0;
        DisplayText();
    }

    public void LoadDialogText(string Text)
    {
        string[] SingleLine = new string[1];
        SingleLine[0] = Text;

        ShowDialogBox();
        TextStore = SingleLine;
        CurrentTextCnt = 0;
        DisplayText();
        FadeOutTimer = Time.time + TimeoutTime;
    }

    private void DisplayText()
    {
        DialogLabel.text = TextStore[CurrentTextCnt];
    }

    public void ShowActionButton()
    {
        DialogAction.gameObject.SetActive(true);
        FadeOutTimer = Time.time + TimeoutTime;
    }

    public void ProgressText()
    {

        if (CurrentTextCnt >= TextStore.Length - 1)
        {
            HideDialogBox();
            TextStore = null;
            CurrentTextCnt = 0;
        }
        else
        {
            CurrentTextCnt++;
            DisplayText();
        }


    }

}
