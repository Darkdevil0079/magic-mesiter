﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ChestLootViewer : MonoBehaviour {

    public Transform target;
    public Chest ChestObject { get; set; }

    private Transform mTrans;
    private Camera mGameCam;
    private Camera mUICam;
    private Vector3 mPos;
    private bool mVisible = true;
    public List<int> IconList;

    public UISprite LootIcon1;
    public UILabel LootQty1;
    public UISprite LootIcon2;
    public UILabel LootQty2;
    public UISprite LootIcon3;
    public UILabel LootQty3;
    public UISprite LootIcon4;
    public UILabel LootQty4;
    public UISprite LootIcon5;
    public UILabel LootQty5;

    private LootItem itemScript;

    void Start()
    {
        if (target == null) { Destroy(gameObject); return; }
        mTrans = transform;
        mGameCam = NGUITools.FindCameraForLayer(target.gameObject.layer);
        mUICam = NGUITools.FindCameraForLayer(gameObject.layer);

        int cnt = 1;
        LootIcon1.enabled = false;
        LootIcon2.enabled = false;
        LootIcon3.enabled = false;
        LootIcon4.enabled = false;
        LootIcon5.enabled = false;
        LootQty1.text = "";
        LootQty2.text = "";
        LootQty3.text = "";
        LootQty4.text = "";
        LootQty5.text = "";

        if (mVisible)
        {
            foreach (int chestItem in IconList)
            {
                InvBaseItem Item = InvDatabase.FindByID(chestItem);
                
                switch (cnt)
                {
                    case 1:
                        LootIcon1.atlas = Item.iconAtlas;
                        LootIcon1.spriteName = Item.iconName;
                        LootQty1.text = "";
                        LootIcon1.enabled = true;
                        itemScript = LootIcon1.gameObject.GetComponent<LootItem>();
                        itemScript.ItemID = chestItem;
                        break;
                    case 2:
                        LootIcon2.atlas = Item.iconAtlas;
                        LootIcon2.spriteName = Item.iconName;
                        LootQty2.text = "";
                        LootIcon2.enabled = true;
                        itemScript = LootIcon2.gameObject.GetComponent<LootItem>();
                        itemScript.ItemID = chestItem;
                        break;
                    case 3:
                        LootIcon3.atlas = Item.iconAtlas;
                        LootIcon3.spriteName = Item.iconName;
                        LootQty3.text = "";
                        LootIcon3.enabled = true;
                        itemScript = LootIcon3.gameObject.GetComponent<LootItem>();
                        itemScript.ItemID = chestItem;
                        break;
                    case 4:
                        LootIcon4.atlas = Item.iconAtlas;
                        LootIcon4.spriteName = Item.iconName;
                        LootQty4.text = "";
                        LootIcon4.enabled = true;
                        itemScript = LootIcon4.gameObject.GetComponent<LootItem>();
                        itemScript.ItemID = chestItem;
                        break;
                    case 5:
                        LootIcon5.atlas = Item.iconAtlas;
                        LootIcon5.spriteName = Item.iconName;
                        LootQty5.text = "";
                        LootIcon5.enabled = true;
                        itemScript = LootIcon5.gameObject.GetComponent<LootItem>();
                        itemScript.ItemID = chestItem;
                        break;
                    default:
                        break;
                }

                cnt++;
            }
        }
    }

    void LateUpdate()
    {
        if (target == null) { Destroy(gameObject); return; }

        mPos = mGameCam.WorldToViewportPoint(target.position);

        bool visible = (mPos.z > 0f && mPos.x > 0f && mPos.x < 1f && mPos.y > 0f && mPos.y < 1f);

        if (mVisible != visible)
        {
            mVisible = visible;
            UIWidget[] widgets = gameObject.GetComponentsInChildren<UIWidget>();
            foreach (UIWidget w in widgets) w.enabled = mVisible;
        }

        if (mVisible)
        {
            mPos = mUICam.ViewportToWorldPoint(mPos);
            mPos.z = 0f;
            mTrans.position = new Vector3(mPos.x - .14f, mPos.y, mPos.z);
        }
    }

    public void CloseWindow()
    {
        if (ChestObject != null)
            ChestObject.GetComponent<Chest>().ForceCloseChest();

        DestroyImmediate(this.gameObject);

    }
}
