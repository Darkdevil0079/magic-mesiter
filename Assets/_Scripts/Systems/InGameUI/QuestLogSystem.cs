﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;
using System.Collections.Generic;
using System.Linq;

public class QuestLogSystem : BaseInGameWindow
{

    public static QuestLogSystem Instance;

    private TweenScale QuestLogScale;
    public UIGrid QuestListGird;
    private bool isShowing = true;
    private List<PlayerQuest> PlayerCurrentQuestLog;
    private QuestState CurrentTab;

    //Quest UI Elements
    public UILabel QuestTitle;
    public UILabel QuestSource;
    public UILabel QuestDescription;
    public UILabel QuestSummary;

    void Awake()
    {
        Instance = this;
    }

    private void GetAllQuest()
    {
        string[] allQuests = QuestLog.GetAllQuests(QuestState.Active);

        Debug.Log(allQuests.Length);

        for (int i = 0; i < allQuests.Length; i++)
        {
            PlayerQuest NewQuestToAdd = new PlayerQuest();

            string str = allQuests[i];

            NewQuestToAdd.QuestID = i;
            NewQuestToAdd.Name = str;
            NewQuestToAdd.Description = QuestLog.GetQuestDescription(str);

            int questEntryCount = QuestLog.GetQuestEntryCount(str);
            string[] formattedTextArray = new string[questEntryCount];

            QuestState[] questEntryState = new QuestState[questEntryCount];
            for (int j = 0; j < questEntryCount; j++)
            {
                formattedTextArray[j] = QuestLog.GetQuestEntry(str, j + 1);
                questEntryState[j] = QuestLog.GetQuestEntryState(str, j + 1);
            }


            NewQuestToAdd.Entries = formattedTextArray;
            NewQuestToAdd.EntryStates = questEntryState;
            NewQuestToAdd.Abandonable = QuestLog.IsQuestAbandonable(str);
            NewQuestToAdd.Available = QuestLog.IsQuestTrackingAvailable(str);
            NewQuestToAdd.Tracking = QuestLog.IsQuestTrackingEnabled(str);

            if (PlayerCurrentQuestLog == null)
                PlayerCurrentQuestLog = new List<PlayerQuest>();

            PlayerCurrentQuestLog.Add(NewQuestToAdd);

        }

    }

    public void HideShowQuestLog()
    {
        if (QuestLogScale == null)
        {
            QuestLogScale = GetComponent<TweenScale>();
        }

        if (isShowing)
        {
            isShowing = false;
            QuestLogScale.PlayForward();
        }
        else
        {
            QuestLogScale.PlayReverse();
            isShowing = true;
        }
    }

    public void DisplayQuestList()
    {
        GetAllQuest();

        foreach (Transform clear in QuestListGird.transform)
        {
            NGUITools.SetActive(clear.gameObject, false);
            DestroyObject(clear.gameObject);
        }

        if (PlayerCurrentQuestLog != null)
        {


            foreach (PlayerQuest quest in PlayerCurrentQuestLog)
            {
                GameObject NewQuestItem = NGUITools.AddChild(QuestListGird.gameObject, Resources.Load("UI/QuestItem") as GameObject);
                NewQuestItem.GetComponent<UILabel>().text = quest.Name;

                QuestItem QuestData = NewQuestItem.GetComponent<QuestItem>();
                QuestData.parentSystem = this;
                QuestData.QuestIndex = quest.QuestID;


            }
        }
    }

    public void ShowQuestLogWindow()
    {
        DisplayQuestList();
        OpenWindow();
    }

    public void ShowCurrentFilterQuests()
    {
        CurrentTab = QuestState.Active;
    }

    public void ShowCompletedFilterQuests()
    {
        CurrentTab = QuestState.Success;
    }

    public void ShowCurrentSelectedQuest(int questID)
    {

        PlayerQuest SelectedQuest = PlayerCurrentQuestLog.Where(w => w.QuestID == questID).FirstOrDefault();

        if (SelectedQuest != null)
        {

            QuestTitle.text = SelectedQuest.Name;
            QuestSource.text = "NPC (Shop Keeper)";

            if (SelectedQuest.Description == null)
                SelectedQuest.Description = "No Description has been added";
            else
                QuestDescription.text = SelectedQuest.Description;

            string[] summary = SelectedQuest.Entries[0].Split(';');
            string summaryDisplay = "";

            for (int i = 0; i < summary.Length; i++)
			{
                summaryDisplay += "-" + summary[i] + "\n\n";
			}

            QuestSummary.text = summaryDisplay;

        }


    }
}
