using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public enum emAttiude
{
		Passive,
		Neutral,
		Aggressive
}

public class BaseEnemy : MonoBehaviour
{

		public int EnemyID;
		
		public int HpMax;
		public int MpMax;
		public List<int> Attacks;
		public int DefencePhysical;
		public int DefenceMagical;
		public emAttiude Attiude;
		public emAttackType CombatType;
		public emAttackType AttackPref;
		public int AttackSwitch;
		public emAttackType AttackPrefSwitch;
		
		public List<int> DropIDList;
		
		void Awake ()
		{
				Attacks = new List<int> ();
				DropIDList = new List<int> ();
		}
}

