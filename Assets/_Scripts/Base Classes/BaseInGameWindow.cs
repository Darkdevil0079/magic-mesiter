﻿using UnityEngine;
using System.Collections;

public class BaseInGameWindow : MonoBehaviour
{
		private TweenScale BaseTS;
		private Transform Window;
		
		public bool isOpen = false;
        public bool StartHidden = true;
		public Vector3 CurrentWindowPosition = Vector3.zero;

		// Use this for initialization
		public void Start ()
		{				
				if (this.GetComponent<TweenScale> () == null) {
						SetupBaseTweenScale ();
				}
				
				Window = this.transform;

                if (StartHidden)
                {
                    Window.localScale = new Vector3(0, 0, 0);
                    NGUITools.SetActive(Window.gameObject, false);
                }
                
                Window.localPosition = CurrentWindowPosition;
		}
	
		// Update is called once per frame
		public void Update ()
		{
		}
	
		void SetupBaseTweenScale ()
		{
				BaseTS = this.gameObject.AddComponent <TweenScale> ();
				BaseTS.from = new Vector3 (0, 0, 0);
				BaseTS.to = new Vector3 (1, 1, 1);
				BaseTS.duration = 0.3f;
				BaseTS.delay = .1f;
				BaseTS.enabled = false;
	
		}
				
		public void OpenWindow ()
		{
				BaseTS.PlayForward ();
				isOpen = true;
                NGUITools.SetActive(Window.gameObject, true);
		}
		
		public void CloseWindow ()
		{
				BaseTS.PlayReverse ();
				isOpen = false;
                UpdateWindowPosition(Window.position);
                NGUITools.SetActive(Window.gameObject, false);
		}
		
		public void UpdateWindowPosition (Vector3 newPosition)
		{
				CurrentWindowPosition = newPosition;
		}
}
