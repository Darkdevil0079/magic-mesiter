using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using PixelCrushers.DialogueSystem;

public class BaseMeister : MonoBehaviour
{

    public object[] NetworkMeisterData;

    public Transform Player;
    public Transform BasePlayer;
    public AoEMouseTarget AOECamera;

    // PLAYER STATES
    public enum PlayerStates : int
    {
        Idle = 1,
        Moveing = 2,
        Attacking = 3,
        CastingSkill = 4,
        Died = 5
    }

    public float CurrentHP;
    public float CurrentMana;
    public int DodgeMove;
    public string emoteValue;

    public int MaxHP { get; set; }
    public int MaxMana { get; set; }
    public int Defence { get; set; }
    public int Attack { get; set; }
    public bool HasWeapon { get; set; }
    public int NumberofKills { get; set; }
    public int XPToNextLvl { get; set; }

    public CharacterController PlayerController { get; set; }
    public MeisterAnimatiors Animatior { get; set; }
    public MeisterSkills Skills { get; set; }
    public MeisterNetwork NetworkController { get; set; }
    public PhotonView NetworkView { get; set; }
    public KGFMapIcon MapIcon { get; set; }
    public ServerChat MeisterChat { get; set; }
    public MeisterEquipment Equipment { get; set; }
    public AudioSource AudioPlayer { get; set; }
    public OverrideActorName dialogNameDisplay { get; set; }

    public bool CanCast;
    public bool IsLocked;
    public bool InCastingMode;
    public bool IsOnMount;
    public bool IsCustomizing { get; set; }
    public bool IsBlocking;
    public bool IsMountSummoned;
    public bool ShowHatPreview { get; set; }
    public bool InChat { get; set; }
    public bool IsRemotePlayer;
    public bool ToggleWalking;
    public bool IsCustomizer;

    internal float ButtonCooler = .5f; // Half a second before reset
    public int ButtonCountLeft { get; set; }
    public int ButtonCountRight { get; set; }
    public int ButtonCountForward { get; set; }
    public int ButtonCountBack { get; set; }

    public float NextMagicRecharge { get; set; }

    public Transform SkillSpawn { get; set; }
    public Transform BuffSpawn { get; set; }
    public Transform CombatRightHolder { get; set; }

    public List<GameObject> ClothsSets;
    public List<GameObject> GlovesSets;
    public List<GameObject> BootsSets;
    public List<GameObject> ExtraSets;

    public GameObject TargetLookAt { get; set; }
    public GameObject MeisterFoward { get; set; }
    public GameObject ResetPoint { get; set; }

    public GameObject HatMountPoint { get; set; }
    public GameObject WeaponMountPoint;
    public GameObject BaseEyesRight;
    public GameObject BaseEyesLeft;
    public GameObject BaseHands;
    public GameObject BaseBody;
    public GameObject BaseHead;
    public GameObject FootPrintHolder;
    public GameObject BlockEffect { get; set; }
    public GameObject WearingHarObject { get; set; }
    public GameObject CurrentSelectedWeapon { get; set; }

    private int LastHairIndex = -1;
    private string LastGender = "n";
    private GameObject NudeOutfit;
    private GameObject CurrentHair;
    private bool IsFrameworkSetup = false;

    public void Update()
    {
        if (!InCastingMode)
        {

            if (NextMagicRecharge < Time.time)
            {

                CurrentMana += 3;
                if (CurrentMana > MaxMana)
                {
                    CurrentMana = MaxMana;
                }
                NextMagicRecharge = Time.time + 1f;
            }

        }
    }

    public void LateUpdate()
    {
        CheckMeisterStats();
    }

    public void SetupMeisterFramework()
    {
        IsMountSummoned = false;

        if (AOECamera.AOEProjector == null)
        {
            AOECamera.SetupProjector();
        }
        AOECamera.AOEProjector.enabled = false;
        AOECamera.enabled = false;

        foreach (Transform Object in BasePlayer.transform)
        {

            if (Object.name.ToLower().Contains("body"))
            {
                ClothsSets.Add(Object.gameObject);
                Object.gameObject.SetActive(false);
            }

            if (Object.name.ToLower().Contains("gloves"))
            {
                GlovesSets.Add(Object.gameObject);
                Object.gameObject.SetActive(false);
            }

            if (Object.name.ToLower().Contains("boots"))
            {
                BootsSets.Add(Object.gameObject);
                Object.gameObject.SetActive(false);
            }

            if (Object.name.ToLower().Contains("nude"))
            {
                NudeOutfit = Object.gameObject;
                Object.gameObject.SetActive(false);
            }

        }

        GameObject SkillSpawnObject = new GameObject();
        SkillSpawnObject.name = "SkillSpawnObject";
        SkillSpawn = SkillSpawnObject.transform;
        SkillSpawn.parent = BasePlayer;
        SkillSpawn.localPosition = new Vector3(0f, 3.916748f, 1.530579f);

        GameObject BuffSpawnFlootObject = new GameObject();
        BuffSpawnFlootObject.name = "BuffSpawnFlootObject";
        BuffSpawn = BuffSpawnFlootObject.transform;
        BuffSpawn.parent = BasePlayer;
        BuffSpawn.localPosition = new Vector3(0f, 0f, 0f);

        GameObject CombatRightObject = new GameObject();
        CombatRightObject.name = "CombatRightObject";
        CombatRightHolder = CombatRightObject.transform;
        CombatRightHolder.parent = BasePlayer;
        CombatRightHolder.localPosition = new Vector3(0f, 7.69f, -17f);

        TargetLookAt = new GameObject();
        TargetLookAt.name = "LookAtPoint";
        TargetLookAt.transform.parent = BasePlayer;
        TargetLookAt.transform.localPosition = new Vector3(-1f, 5.26f, 0f);

        MeisterFoward = new GameObject();
        MeisterFoward.name = "MeisterFoward";
        MeisterFoward.transform.parent = BasePlayer;
        MeisterFoward.transform.localPosition = new Vector3(0f, 4.78f, 3.05f);

        ResetPoint = new GameObject();
        ResetPoint.name = "ResetPoint";
        ResetPoint.transform.parent = BasePlayer;
        ResetPoint.transform.localPosition = new Vector3(0f, 5.26f, 0f);

        HatMountPoint = new GameObject();
        HatMountPoint.name = "HatMountPoint";
        HatMountPoint.transform.parent = BasePlayer.Find("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 Head");
        HatMountPoint.transform.localPosition = new Vector3(-1.531376f, 0.006149275f, -0.01035217f);
        HatMountPoint.transform.localRotation = Quaternion.Euler(new Vector3(0f, 270f, 180f));

        WeaponMountPoint = new GameObject();
        WeaponMountPoint.name = "WeaponMountPoint";
        WeaponMountPoint.transform.parent = BasePlayer.Find("Bip001/Bip001 Pelvis/Bip001 Spine/Bip001 Spine1/Bip001 Neck/Bip001 R Clavicle/Bip001 R UpperArm/Bip001 R Forearm/Bip001 R Hand");
        WeaponMountPoint.transform.localPosition = new Vector3(-0.8110146f, 0.09582624f, -2.346795f);
        WeaponMountPoint.transform.localRotation = Quaternion.Euler(new Vector3(0f, 0f, 0f));
        WeaponMountPoint.transform.localScale = new Vector3(3, 3, 3);

        IsFrameworkSetup = true;

    }

    internal void SetupPlayerAppearance()
    {
        if (!IsFrameworkSetup)
            return;

        if (IsCustomizer)
        {
            IsCustomizing = true;
            UpdatePlayerAppearance(PlayerDataStorage.Instance.Gender, PlayerDataStorage.Instance.SkinColourIndex, PlayerDataStorage.Instance.EyeStyleIndex, PlayerDataStorage.Instance.EyeColorIndex);
            UpdatePlayerCloths(PlayerDataStorage.Instance.WearingHat,
                              PlayerDataStorage.Instance.Gender,
                              PlayerDataStorage.Instance.HairStyleIndex,
                              PlayerDataStorage.Instance.HairColorIndex,
                              -1,
                              -1,
                              -1,
                              -1,
                              -1
                              );

            UpdatePlayerClothsSets();
            CustomizerManager.Instance.UpdateMageDetails(PlayerDataStorage.Instance.PlayerElement);


        }
        else if (IsRemotePlayer)
        {
            if (NetworkMeisterData != null)
            {
                UpdatePlayerAppearance((string)NetworkMeisterData[0], (int)NetworkMeisterData[2], (int)NetworkMeisterData[3], (int)NetworkMeisterData[4]);
                UpdatePlayerCloths((bool)NetworkMeisterData[1],
                                    (string)NetworkMeisterData[0],
                                    (int)NetworkMeisterData[5],
                                    (int)NetworkMeisterData[6],
                                    (int)NetworkMeisterData[7],
                                    (int)NetworkMeisterData[8],
                                    (int)NetworkMeisterData[9],
                                    (int)NetworkMeisterData[10],
                                    (int)NetworkMeisterData[11]);

                if (NetworkMeisterData.Length > 11)
                {
                    PlayerItemEquiped remoteWep = new PlayerItemEquiped();
                    remoteWep.ItemID = (int)NetworkMeisterData[12];
                    UpdatePlayerWeapon(remoteWep);
                }


                Debug.Log("Update Network Player");
            }
        }
        else if (!IsRemotePlayer)
        {
            if (Equipment == null)
                return;

            int HatIndex;
            int ClothsIndex;
            int GlovesIndex;
            int BootsIndex;
            int ExtraIndex;

            PlayerItemEquiped ItemToLookUp;

            ItemToLookUp = Equipment.GetItemBySlot(InvBaseItem.Slot.Hat);
            if (ItemToLookUp != null)
            {
                HatIndex = ItemToLookUp.ItemID;
                PlayerDataStorage.Instance.WearingHat = true;
            }
            else
            {
                HatIndex = -1;
                PlayerDataStorage.Instance.WearingHat = false;
            }

            ItemToLookUp = Equipment.GetItemBySlot(InvBaseItem.Slot.Clothes);
            if (ItemToLookUp != null)
                ClothsIndex = ItemToLookUp.ItemID;
            else
                ClothsIndex = -1;
            ItemToLookUp = Equipment.GetItemBySlot(InvBaseItem.Slot.Gloves);
            if (ItemToLookUp != null)
                GlovesIndex = ItemToLookUp.ItemID;
            else
                GlovesIndex = -1;
            ItemToLookUp = Equipment.GetItemBySlot(InvBaseItem.Slot.Boots);
            if (ItemToLookUp != null)
                BootsIndex = ItemToLookUp.ItemID;
            else
                BootsIndex = -1;
            ItemToLookUp = Equipment.GetItemBySlot(InvBaseItem.Slot.Extra);
            if (ItemToLookUp != null)
                ExtraIndex = ItemToLookUp.ItemID;
            else
                ExtraIndex = -1;

            UpdatePlayerAppearance(PlayerDataStorage.Instance.Gender, PlayerDataStorage.Instance.SkinColourIndex, PlayerDataStorage.Instance.EyeStyleIndex, PlayerDataStorage.Instance.EyeColorIndex);
            UpdatePlayerCloths(PlayerDataStorage.Instance.WearingHat,
                               PlayerDataStorage.Instance.Gender,
                               PlayerDataStorage.Instance.HairStyleIndex,
                               PlayerDataStorage.Instance.HairColorIndex,
                               HatIndex,
                               ClothsIndex,
                               GlovesIndex,
                               BootsIndex,
                               ExtraIndex
                               );

            PlayerItemEquiped weap = PlayerDataStorage.Instance.WearingEquipment.Find(weapon => weapon.Slot == InvBaseItem.Slot.Weapon);
            UpdatePlayerWeapon(weap);
        }
        else
            Debug.Log("Unknow route");
    }

    internal void SelectMageElement(GameObject sender)
    {
        CustomizerManager.Instance.UpdateMageDetails((emElementType)System.Enum.Parse(typeof(emElementType), sender.name));
    }

    internal void SummmonMount()
    {
        if (IsMountSummoned)
            return;

        GameObject SummonedMount = Instantiate(Resources.Load("Mounts/Phorus"), Player.position + new Vector3(0, 0, 100), Quaternion.identity) as GameObject;
        SummonedMount.transform.LookAt(Player);
        IsMountSummoned = true;


    }

    internal void EnterCombatMode()
    {
        InCastingMode = true;
        Screen.showCursor = false;
        if (PlayerCamera.Instance != null)
            PlayerCamera.Instance.SetupCombatCamera();


    }

    internal void SetupPlayerBaseStats()
    {
        MaxHP = 100;
        MaxMana = 100;
        Attack = 1;
        Defence = 1;

        CurrentHP = MaxHP;
        CurrentMana = MaxMana;
    }

    internal void CollectItemAroundPlayer()
    {
        //				Collider[] hitColliders = Physics.OverlapSphere (transform.position, 3f);
        //				
        //				foreach (Collider FoundItem in hitColliders) {
        //					
        //						if (FoundItem.GetComponent<Item> () != null) {
        //								DataStorage.UpdateItemToPlayerInventory (FoundItem.GetComponent<Item> ());
        //						}
        //					
        //				}
    }

    internal void GainXPForKill(int GainXP)
    {
        Debug.Log("XP Added");
        //PlayerDataStorage.Instance.CurrentXP += GainXP;
    }

    internal void MeisterTakeDamage(int DamageTaken)
    {

        CurrentHP -= DamageTaken;
        Helper.DisplayBattleText(HatMountPoint, DamageTaken.ToString(), emElementType.None, 36);


        if (CurrentHP <= 0)
        {
            Animatior.IsDead = true;
        }


    }

    internal void AddHealthToMeister(float HealthToAdd)
    {

        Helper.DisplayBattleText(TargetLookAt, "+ " + HealthToAdd, emElementType.Vale, 32);

        if (CurrentHP < MaxHP)
        {
            CurrentHP += HealthToAdd;

            if (CurrentHP == MaxHP)
            {
                CurrentHP = MaxHP;
            }

        }
    }

    #region Meister Appearance - Private Functions

    private void UpdatePlayerAppearance(string gender, int skinIndex, int eyeStyle, int eyeColour)
    {
        CustomizerResource _cm = CustomizerResource.Instance;

        if (_cm == null)
            return;

        BaseBody.renderer.material.mainTexture = _cm.PlayerSkins[skinIndex - 1] as Texture2D;
        BaseHands.renderer.material.mainTexture = _cm.PlayerSkins[skinIndex - 1] as Texture2D;
        BaseHead.renderer.material.mainTexture = _cm.PlayerHeadSkins[skinIndex - 1] as Texture2D;

        string EyeStyle = "";
        string EyeColor = "";
        string EyeIndex = "";

        EyeStyle = "CUSTOMIZATION/Eye_Set/eye_" + gender + "_";

        if (PlayerDataStorage.Instance.Gender == "m")
        {
            EyeIndex = _cm.PlayerEyesMale[eyeStyle - 1].name.Substring(_cm.PlayerEyesMale[eyeStyle - 1].name.Length - 5, 2);
        }
        else
        {
            EyeIndex = _cm.PlayerEyesFemale[eyeStyle - 1].name.Substring(_cm.PlayerEyesFemale[eyeStyle - 1].name.Length - 5, 2);
        }
        EyeStyle += EyeIndex + "/";

        EyeColor = gender + "eye" + EyeIndex + "_" + (eyeColour - 1).ToString("0#");

        if (Resources.Load(EyeStyle + EyeColor) == null)
        {
            EyeColor = gender + "eye" + EyeIndex + "_00";
        }

        BaseEyesLeft.renderer.material.mainTexture = Resources.Load(EyeStyle + EyeColor) as Texture2D;
        BaseEyesRight.renderer.material.mainTexture = Resources.Load(EyeStyle + EyeColor) as Texture2D;
    }

    private void UpdatePlayerCloths(bool wearingHat, string gender, int hairStyle, int hairColor, int hatIndexId, int clothsItemId, int glovesItemId, int bootsItemId, int extraItemId)
    {
        CustomizerResource _cm = CustomizerResource.Instance;

        foreach (Transform clear in HatMountPoint.transform)
        {
            DestroyObject(clear.gameObject);
        }

        if (wearingHat)
        {
            GameObject CurrentHair = Instantiate(Resources.Load("CUSTOMIZATION/Hair_Set/HatHair/HatHair")) as GameObject;

            CurrentHair.transform.parent = HatMountPoint.transform;
            CurrentHair.transform.localPosition = CurrentHair.GetComponent<CustomizerPart>().PosOffset;
            CurrentHair.transform.localRotation = Quaternion.Euler(CurrentHair.GetComponent<CustomizerPart>().RotOffset);
            CurrentHair.transform.localScale = CurrentHair.GetComponent<CustomizerPart>().ScaleOffset;

            CurrentHair.renderer.material.mainTexture = Resources.Load("CUSTOMIZATION/Hair_Set/" + CurrentHair.name.Replace("(Clone)", "") + "/Textures/" + (hairColor - 1).ToString("0#")) as Texture2D;

            if (hatIndexId != -1)
            {
                InvBaseItem item = InvDatabase.FindByID(hatIndexId);

                GameObject PlayerHat = GameObject.Instantiate(item.attachment) as GameObject;
                PlayerHat.transform.parent = HatMountPoint.transform;
                PlayerHat.transform.localPosition = PlayerHat.GetComponent<CustomizerPart>().PosOffset;
                PlayerHat.transform.localRotation = Quaternion.Euler(PlayerHat.GetComponent<CustomizerPart>().RotOffset);
                PlayerHat.transform.localScale = PlayerHat.GetComponent<CustomizerPart>().ScaleOffset;
            }
        }
        else
        {
            if (gender == "m")
            {
                CurrentHair = GameObject.Instantiate(_cm.PlayerHairMale[hairStyle - 1]) as GameObject;
            }
            else
            {
                CurrentHair = GameObject.Instantiate(_cm.PlayerHairFemale[hairStyle - 1]) as GameObject;
            }

            CurrentHair.transform.parent = HatMountPoint.transform;
            CurrentHair.transform.localPosition = CurrentHair.GetComponent<CustomizerPart>().PosOffset;
            CurrentHair.transform.localRotation = Quaternion.Euler(CurrentHair.GetComponent<CustomizerPart>().RotOffset);
            CurrentHair.transform.localScale = CurrentHair.GetComponent<CustomizerPart>().ScaleOffset;
            LastHairIndex = (PlayerDataStorage.Instance.HairStyleIndex);

            if (CurrentHair != null)
            {
                CurrentHair.renderer.material.mainTexture = Resources.Load("CUSTOMIZATION/Hair_Set/" + CurrentHair.name.Replace("(Clone)", "") + "/Textures/" + (hairColor - 1).ToString("0#")) as Texture2D;
            }
        }

        bool ShowNude = true;

        string clothsitem = InvDatabase.FindByID(clothsItemId) != null ? InvDatabase.FindByID(clothsItemId).ListIndex.ToLower() : "";
        foreach (GameObject Cloth in ClothsSets)
        {
            if (Cloth.name.ToLower() == clothsitem)
            {
                Cloth.SetActive(true);
                ShowNude = false;
            }
            else
                Cloth.SetActive(false);
        }

        NudeOutfit.SetActive(ShowNude);

        string glovesitem = InvDatabase.FindByID(glovesItemId) != null ? InvDatabase.FindByID(glovesItemId).ListIndex.ToLower() : "NoItem";

        bool ShowBaseHands = true;

        foreach (GameObject Glove in GlovesSets)
        {
            if (Glove.name.ToLower() == glovesitem)
            {
                Glove.SetActive(true);
                BaseHands.SetActive(false);
                ShowBaseHands = false;
            }
            else
            {
                Glove.SetActive(false);
            }
        }

        if (glovesitem.ToLower() == "clo_05_gloves" || glovesitem.ToLower() == "clo_07_gloves")
        {
            ShowBaseHands = true;
        }

        BaseHands.SetActive(ShowBaseHands);

        string bootsitem = InvDatabase.FindByID(bootsItemId) != null ? InvDatabase.FindByID(bootsItemId).ListIndex.ToLower() : "";
        foreach (GameObject Boots in BootsSets)
        {
            if (Boots.name.ToLower() == bootsitem)
            {
                Boots.SetActive(true);
            }
            else
                Boots.SetActive(false);
        }




        string extraitem = InvDatabase.FindByID(extraItemId) != null ? InvDatabase.FindByID(extraItemId).ListIndex.ToLower() : "";
        foreach (GameObject Extra in ExtraSets)
        {
            if (Extra.name.ToLower() == extraitem)
            {
                Extra.SetActive(true);
            }
            else
                Extra.SetActive(false);
        }

    }

    private void UpdatePlayerClothsSets()
    {

        CustomizerResource _cm = CustomizerResource.Instance;
        GameObject ItemToLookUp = null;

        if (ShowHatPreview)
        {
            ItemToLookUp = Resources.Load("CUSTOMIZATION/Hats/Hat0" + CustomizerManager.Instance.CurrentClothsPreview) as GameObject;
        }

        #region Players Hats
        if (ItemToLookUp != null)
        {
            foreach (Transform clear in HatMountPoint.transform)
                DestroyObject(clear.gameObject);

            GameObject CurrentHair = Instantiate(Resources.Load("CUSTOMIZATION/Hair_Set/HatHair/HatHair")) as GameObject;

            CurrentHair.transform.parent = HatMountPoint.transform;
            CurrentHair.transform.localPosition = CurrentHair.GetComponent<CustomizerPart>().PosOffset;
            CurrentHair.transform.localRotation = Quaternion.Euler(CurrentHair.GetComponent<CustomizerPart>().RotOffset);
            CurrentHair.transform.localScale = CurrentHair.GetComponent<CustomizerPart>().ScaleOffset;

            CurrentHair.renderer.material.mainTexture = Resources.Load("CUSTOMIZATION/Hair_Set/" + CurrentHair.name.Replace("(Clone)", "") + "/Textures/" + (PlayerDataStorage.Instance.HairColorIndex - 1).ToString("0#")) as Texture2D;

            GameObject PlayerHat = GameObject.Instantiate(ItemToLookUp) as GameObject;
            PlayerHat.transform.parent = HatMountPoint.transform;
            PlayerHat.transform.localPosition = PlayerHat.GetComponent<CustomizerPart>().PosOffset;
            PlayerHat.transform.localRotation = Quaternion.Euler(PlayerHat.GetComponent<CustomizerPart>().RotOffset);
            PlayerHat.transform.localScale = PlayerHat.GetComponent<CustomizerPart>().ScaleOffset;
        }
        else
        {
            if (LastHairIndex != (PlayerDataStorage.Instance.HairStyleIndex) || LastGender != PlayerDataStorage.Instance.Gender)
            {

                foreach (Transform clear in HatMountPoint.transform)
                {
                    DestroyObject(clear.gameObject);
                }

                if (PlayerDataStorage.Instance.Gender == "m")
                {
                    if (PlayerDataStorage.Instance.HairStyleIndex > _cm.PlayerHairMale.Count)
                        PlayerDataStorage.Instance.HairStyleIndex = 1;

                    CurrentHair = GameObject.Instantiate(_cm.PlayerHairMale[PlayerDataStorage.Instance.HairStyleIndex - 1]) as GameObject;
                }
                else
                {
                    CurrentHair = GameObject.Instantiate(_cm.PlayerHairFemale[PlayerDataStorage.Instance.HairStyleIndex - 1]) as GameObject;
                }
                CurrentHair.transform.parent = HatMountPoint.transform;
                CurrentHair.transform.localPosition = CurrentHair.GetComponent<CustomizerPart>().PosOffset;
                CurrentHair.transform.localRotation = Quaternion.Euler(CurrentHair.GetComponent<CustomizerPart>().RotOffset);
                CurrentHair.transform.localScale = CurrentHair.GetComponent<CustomizerPart>().ScaleOffset;
                LastHairIndex = (PlayerDataStorage.Instance.HairStyleIndex);
            }

            if (CurrentHair != null)
            {
                CurrentHair.renderer.material.mainTexture = Resources.Load("CUSTOMIZATION/Hair_Set/" + CurrentHair.name.Replace("(Clone)", "") + "/Textures/" + (PlayerDataStorage.Instance.HairColorIndex - 1).ToString("0#")) as Texture2D;
            }
        }
        #endregion


        foreach (GameObject item in ClothsSets)
        {
            if (item.name.ToLower() == ClothsSets[CustomizerManager.Instance.CurrentClothsPreview - 1].name)
            {
                item.SetActive(true);
            }
            else
                item.SetActive(false);

            if (NudeOutfit != null)
            {
                NudeOutfit.SetActive(false);
            }


        }

        foreach (GameObject item in GlovesSets)
        {
            GameObject GlovesName = GlovesSets.Where(w => w.name.Contains("0" + CustomizerManager.Instance.CurrentClothsPreview)).FirstOrDefault();

            if (GlovesName != null)
            {
                if (item.name.ToLower() == GlovesName.name)
                {
                    item.SetActive(true);
                    if (item.name == "clo_05_gloves")
                        BaseHands.SetActive(true);
                    else
                        BaseHands.SetActive(false);

                }
                else
                    item.SetActive(false);
            }
            else
            {
                BaseHands.SetActive(true);
                item.SetActive(false);
            }

        }

        foreach (GameObject item in BootsSets)
        {
            GameObject BootsName = BootsSets.Where(w => w.name.Contains("0" + CustomizerManager.Instance.CurrentClothsPreview)).FirstOrDefault();

            if (BootsName != null)
            {
                if (item.name.ToLower() == BootsName.name)
                {
                    item.SetActive(true);
                }
                else
                    item.SetActive(false);
            }
            else
            {
                item.SetActive(false);
            }

        }

    }

    private void UpdatePlayerWeapon(PlayerItemEquiped Found)
    {

        foreach (Transform clear in WeaponMountPoint.transform)
        {
            DestroyObject(clear.gameObject);
        }

        if (Found == null)
        {
            HasWeapon = false;
        }
        else
        {
            HasWeapon = true;
            InvBaseItem wep = InvDatabase.FindByID(Found.ItemID);
            if (wep == null)
                return;

            GameObject Weapon = GameObject.Instantiate(InvDatabase.FindByID(Found.ItemID).attachment) as GameObject;
            if (Weapon != null)
            {
                Weapon.transform.parent = WeaponMountPoint.transform;

                CustomizerPart WeaponOffset = Weapon.GetComponent<CustomizerPart>();
                if (WeaponOffset != null)
                {
                    Weapon.transform.localPosition = WeaponOffset.PosOffset;
                    Weapon.transform.localRotation = Quaternion.Euler(WeaponOffset.RotOffset);
                    Weapon.transform.localScale = WeaponOffset.ScaleOffset;
                }
                else
                {
                    Weapon.transform.localPosition = Vector3.zero;
                    Weapon.transform.localRotation = Quaternion.identity;
                    Weapon.transform.localScale = new Vector3(1, 1, 1);
                }
            }
        }
    }

    private void CheckMeisterStats()
    {
        PlayerDataStorage.Instance.XPLeft = Helper.GetLevelXP();

        if (PlayerDataStorage.Instance.CurrentXP >= PlayerDataStorage.Instance.XPLeft)
        {
            PlayerDataStorage.Instance.CurrentXP = 0;
            PlayerDataStorage.Instance.CurrentLevel++;
        }
    } 
    #endregion
}

