﻿using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem;

public class NPCController : ActOnDialogueEvent
{
    private const string TALK1 = "talk1";
    private const string TALK2 = "talk2";
    private const string TALKNO = "talkno";
    private const string IDLE = "idle";
    
    void Start()
    {
        animation.CrossFade(IDLE);
    }

    public override void TryEndActions(Transform actor)
    {
        animation.CrossFade(IDLE);
    }

    public override void TryStartActions(Transform actor)
    {
        int TalkAnimId = Random.Range(1, 3);

        switch (TalkAnimId)
        {
            case 1:
            case 2:
                animation.CrossFade(IDLE);
                break;
            default:
                animation.CrossFade(IDLE);
                break;
        }
    }
}
