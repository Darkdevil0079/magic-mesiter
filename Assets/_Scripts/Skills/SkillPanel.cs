﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class SkillPanel : MonoBehaviour
{
    public static SkillPanel Instance;

    public UISprite SkillHolder;
    public UISprite Hightlight;

    public UISprite SkillSlot1;
    public UISprite SkillSlot2;
    public UISprite SkillSlot3;
    public UISprite SkillSlot4;
    public UILabel HotBarLabel;

    public UISkillIcon SelectedSkill { get; set; }
    public int CurrentSelectedSkill { get; set; }
    public int CurrentHotBarID = 1;

    private TweenRotation SwapBars1;
    private TweenRotation SwapBars2;
    private TweenRotation SwapBars3;
    private TweenRotation SwapBars4;



    void Awake()
    {
        Instance = this;
    }

    // Use this for initialization
    void Start()
    {
        SwapBars1 = SkillSlot1.GetComponent<TweenRotation>();
        SwapBars2 = SkillSlot2.GetComponent<TweenRotation>();
        SwapBars3 = SkillSlot3.GetComponent<TweenRotation>();
        SwapBars4 = SkillSlot4.GetComponent<TweenRotation>();

       CurrentHotBarID = 1;
       LoadPlayerSkillHotBarSetup();
    }

    // Update is called once per frame
    void Update()
    {
        if (GameManager.LocalPlayer == null)
            return;

        if (GameManager.LocalPlayer.InCastingMode)
        {
            if (Input.GetAxis("Mouse ScrollWheel") < -Helper.deadZone || Input.GetAxis("Mouse ScrollWheel") > Helper.deadZone)
            {
                if (Input.GetAxis("Mouse ScrollWheel") > 0)
                {
                    if (CurrentSelectedSkill == 4)
                        CurrentSelectedSkill = 1;
                    else
                        CurrentSelectedSkill++;
                }
                else
                {
                    if (CurrentSelectedSkill == 1)
                        CurrentSelectedSkill = 4;
                    else
                        CurrentSelectedSkill--;
                }

            }
        }

        if (!GameManager.LocalPlayer.IsLocked)
        {
            if (Input.GetKeyDown(KeyCode.Alpha1)) // Swap To Hot Bar 1
            {
                CurrentHotBarID = 1;

                LoadPlayerSkillHotBarSetup();
            }

            if (Input.GetKeyDown(KeyCode.Alpha2)) // Swap To Hot Bar 2
            {
                CurrentHotBarID = 2;
                LoadPlayerSkillHotBarSetup();
            }

            if (Input.GetKeyDown(KeyCode.Alpha3)) // Swap To Hot Bar 3
            {
                CurrentHotBarID = 3;
                LoadPlayerSkillHotBarSetup();
            }
        }


        HotBarLabel.text = "Hot Bar " + CurrentHotBarID;

        ShowSelectedSkill();
    }

    public void LoadPlayerSkillHotBarSetup()
    {
        List<PlayerSkill> HotBarSkill = PlayerDataStorage.Instance.PlayerSkills.Where(w => w.HotBarId == CurrentHotBarID).ToList();

        for (int i = 1; i < 5; i++)
        {
            UISkillIcon SetupSkill = null;
            PlayerSkill SkillToLoad = HotBarSkill.Where(w => w.PosId == i).FirstOrDefault();


            switch (i)
            {
                case 1:
                    SetupSkill = SkillSlot1.GetComponent<UISkillIcon>();
                    SwapBars1.ResetToBeginning();
                    SwapBars1.PlayForward();

                    break;
                case 2:
                    SetupSkill = SkillSlot2.GetComponent<UISkillIcon>();
                    SwapBars2.ResetToBeginning();
                    SwapBars2.PlayForward();

                    break;
                case 3:
                    SetupSkill = SkillSlot3.GetComponent<UISkillIcon>();
                    SwapBars3.ResetToBeginning();
                    SwapBars3.PlayForward();

                    break;
                case 4:
                    SetupSkill = SkillSlot4.GetComponent<UISkillIcon>();
                    SwapBars4.ResetToBeginning();
                    SwapBars4.PlayForward();
                    break;
                default:
                    break;
            }

            if (SetupSkill != null)
            {

                if (SkillToLoad != null)
                {
                    SetupSkill.SkillData = SkillDatabase.FindByID(SkillToLoad.SkillId);
                    SetupSkill.CurrentLvl = SkillToLoad.SkillLevel;
                    SetupSkill.LoadSkillData();
                    SetupSkill.gameObject.SetActive(true);
                }
                else
                    SetupSkill.ResetToBlank();
               
            }
            else
                SetupSkill.gameObject.SetActive(false);
        }

        CurrentSelectedSkill = 1;
        ShowSelectedSkill();
        
            
    }

    public void ShowSelectedSkill()
    {
        switch (CurrentSelectedSkill)
        {
            case 1:
                Hightlight.transform.position = SkillSlot1.transform.position;
                SelectedSkill = SkillSlot1.GetComponent<UISkillIcon>();
                break;
            case 2:
                Hightlight.transform.position = SkillSlot2.transform.position;
                SelectedSkill = SkillSlot2.GetComponent<UISkillIcon>();
                break;
            case 3:
                Hightlight.transform.position = SkillSlot3.transform.position;
                SelectedSkill = SkillSlot3.GetComponent<UISkillIcon>();
                break;
            case 4:
                Hightlight.transform.position = SkillSlot4.transform.position;
                SelectedSkill = SkillSlot4.GetComponent<UISkillIcon>();
                break;
        }
    }

}
