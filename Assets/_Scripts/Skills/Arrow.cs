﻿using UnityEngine;
using System.Collections;

public class Arrow : MonoBehaviour
{
	public float forwardForce = 50;							// How much force to be applied to the forward direction of the arrow
	public float upForce = 10;								// How much force to be applied to the up direction of the arrow
	public float lifeSpan = 5;								// How long the arrow lasts (in seconds) before being destroyed

	private float _life = 0;								// Timer for arrow before being destroyed
	private Transform _t;									// Reference to gameobject's own transform


	void Start()
	{
		_t = transform;

		// Set up our life timer
		_life = lifeSpan;

		// Apply initial force
		rigidbody.AddForce(forwardForce * _t.forward);
		rigidbody.AddForce(upForce * _t.up);
	}

	void Update()
	{
		// Destory object when life runs out
		if(_life <= 0)
		{
			Destroy(gameObject);
		}
		else
		{
			_life -= Time.deltaTime;
		}
	}

	void FixedUpdate()
	{
		// Rotate the arrow so it is always facing the direction it is moving
		if(rigidbody.velocity != Vector3.zero)
		{
			rigidbody.rotation = Quaternion.LookRotation(rigidbody.velocity);
		}
	}

	void OnTriggerEnter(Collider other)
	{
		// Deal damage to enemies if colliding with them and destroy gameobject
		
	}
}