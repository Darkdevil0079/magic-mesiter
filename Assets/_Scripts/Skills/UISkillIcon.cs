﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class UISkillIcon : MonoBehaviour
{
		public UISprite SkillIcon = null;
		public UISprite SkillWipe = null;
		public UILabel SkillCooldownLable = null;
		public SkillBaseSkill SkillData { get; set; }
        public bool OnCoolDown { get; set; }

		public int CurrentLvl;

        private float CoolDownTimer;

		// Use this for initialization
		void Start ()
		{
				RemoveCoolDown ();
		}
	
		// Update is called once per frame
		void FixedUpdate ()
		{
            if (OnCoolDown)
            {

                SkillCooldownLable.text = CoolDownTimer.ToString("#.0");
                SkillWipe.fillAmount = CoolDownTimer / SkillData.CoolDown;

                CoolDownTimer -= Time.deltaTime;

                if (CoolDownTimer <= 0)
                {
                    RemoveCoolDown();
                }

            }
	            
		}

		void RemoveCoolDown ()
		{
				SkillCooldownLable.enabled = false;
				SkillWipe.fillAmount = 0;
                OnCoolDown = false;
		}

        public void StartCoolDown()
        {
            OnCoolDown = true;
            CoolDownTimer = SkillData.CoolDown;
            SkillCooldownLable.enabled = true;
        }
		
		public void LoadSkillData ()
		{
				if (SkillData != null) {
			
						SkillIcon.atlas = SkillData.iconAtlas;
						SkillIcon.spriteName = SkillData.iconName;
						SkillCooldownLable.text = SkillData.CoolDown.ToString ();			
				}
		}

        internal void AddSkillToHotBar(int SkillID)
        {

            PlayerSkill SkillToAdd = PlayerDataStorage.Instance.PlayerSkills.Where(w => w.SkillId == SkillID).FirstOrDefault();

            if (SkillToAdd != null)
            {
                SkillToAdd.HotBarId = SkillPanel.Instance.CurrentHotBarID;

                switch (name)
                {
                    case "SkillIcon1":
                        SkillToAdd.PosId = 1;
                        break;
                    case "SkillIcon2":
                        SkillToAdd.PosId = 2;
                        break;
                    case "SkillIcon3":
                        SkillToAdd.PosId = 3;
                        break;
                    case "SkillIcon4":
                        SkillToAdd.PosId = 4;
                        break;
                }
            }


            SkillPanel.Instance.LoadPlayerSkillHotBarSetup();
            SkillTree.Instance.LoadSelectedTab();

        }

        internal void ResetToBlank()
        {
            SkillIcon.spriteName = "";
        }
}
