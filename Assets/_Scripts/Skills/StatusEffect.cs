﻿using UnityEngine;
using System.Collections;

public class StatusEffect : MonoBehaviour {

    public float EffectLenght;
    public float TimeLeft = 1f;

    private TweenAlpha Remove;
    private float EndTime;
    private bool CountDown = false;

	// Use this for initialization
	void Start () {

        
	}
	
	// Update is called once per frame
	void Update () {

        if (CountDown)
        {
            if (TimeLeft < 0)
            {
                CountDown = false;
                GameManager.Instance.LocalUI.StatusIcons.Remove(this);
                GameManager.Instance.LocalUI.UpdateStatusLayout();
                DestroyImmediate(this.gameObject);
            }

            TimeLeft = EndTime - Time.time;

            Remove.to = TimeLeft / EndTime;

            
        }
        
	}

    public void SetupEffectIcon()
    {
        Remove = gameObject.AddComponent<TweenAlpha>();
        Remove.from = 1f;
        Remove.to = 1f;
        Remove.style = UITweener.Style.PingPong;
        Remove.duration = .2f;
        Remove.enabled = false;
        Remove.enabled = true;

        EndTime = Time.time + EffectLenght;
        CountDown = true;
    }
}
