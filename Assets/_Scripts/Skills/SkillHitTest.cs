using UnityEngine;
using System.Collections;

public class SkillHitTest : MonoBehaviour
{
    public bool StopMoving = false;
    public float translationSpeedX = 0f;
    public float translationSpeedY = 0f;
    public float translationSpeedZ = 0f;
    public emElementType SkillElement;
    public GameObject DestoryPrefab;

        void Start()
        {
            Destroy(gameObject, 5f);
        }

        void Update()
        {
            if (!StopMoving)
            {
                transform.Translate(new Vector3(translationSpeedX, translationSpeedY, translationSpeedZ) * Time.deltaTime);
            }
            
        }
	
		// Update is called once per frame
		void FixedUpdate ()
		{
            if (!StopMoving)
            {
                var allNearbyColliders = Physics.OverlapSphere(transform.position, 1);

                foreach (Collider current in allNearbyColliders)
                {
                    if (current.tag == "Enemy")
                    {
                        current.GetComponent<EnemyStatController>().TakeDamage(Random.Range(10 , 30) , GameManager.LocalPlayer.Player);
                        PlayDestoryEffect();
                        break;
                    }
                    if (current.tag == "ground" || current.tag == "World")
                    {
                        PlayDestoryEffect();
                        break;
                    }
                }
            }
		}

        private void PlayDestoryEffect()
        {
            StopMoving = true;
            GameObject Destory = Instantiate(DestoryPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(this.gameObject);
        }
}

