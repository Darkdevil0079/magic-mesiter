﻿using UnityEngine;
using System.Collections;

public class TimeSpawnEffect : MonoBehaviour {

    public int NumberToSpawn;
    public float Delay;
    public GameObject EffectToSpawn;
    public bool StartSpawn;
    public Vector3 SpawnOffset;


    private float NextSpawn;
    public int CountSpawn;



	// Use this for initialization
	void Start () {
        CountSpawn = 0;
	}
	
	// Update is called once per frame
	void Update () {

        if (StartSpawn)
        {
            if (Time.time > NextSpawn)
            {
                Vector3 newSpawnOffset = SpawnOffset + new Vector3(Random.Range(-10, 10), 0, Random.Range(-10, 10));

                GameObject ObjNew = Instantiate(EffectToSpawn, transform.position + newSpawnOffset, Quaternion.identity)  as GameObject;
                ObjNew.GetComponent<FallSpinEffect>().HitSpot = transform.position;
                ObjNew.GetComponent<FallSpinEffect>().StopMoving = false;
                NextSpawn = Time.time + Delay;
                CountSpawn++;
            }

            if (CountSpawn >= NumberToSpawn)
                StartSpawn = false;
        }

	}
}
