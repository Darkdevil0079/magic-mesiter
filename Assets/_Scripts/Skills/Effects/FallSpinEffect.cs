﻿using UnityEngine;
using System.Collections;

public class FallSpinEffect : MonoBehaviour {

    public Vector3 rotationsPerSecond = new Vector3(0f, 0.1f, 0f);
    public Vector3 HitSpot = new Vector3(0, 0, 0);
    public bool StopMoving = false;
    public float translationSpeed = 0f;
    public GameObject DestoryPrefab;
    public float RotationSpeed;
    public emElementType SkillElement;
    private Quaternion _lookRotation;
    private Vector3 _direction;
    private Transform mTrans;

    void Start()
    {
        mTrans = transform;
    }

    void Update()
    {
        if (!StopMoving)
        {
            _direction = (HitSpot - mTrans.position).normalized;
            _lookRotation = Quaternion.LookRotation(_direction);
            mTrans.rotation = Quaternion.Slerp(mTrans.rotation, _lookRotation, Time.deltaTime * RotationSpeed);

            transform.Translate(new Vector3(0, 0, translationSpeed) * Time.deltaTime);
        }
    }

    void FixedUpdate()
    {
        if (!StopMoving)
        {

            var allNearbyColliders = Physics.OverlapSphere(transform.position, 1);

            foreach (Collider current in allNearbyColliders)
            {
                if (current.tag == "Enemy")
                {
                    current.GetComponent<EnemyStatController>().TakeDamage(10, GameManager.LocalPlayer.Player);
                    PlayDestoryEffect();
                    break;
                }
                if (current.tag == "ground" || current.tag == "World")
                {
                    PlayDestoryEffect();
                    break;
                }
            }
        }
    }

    private void ApplyDelta(float delta)
    {
        delta *= Mathf.Rad2Deg * Mathf.PI * 2f;
        Quaternion offset = Quaternion.Euler(rotationsPerSecond * delta);
        mTrans.rotation = mTrans.rotation * offset;
    }

    private void PlayDestoryEffect()
    {
        StopMoving = true;
        if (DestoryPrefab != null)
        {
            GameObject Destory = Instantiate(DestoryPrefab, transform.position, Quaternion.identity) as GameObject;
            Destroy(this.gameObject);
        }
        
    }
}
