using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Projector))]
public class AoEMouseTarget : MonoBehaviour
{
		public Camera Camera;
		public float YOffset = 10;
		public Projector AOEProjector;

		void Start ()
		{
				AOEProjector = this.GetComponent<Projector> ();
		}

		public void SetupProjector ()
		{
				AOEProjector = this.GetComponent<Projector> ();
		}

		void Update ()
		{
		
				
		
				if (Camera == null) {
						Camera = Camera.main;

						if (Camera == null) {
								Debug.LogError ("AoETarget: Camera is null");
								return;
						}
				}

				Ray ray = Camera.ScreenPointToRay (Input.mousePosition);
				RaycastHit hit = default(RaycastHit);

				if (Physics.Raycast (ray, out hit, float.MaxValue, ~GetComponent<Projector> ().ignoreLayers)) {
						transform.position = new Vector3 (hit.point.x, hit.point.y + YOffset, hit.point.z);

						if (Input.GetMouseButtonUp (0) && GameManager.LocalPlayer.InCastingMode) {
								//PlayerSkillManager.Instance.SpawnSelectedAOESkill (transform);
						}
				}
		}
}