﻿using UnityEngine;
using System.Collections;

public class MeleeHitbox : MonoBehaviour
{
	void OnTriggerEnter(Collider other)
	{
        if (other.tag == "Player")
        {
            GameManager.LocalPlayer.MeisterTakeDamage(10);
        }
	}
}