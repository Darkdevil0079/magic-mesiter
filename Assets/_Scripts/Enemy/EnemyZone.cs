﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class EnemyZone : MonoBehaviour
{

    public GameObject EnemyToSpawn;
    public int SpawnCount;
    public string NavPathTag;
    public bool AllowSpawning;
    public float SpawnerRange = 5f;
    public List<GameObject> EnemyList;

    private float waittimer = 0f;
    private Transform spawner;

    void Start()
    {
        spawner = this.transform;
        EnemyList = new List<GameObject>();
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireCube(this.transform.position, new Vector3(SpawnerRange, 1, SpawnerRange));
    }

    void FixedUpdate()
    {
        if (AllowSpawning)
        {
            if (EnemyList.Count < SpawnCount)
            {
                if (Time.time > waittimer)
                {
                    AllowSpawning = false;
                    SpawnEnemyInZone();
                    waittimer = Time.time + 5f;
                    AllowSpawning = true;
                }
            }
            else
                AllowSpawning = false;
        }
    }

    private void SpawnEnemyInZone()
    {
        Vector3 SpawnLoc = Vector3.zero;
        bool allowSpawnLoc = false;
        int loopCount = 0;

        do
        {

            SpawnLoc = Vector3.zero;

            SpawnLoc = new Vector3(Random.Range(-SpawnerRange / 2, SpawnerRange / 2),
                                   spawner.position.y + 1f,
                                   Random.Range(-SpawnerRange / 2, SpawnerRange / 2));

            allowSpawnLoc = true;

            Collider[] HitTest = Physics.OverlapSphere(SpawnLoc, 1f);

            foreach (Collider Hit in HitTest)
            {
                if (Hit.tag != "Ground")
                {
                    if (Hit.transform.position == SpawnLoc)
                    {
                        allowSpawnLoc = false;
                        break;
                    }
                }
            }

            if (loopCount == 100)
            {
                Debug.Log("fallback loop");
                allowSpawnLoc = true;
            }
            else
                loopCount++;

        } while (!allowSpawnLoc);

        GameObject NewEnemy = Instantiate(EnemyToSpawn, Vector3.zero, Quaternion.identity) as GameObject;
        NewEnemy.transform.parent = spawner;
        NewEnemy.transform.localPosition = SpawnLoc;


        EnemyList.Add(NewEnemy);
    }
}
