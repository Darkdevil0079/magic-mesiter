using UnityEngine;
using System.Collections;
using PixelCrushers.DialogueSystem.UnityGUI;

namespace PixelCrushers.DialogueSystem.Examples {

	/// <summary>
	/// This component implements a selector that allows the player to target and use a usable 
	/// object. 
	/// 
	/// To mark an object usable, add the Usable component and a collider to it. The object's
	/// layer should be in the layer mask specified on the Selector component.
	/// 
	/// The selector can be configured to target items under the mouse cursor or the middle of
	/// the screen. When a usable object is targeted, the selector displays a targeting reticle
	/// and information about the object. If the target is in range, the inRange reticle 
	/// texture is displayed; otherwise the outOfRange texture is displayed.
	/// 
	/// If the player presses the use button (which defaults to spacebar and Fire2), the targeted
	/// object will receive an "OnUse" message.
	/// 
	/// You can hook into SelectedUsableObject and DeselectedUsableObject to get notifications
	/// when the current target has changed.
	/// </summary>
	public class Selector : MonoBehaviour {
		
		/// <summary>
		/// This class defines the textures and size of the targeting reticle.
		/// </summary>
		[System.Serializable]
		public class Reticle {
			public Texture2D inRange;
			public Texture2D outOfRange;
			public float width = 64f;
			public float height = 64f;
		}
				
		/// <summary>
		/// Specifies whether to compute range from the targeted object (distance to the camera
		/// or distance to the selector's game object).
		/// </summary>
		public enum DistanceFrom { Camera, GameObject };
		
		/// <summary>
		/// The default layermask is just the Default layer.
		/// </summary>
		private static LayerMask DefaultLayer = 1;
		
		/// <summary>
		/// The layer mask to use when targeting objects. Objects on others layers are ignored.
		/// </summary>
		public LayerMask layerMask = DefaultLayer;
				
		/// <summary>
		/// How to compute range to targeted object. Default is from the camera.
		/// </summary>
		public DistanceFrom distanceFrom = DistanceFrom.Camera;
		
		/// <summary>
		/// The max selection distance. The selector won't target objects farther than this.
		/// </summary>
		public float maxSelectionDistance = 30f;
		
		/// <summary>
		/// If <c>true</c>, uses a default OnGUI to display a selection message and
		/// targeting reticle.
		/// </summary>
		public bool useDefaultGUI = true;
		
		/// <summary>
		/// The GUI skin to use for the target's information (name and use message).
		/// </summary>
		public GUISkin guiSkin;
		
		/// <summary>
		/// The color of the information labels when the target is in range.
		/// </summary>
		public Color inRangeColor = Color.yellow;
		
		/// <summary>
		/// The color of the information labels when the target is out of range.
		/// </summary>
		public Color outOfRangeColor = Color.gray;
		
		/// <summary>
		/// The reticle images.
		/// </summary>
		public Reticle reticle;
		
		/// <summary>
		/// The key that sends an OnUse message.
		/// </summary>
		public KeyCode useKey = KeyCode.Space;
		
		/// <summary>
		/// The button that sends an OnUse message.
		/// </summary>
		public string useButton = "Fire2";
		
		/// <summary>
		/// The default use message. This can be overridden in the target's Usable component.
		/// </summary>
		public string defaultUseMessage = "(spacebar to interact)";
		
		/// <summary>
		/// If ticked, the OnUse message is broadcast to the usable object's children.
		/// </summary>
		public bool broadcastToChildren = true;
		
		/// <summary>
		/// Gets or sets the custom position used when the selectAt is set to SelectAt.CustomPosition.
		/// You can use, for example, to slide around a targeting icon onscreen using a gamepad.
		/// </summary>
		/// <value>
		/// The custom position.
		/// </value>
		public Vector3 CustomPosition { get; set; }
		
		/// <summary>
		/// Occurs when the selector has targeted a usable object.
		/// </summary>
		public event SelectedUsableObjectDelegate SelectedUsableObject = null;
		
		/// <summary>
		/// Occurs when the selector has untargeted a usable object.
		/// </summary>
		public event DeselectedUsableObjectDelegate DeselectedUsableObject = null;
		
		private GameObject selection = null;
		private Usable usable = null;
		private float distance = 0;
		private GUIStyle guiStyle = null;
        private Transform _t;
		
        void Start()
        {
            _t = this.transform;
        }

		/// <summary>
		/// Runs a raycast to see what's under the selection point. Updates the selection and
		/// calls the selection delegates if the selection has changed. If the player hits the
		/// use button, sends an OnUse message to the selection.
		/// </summary>
		void Update() {
			// Exit if disabled or paused:
			if (!enabled || (Time.timeScale <= 0)) return;
			
			// Exit if there's no camera:
			if (Camera.main == null) return;

            Collider[] hitColliders = Physics.OverlapSphere(_t.position, maxSelectionDistance);

            foreach (Collider hitItem in hitColliders)
            {
                if (hitItem.tag == "NPC")
                {
                    RaycastHit hit;
                    Physics.Linecast(hitItem.transform.position + (Vector3.up * 5f),
                                        this.GetComponent<MeisterController>().MeisterFoward.transform.position, out hit);

                    Debug.DrawLine(hitItem.transform.position + (Vector3.up * 3f),
                                        this.GetComponent<MeisterController>().MeisterFoward.transform.position);

                    if (hit.collider == null)
                    {
                        Usable hitUsable = hitItem.gameObject.GetComponent<Usable>();
                        if (hitUsable != null)
                        {
                            usable = hitUsable;
                            selection = hitItem.collider.gameObject;
                            if (SelectedUsableObject != null) SelectedUsableObject(usable);
                            GameManager.Instance.NPCSelector.gameObject.SetActive(true);
                            break;
                        }
                    }
                    else
                    {
                        DeselectTarget();
                    }
                }
                else
                    DeselectTarget();
            }

			// If the player presses the use key/button, send the OnUse message:
			if (IsUseButtonDown() && (usable != null) && (distance <= usable.maxUseDistance)) {
				if (broadcastToChildren) {
					usable.gameObject.BroadcastMessage("OnUse", this.transform, SendMessageOptions.DontRequireReceiver);
				} else {
					usable.gameObject.SendMessage("OnUse", this.transform, SendMessageOptions.DontRequireReceiver);
				}
			}
		}
		
		private void DeselectTarget() {
			if ((usable != null) && (DeselectedUsableObject != null)) DeselectedUsableObject(usable);
			usable = null;
			selection = null;

            if ( GameManager.Instance.NPCSelector != null)
                GameManager.Instance.NPCSelector.gameObject.SetActive(false);

		}
		
		private bool IsUseButtonDown() {
			return ((useKey != KeyCode.None) && Input.GetKeyDown(useKey))
				|| (!string.IsNullOrEmpty(useButton)  && Input.GetButtonUp(useButton));
		}
				
	}

}
