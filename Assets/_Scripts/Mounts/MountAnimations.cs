﻿using UnityEngine;
using System.Collections;

public class MountAnimations : MonoBehaviour
{

    public static MountAnimations Instance;

    public enum Direction
    {
        Stationary,
        Forward,
        Backward,
        Left,
        Right,
        LeftForward,
        RightForward,
        LeftBackward,
        RightBackward
    }

    public Direction MoveDirection;

    public void Awake()
    {
        Instance = this;
    }

    public void DetermineCurrentDirection()
    {

        var forward = false;
        var backward = false;
        var left = false;
        var right = false;

        if (MountMotor.Instance.MoveVector.z > 0)
            forward = true;
        if (MountMotor.Instance.MoveVector.z < 0)
            backward = true;
        if (MountMotor.Instance.MoveVector.x > 0)
            left = true;
        if (MountMotor.Instance.MoveVector.x < 0)
            right = true;

        if (forward)
        {
            if (left)
                MoveDirection = Direction.LeftForward;
            else if (right)
                MoveDirection = Direction.RightForward;
            else
                MoveDirection = Direction.Forward;
        }
        else if (backward)
        {
            if (left)
                MoveDirection = Direction.LeftBackward;
            else if (right)
                MoveDirection = Direction.RightBackward;
            else
                MoveDirection = Direction.Backward;
        }
        else if (left)
            MoveDirection = Direction.Left;
        else if (right)
            MoveDirection = Direction.Right;
        else
            MoveDirection = Direction.Stationary;
    }
}
