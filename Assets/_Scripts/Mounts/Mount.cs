﻿using UnityEngine;
using System.Collections;

public class Mount : MonoBehaviour
{

    public static Mount Instance;
    public static CharacterController MountController;

    public Transform mountTransform;
    public Transform MountPoint;


    void Awake()
    {
        Instance = this;
        mountTransform = this.transform;
        MountController = GetComponent<CharacterController>();
    }

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(KeyCode.M)) // Summon the Player Mount or Send away
        {
            if (GameManager.LocalPlayer.IsOnMount)
            {
                Mount.Instance.GetOffMount();
            }
        }

        if (GameManager.LocalPlayer.IsOnMount)
        {
            GetLocomotionInput();
            MountMotor.Instance.UpdateMotor();
        }
        else
        {
            if (GameManager.LocalPlayer.IsMountSummoned)
            {
                if (Vector3.Distance(mountTransform.position, GameManager.LocalPlayer.Player.position) > 3f)
                    mountTransform.position = Vector3.Lerp(mountTransform.position, GameManager.LocalPlayer.Player.position, 1f * Time.deltaTime);
                else
                    GetOnMount();
            }           
        }
    }

    private void GetLocomotionInput()
    {
        var deadZone = 0.1f;

        MountMotor.Instance.VerticalVelocity = MountMotor.Instance.MoveVector.y;
        MountMotor.Instance.MoveVector = Vector3.zero;

        if (Input.GetAxis("Vertical") > deadZone || Input.GetAxis("Vertical") < -deadZone)
        {
            MountMotor.Instance.MoveVector += new Vector3(0, 0, Input.GetAxis("Vertical"));
        }

        if (Input.GetButtonUp("Horizontal"))
            MountMotor.Instance.MountTurn = Turn.none;
        else
        {
            if (Input.GetButton("Horizontal"))
            {
                if (Input.GetAxis("Horizontal") > 0)
                    MountMotor.Instance.MountTurn = Turn.right;
                else
                    MountMotor.Instance.MountTurn = Turn.left;
            }
        }

        MountAnimations.Instance.DetermineCurrentDirection();
    }

    private void SendMountAway()
    {
        Mount.Instance.mountTransform.position = Vector3.Lerp(Mount.Instance.mountTransform.position, GameManager.LocalPlayer.Player.position, 1f * Time.deltaTime);
    }

    public void GetOnMount()
    {
        Debug.Log("Get On");
        GameManager.LocalPlayer.IsOnMount = true;
        GameManager.LocalPlayer.Player.parent = Mount.Instance.MountPoint;
        GameManager.LocalPlayer.Player.localPosition = Vector3.zero;
        GameManager.LocalPlayer.Player.localRotation = Quaternion.identity;
    }

    public void GetOffMount()
    {
        Debug.Log("Get Off");

        GameManager.LocalPlayer.Player.parent = null;
        GameManager.LocalPlayer.Player.localPosition = mountTransform.localPosition;
        Destroy(Mount.Instance.gameObject);
        GameManager.LocalPlayer.IsMountSummoned = false;
        GameManager.LocalPlayer.IsOnMount = false;

    }
}
