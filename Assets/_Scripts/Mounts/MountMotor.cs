﻿using UnityEngine;
using System.Collections;

public class MountMotor : MonoBehaviour {

    public static MountMotor Instance;

    public float ForwardSpeed = 40f;
    public float BackwardSpeed = 2f;
    public float JumpSpeed = 6f;
    public float RotationSpeed = 140f;

    public Turn MountTurn { get; set; }

    public float Gravity = 21f;
    public float TerminalVelocity = 20f;

    public Vector3 MoveVector { get; set; }
    public float VerticalVelocity;

    void Awake()
    {
        Instance = this;
    }

    public void UpdateMotor()
    {
        ProcessMotion();
    }

    void ProcessMotion()
    {
        if (MountTurn != Turn.none)
            Mount.Instance.mountTransform.Rotate(0, (int)MountTurn * Time.deltaTime * RotationSpeed, 0);

        // Transform MoveVector to Worlds Space
        MoveVector = transform.TransformDirection(MoveVector);

        // Normalize MoveVector if Magnitude > 1
        if (MoveVector.magnitude > 1)
            MoveVector = Vector3.Normalize(MoveVector);

        // Multiply MoveVector by MoveSpeed

        MoveVector *= MoveSpeed();

        // Reapply VerticalVelocity MoveVector.y
        MoveVector = new Vector3(MoveVector.x, VerticalVelocity, MoveVector.z);

        // Apply Gravity
        ApplyGravity();

        Mount.MountController.Move(MoveVector * Time.deltaTime);

    }

    void ApplyGravity()
    {
        if (MoveVector.y > -TerminalVelocity)
            MoveVector = new Vector3(MoveVector.x, MoveVector.y - Gravity * Time.deltaTime, MoveVector.z);

        if (Mount.MountController.isGrounded && MoveVector.y < -1)
            MoveVector = new Vector3(MoveVector.x, -1, MoveVector.z);
    }

    float MoveSpeed()
    {
        float Movespeed = 0f;

        switch (MountAnimations.Instance.MoveDirection)
        {
            case MountAnimations.Direction.Stationary:
                Movespeed = 0;
                break;
            case MountAnimations.Direction.Forward:
            case MountAnimations.Direction.LeftForward:
            case MountAnimations.Direction.RightForward:
                        Movespeed = ForwardSpeed;
                break;
            case MountAnimations.Direction.Backward:
            case MountAnimations.Direction.LeftBackward:
            case MountAnimations.Direction.RightBackward:
                Movespeed = BackwardSpeed;
                break;
        }

        return Movespeed;
    }
}
