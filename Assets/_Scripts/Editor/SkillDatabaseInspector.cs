using UnityEngine;
using UnityEditor;
using System.Collections.Generic;

/// <summary>
/// Inspector class used to edit Skill Databases.
/// </summary>

[CustomEditor(typeof(SkillDatabase))]
public class SkillDatabaseInspector : Editor
{
		static int mIndex = 0;

		bool mConfirmDelete = false;

		/// <summary>
		/// Draw an enlarged sprite within the specified texture atlas.
		/// </summary>

		public Rect DrawSprite (Texture2D tex, Rect sprite, Material mat)
		{
				return DrawSprite (tex, sprite, mat, true, 0);
		}

		/// <summary>
		/// Draw an enlarged sprite within the specified texture atlas.
		/// </summary>

		public Rect DrawSprite (Texture2D tex, Rect sprite, Material mat, bool addPadding)
		{
				return DrawSprite (tex, sprite, mat, addPadding, 0);
		}

		/// <summary>
		/// Draw an enlarged sprite within the specified texture atlas.
		/// </summary>

		public Rect DrawSprite (Texture2D tex, Rect sprite, Material mat, bool addPadding, int maxSize)
		{
				float paddingX = addPadding ? 4f / tex.width : 0f;
				float paddingY = addPadding ? 4f / tex.height : 0f;
				float ratio = (sprite.height + paddingY) / (sprite.width + paddingX);

				ratio *= (float)tex.height / tex.width;

				// Draw the checkered background
				Color c = GUI.color;
				Rect rect = NGUIEditorTools.DrawBackground (tex, ratio);
				GUI.color = c;

				if (maxSize > 0) {
						float dim = maxSize / Mathf.Max (rect.width, rect.height);
						rect.width *= dim;
						rect.height *= dim;
				}

				// We only want to draw into this rectangle
				if (Event.current.type == EventType.Repaint) {
						if (mat == null) {
								GUI.DrawTextureWithTexCoords (rect, tex, sprite);
						} else {
								// NOTE: DrawPreviewTexture doesn't seem to support BeginGroup-based clipping
								// when a custom material is specified. It seems to be a bug in Unity.
								// Passing 'null' for the material or omitting the parameter clips as expected.
								UnityEditor.EditorGUI.DrawPreviewTexture (sprite, tex, mat);
								//UnityEditor.EditorGUI.DrawPreviewTexture(drawRect, tex);
								//GUI.DrawTexture(drawRect, tex);
						}
						rect = new Rect (sprite.x + rect.x, sprite.y + rect.y, sprite.width, sprite.height);
				}
				return rect;
		}

		/// <summary>
		/// Helper function that sets the index to the index of the specified item.
		/// </summary>

		public static void SelectIndex (SkillDatabase db, SkillBaseSkill item)
		{
				mIndex = 0;

				foreach (SkillBaseSkill i in db.items) {
						if (i == item)
								break;
						++mIndex;
				}
		}

		/// <summary>
		/// Draw the inspector widget.
		/// </summary>

		public override void OnInspectorGUI ()
        {
				NGUIEditorTools.SetLabelWidth (80f);
                
				SkillDatabase db = target as SkillDatabase;
				NGUIEditorTools.DrawSeparator ();

				SkillBaseSkill item = null;

				if (db.items == null || db.items.Count == 0) {
						mIndex = 0;
				} else {
						mIndex = Mathf.Clamp (mIndex, 0, db.items.Count - 1);
						item = db.items [mIndex];
				}

				if (mConfirmDelete) {
						// Show the confirmation dialog
						GUILayout.Label ("Are you sure you want to delete '" + item.name + "'?");
						NGUIEditorTools.DrawSeparator ();

						GUILayout.BeginHorizontal ();
						{
								GUI.backgroundColor = Color.green;
								if (GUILayout.Button ("Cancel"))
										mConfirmDelete = false;
								GUI.backgroundColor = Color.red;

								if (GUILayout.Button ("Delete")) {
										NGUIEditorTools.RegisterUndo ("Delete Skill", db);
										db.items.RemoveAt (mIndex);
										mConfirmDelete = false;
								}
								GUI.backgroundColor = Color.white;
						}
						GUILayout.EndHorizontal ();
				} else {
				
						// Database icon atlas
						UIAtlas atlas = EditorGUILayout.ObjectField ("Icon Atlas", db.iconAtlas, typeof(UIAtlas), false) as UIAtlas;

						if (atlas != db.iconAtlas) {
								NGUIEditorTools.RegisterUndo ("Databse Atlas change", db);
								db.iconAtlas = atlas;
								foreach (SkillBaseSkill i in db.items)
										i.iconAtlas = atlas;
						}

						// Database ID
						int dbID = EditorGUILayout.IntField ("Database ID", db.databaseID);

						if (dbID != db.databaseID) {
								NGUIEditorTools.RegisterUndo ("Database ID change", db);
								db.databaseID = dbID;
						}

						// "New" button
						GUI.backgroundColor = Color.green;

						if (GUILayout.Button ("New Skill")) {
								NGUIEditorTools.RegisterUndo ("Add Skill", db);

								SkillBaseSkill bi = new SkillBaseSkill ();
								bi.iconAtlas = db.iconAtlas;
								bi.id16 = (db.items.Count > 0) ? db.items [db.items.Count - 1].id16 + 1 : 0;
								db.items.Add (bi);
								mIndex = db.items.Count - 1;

								if (item != null) {
										bi.name = "Copy of " + item.name;
										bi.description = item.description;
                                        bi.unLockedLevel = item.unLockedLevel;
										bi.tier = item.tier;
                                        bi.tierPlaceID = item.tierPlaceID;
										bi.isStartingSkill = item.isStartingSkill;
										bi.iconName = item.iconName;
										bi.skillType = item.skillType;
										bi.skillElement = item.skillElement;
                                        bi.skillEffectType = item.skillEffectType;
										bi.skillAnimationType = item.skillAnimationType;
										bi.skillMinLevel = item.skillMinLevel;
										bi.skillMaxLevel = item.skillMaxLevel;
										bi.Casttime = item.Casttime;
										bi.CoolDown = item.CoolDown;	
										bi.damageLevel1 = item.damageLevel1;
										bi.damageLevel2 = item.damageLevel2;
										bi.damageLevel3 = item.damageLevel3;
										bi.damageLevel4 = item.damageLevel4;
										bi.damageLevel5 = item.damageLevel5;
										bi.manaCostLevel1 = item.manaCostLevel1;
										bi.manaCostLevel2 = item.manaCostLevel2;
										bi.manaCostLevel3 = item.manaCostLevel3;
										bi.manaCostLevel4 = item.manaCostLevel4;
										bi.manaCostLevel5 = item.manaCostLevel5;
                                        bi.skilleffect = item.skilleffect;

								} else {
										bi.name = "New Skill";
										bi.description = "Skill Description";
								}

								item = bi;
						}
						GUI.backgroundColor = Color.white;

						if (item != null) {
						
								NGUIEditorTools.DrawSeparator ();

								// Navigation section
								GUILayout.BeginHorizontal ();
								{
										if (mIndex == 0)
												GUI.color = Color.grey;
										if (GUILayout.Button ("<<")) {
												mConfirmDelete = false;
												--mIndex;
										}
										GUI.color = Color.white;
										mIndex = EditorGUILayout.IntField (mIndex + 1, GUILayout.Width (40f)) - 1;
										GUILayout.Label ("/ " + db.items.Count, GUILayout.Width (40f));
										if (mIndex + 1 == db.items.Count)
												GUI.color = Color.grey;
										if (GUILayout.Button (">>")) {
												mConfirmDelete = false;
												++mIndex;
										}
										GUI.color = Color.white;
								}
								GUILayout.EndHorizontal ();

								NGUIEditorTools.DrawSeparator ();

								// Item name and delete item button
								GUILayout.BeginHorizontal ();
								{
										string itemName = EditorGUILayout.TextField ("Skill Name", item.name);

										GUI.backgroundColor = Color.red;

										if (GUILayout.Button ("Delete", GUILayout.Width (55f))) {
												mConfirmDelete = true;
										}
										GUI.backgroundColor = Color.white;

										if (!itemName.Equals (item.name)) {
												NGUIEditorTools.RegisterUndo ("Rename Skill", db);
												item.name = itemName;
										}
								}
								GUILayout.EndHorizontal ();

                                if (GUILayout.Button("Get Clipboard Text", GUILayout.Width(130f)))
                                {
                                    item.description = ClipboardHelper.clipBoard;
                                }
								string itemDesc = GUILayout.TextField (item.description, 200, GUILayout.Height (100f));
                                
                                
								GUILayout.Space (5f);
								SkillBaseSkill.emCastAnimationType animationtype = (SkillBaseSkill.emCastAnimationType)EditorGUILayout.EnumPopup ("Anim Type", item.skillAnimationType);
								emElementType skillelement = (emElementType)EditorGUILayout.EnumPopup ("Element Type", item.skillElement);
								SkillBaseSkill.emSkillType skilltype = (SkillBaseSkill.emSkillType)EditorGUILayout.EnumPopup ("Skill Type", item.skillType);
                                SkillBaseSkill.emSkillEffectType skilleffecttype = (SkillBaseSkill.emSkillEffectType)EditorGUILayout.EnumPopup("Skill Effect", item.skillEffectType);

								GUILayout.Space (5f);

								int unlockLevel = EditorGUILayout.IntField ("Skill Unlock Level " ,item.unLockedLevel, GUILayout.MinWidth (180f));
                                int tier = EditorGUILayout.IntField("Skill Tier", item.tier, GUILayout.MaxWidth(120f));
                                int tierPlaceId = EditorGUILayout.IntField("Tier Place ID", item.tierPlaceID, GUILayout.MaxWidth(120f));
								float casttime = EditorGUILayout.FloatField ("Cast Time", item.Casttime, GUILayout.MaxWidth (120f));
								float coolDown = EditorGUILayout.FloatField ("Cooldown", item.CoolDown, GUILayout.MaxWidth (120f));

								GUILayout.Label ("Damage Level Range", GUILayout.Width (140f));
								GUILayout.Space (5f);
								GUILayout.BeginHorizontal ();
				
								float damlvl1 = EditorGUILayout.FloatField (item.damageLevel1, GUILayout.Width (40f));
                                float damlvl2 = EditorGUILayout.FloatField(item.damageLevel2, GUILayout.Width(40f));
                                float damlvl3 = EditorGUILayout.FloatField(item.damageLevel3, GUILayout.Width(40f));
                                float damlvl4 = EditorGUILayout.FloatField(item.damageLevel4, GUILayout.Width(40f));
                                float damlvl5 = EditorGUILayout.FloatField(item.damageLevel5, GUILayout.Width(40f));
								GUILayout.EndHorizontal ();
								GUILayout.Space (5f);
								GUILayout.Label ("Mana Cost Range", GUILayout.Width (140f));
								GUILayout.Space (5f);
								GUILayout.BeginHorizontal ();
								int manacst1 = EditorGUILayout.IntField (item.manaCostLevel1, GUILayout.Width (40f));
								int manacst2 = EditorGUILayout.IntField (item.manaCostLevel2, GUILayout.Width (40f));
								int manacst3 = EditorGUILayout.IntField (item.manaCostLevel3, GUILayout.Width (40f));
								int manacst4 = EditorGUILayout.IntField (item.manaCostLevel4, GUILayout.Width (40f));
								int manacst5 = EditorGUILayout.IntField (item.manaCostLevel5, GUILayout.Width (40f));
								GUILayout.EndHorizontal ();
								GUILayout.Space (5f);

                                //int skillunloadlevel = EditorGUILayout.IntField("Unlock Level", item.unLockedLevel, GUILayout.MaxWidth(120f));
                                GameObject go = (GameObject)EditorGUILayout.ObjectField("Effect", item.skilleffect, typeof(GameObject), false);


								string iconName = "";
								float iconSize = 64f;
								bool drawIcon = false;
								float extraSpace = 0f;

								if (item.iconAtlas != null) {
										BetterList<string> sprites = item.iconAtlas.GetListOfSprites ();
										sprites.Insert (0, "<None>");

										int index = 0;
										string spriteName = (item.iconName != null) ? item.iconName : sprites [0];

										// We need to find the sprite in order to have it selected
										if (!string.IsNullOrEmpty (spriteName)) {
												for (int i = 1; i < sprites.size; ++i) {
														if (spriteName.Equals (sprites [i], System.StringComparison.OrdinalIgnoreCase)) {
																index = i;
																break;
														}
												}
										}

										// Draw the sprite selection popup
										index = EditorGUILayout.Popup ("Icon", index, sprites.ToArray ());
										UISpriteData sprite = (index > 0) ? item.iconAtlas.GetSprite (sprites [index]) : null;

										if (sprite != null) {
												iconName = sprite.name;

												Material mat = item.iconAtlas.spriteMaterial;

												if (mat != null) {
														Texture2D tex = mat.mainTexture as Texture2D;

														if (tex != null) {
																drawIcon = true;
																Rect rect = new Rect (sprite.x, sprite.y, sprite.width, sprite.height);
																rect = NGUIMath.ConvertToTexCoords (rect, tex.width, tex.height);

																GUILayout.Space (4f);
																GUILayout.BeginHorizontal ();
																{
																		GUILayout.Space (Screen.width - iconSize);
																		DrawSprite (tex, rect, null, false);
																}
																GUILayout.EndHorizontal ();

																extraSpace = iconSize * (float)sprite.height / sprite.width;
														}
												}
										}
								}

								
								
								// Item level range
								GUILayout.BeginHorizontal ();
								GUILayout.Label ("Level Range", GUILayout.Width (77f));
								int min = EditorGUILayout.IntField (item.skillMinLevel, GUILayout.MinWidth (40f));
								int max = EditorGUILayout.IntField (item.skillMaxLevel, GUILayout.MinWidth (40f));


								if (drawIcon)
										GUILayout.Space (iconSize);
								GUILayout.EndHorizontal ();

								// Color tint field, left of the icon
								GUILayout.BeginHorizontal ();
								if (drawIcon)
										GUILayout.Space (iconSize);
								GUILayout.EndHorizontal ();

								// Calculate the extra spacing necessary for the icon to show up properly and not overlap anything
								if (drawIcon) {
										extraSpace = Mathf.Max (0f, extraSpace - 60f);
										GUILayout.Space (extraSpace);
								}


								GUILayout.Space (30f);

								// Save all values
								if (!itemDesc.Equals (item.description) ||
										tier != item.tier ||
                                        tierPlaceId != item.tierPlaceID ||
										unlockLevel != item.unLockedLevel ||
										skilltype != item.skillType ||
										min != item.skillMinLevel ||
										max != item.skillMaxLevel ||
										skillelement != item.skillElement ||
										animationtype != item.skillAnimationType ||
										casttime != item.Casttime ||
										coolDown != item.CoolDown ||
										damlvl1 != item.damageLevel1 ||
										damlvl2 != item.damageLevel2 ||
										damlvl3 != item.damageLevel3 ||
										damlvl4 != item.damageLevel4 ||
										damlvl5 != item.damageLevel5 ||
										manacst1 != item.manaCostLevel1 ||
										manacst2 != item.manaCostLevel2 ||
										manacst3 != item.manaCostLevel3 ||
										manacst4 != item.manaCostLevel4 ||
										manacst5 != item.manaCostLevel5 ||
                                        go != item.skilleffect ||
                                        skilleffecttype != item.skillEffectType ||
										!iconName.Equals (item.iconName)) {
										NGUIEditorTools.RegisterUndo ("Skill Properties", db);
										
										item.description = itemDesc;
                                        item.unLockedLevel = unlockLevel;
										item.tier = tier;
                                        item.tierPlaceID = tierPlaceId;
										item.isStartingSkill = false;
										item.skillType = skilltype;
										item.skillElement = skillelement;
                                        item.skillEffectType = skilleffecttype;
										item.iconName = iconName;
										item.skillMinLevel = min;
										item.skillMaxLevel = max;
										item.skillAnimationType = animationtype;
										item.Casttime = casttime;
										item.CoolDown = coolDown;
                                        item.skilleffect = go;
										
										item.damageLevel1 = damlvl1;
										item.damageLevel2 = damlvl2;
										item.damageLevel3 = damlvl3;
										item.damageLevel4 = damlvl4;
										item.damageLevel5 = damlvl5;
										
										item.manaCostLevel1 = manacst1;
										item.manaCostLevel2 = manacst2;
										item.manaCostLevel3 = manacst3;
										item.manaCostLevel4 = manacst4;
										item.manaCostLevel5 = manacst5;
								}
						}
				}
		}
}
