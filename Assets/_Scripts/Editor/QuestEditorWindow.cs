﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class QuestEditorWindow : EditorWindow
{

    [MenuItem("QFI/Editor/Quest System")]
    static void Init()
    {
        editor = (QuestEditorWindow)EditorWindow.GetWindow(typeof(QuestEditorWindow));
        editor.scrollPosition = new Vector2();
        editor.scrollView = new Vector2(5000, 5000);
        editor.title = "Quest Editor";
        EditorApplication.SaveCurrentSceneIfUserWantsTo();

    }

    private static QuestEditorWindow editor;
    private Vector2 scrollPosition;
    private Vector2 scrollView;

    void OnGUI()
    {
        scrollPosition = GUI.BeginScrollView(new Rect(0, 0, this.position.width - 250, this.position.height), scrollPosition, new Rect(0, 0, scrollView.x, scrollView.y), true, true);
        GUILayout.BeginArea(new Rect(0, 0, scrollView.x, scrollView.y));

        //GUILayout.BeginHorizontal(EditorStyles.toolbar);
        //FileMenu();
        //GUILayout.EndHorizontal();

        GUILayout.EndArea();
        GUI.EndScrollView();

        GUILayout.BeginVertical();
        GUILayout.BeginArea(new Rect(this.position.width - 250, 0, 250, this.position.height));


        GUILayout.BeginVertical();

        SMQuestParts();
        GUILayout.BeginVertical();


        GUILayout.EndArea();
        GUILayout.EndVertical();


        switch (Event.current.type)
        {
            case EventType.mouseUp:
                
                Event.current.Use();
                break;
            case EventType.mouseDrag:
                
                break;

        }

    }

    private void SMQuestParts()
    {

        Vector2 pos = new Vector2(position.width / 2 + scrollPosition.x, position.height / 2 + scrollPosition.y);

        GUI.backgroundColor = Color.green;
        if (GUILayout.Button("Start New Quest"))
        {

        }

        GUILayout.Label("-- Quest Types --", GUILayout.Width(250f));

        GUI.backgroundColor = Color.blue;
        if (GUILayout.Button("Talk To"))
        {

        }
        if (GUILayout.Button("Kill #"))
        {

        }
        if (GUILayout.Button("Collect #"))
        {

        }
        if (GUILayout.Button("End Quest"))
        {

        }
    }

    private void FileMenu()
    {

        if (GUILayout.Button("File", EditorStyles.toolbarDropDown, GUILayout.Width(60)))
        {
            GenericMenu toolsMenu = new GenericMenu();
            Vector2 pos = new Vector2(position.width / 2 + scrollPosition.x, position.height / 2 + scrollPosition.y);

            toolsMenu.AddItem(new GUIContent("Create/Quest/New Quest"), false, null);
            toolsMenu.AddItem(new GUIContent("Save"), false, null);
            toolsMenu.AddItem(new GUIContent("Load"), false, null);

            toolsMenu.DropDown(new Rect(4, 0, 0, 16));
            EditorGUIUtility.ExitGUI();
        }

        if (GUILayout.Button("Clear", EditorStyles.toolbarDropDown, GUILayout.Width(60)))
        {
            GenericMenu toolsMenu = new GenericMenu();
            toolsMenu.AddItem(new GUIContent("All"), false, null);
            toolsMenu.AddItem(new GUIContent("Selected"), false, null);
            toolsMenu.DropDown(new Rect(64, 0, 0, 16));
            EditorGUIUtility.ExitGUI();

        }

        GUILayout.FlexibleSpace();
    }

    private void OnCreate(object data)
    {
        Debug.Log(data);

    }

}
