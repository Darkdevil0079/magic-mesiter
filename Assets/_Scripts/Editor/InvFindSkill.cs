using UnityEditor;
using UnityEngine;
using System.Collections.Generic;

/// <summary>
/// Inventory System search functionality.
/// </summary>

public class InvFindSkill : ScriptableWizard
{
		/// <summary>
		/// Private class used to return data from the Find function below.
		/// </summary>

		struct FindResult
		{
				public SkillDatabase db;
				public SkillBaseSkill item;
		}

		string mSkillName = "";
		List<FindResult> mResults = new List<FindResult> ();

		/// <summary>
		/// Add a menu option to display this wizard.
		/// </summary>

		[MenuItem("Window/Find Skill #&i")]
		static void FindItem ()
		{
				ScriptableWizard.DisplayWizard<InvFindItem> ("Find Skill");
		}

		/// <summary>
		/// Draw the custom wizard.
		/// </summary>

		void OnGUI ()
		{
				NGUIEditorTools.SetLabelWidth (80f);
				string newSkillName = EditorGUILayout.TextField ("Search for:", mSkillName);
				NGUIEditorTools.DrawSeparator ();

				if (GUI.changed || newSkillName != mSkillName) {
						mSkillName = newSkillName;

						if (string.IsNullOrEmpty (mSkillName)) {
								mResults.Clear ();
						} else {
								FindAllByName (mSkillName);
						}
				}

				if (mResults.Count == 0) {
						if (!string.IsNullOrEmpty (mSkillName)) {
								GUILayout.Label ("No matches found");
						}
				} else {
						Print3 ("Skill ID", "Skill Name", "Path", false);
						NGUIEditorTools.DrawSeparator ();

						foreach (FindResult fr in mResults) {
								if (Print3 (SkillDatabase.FindItemID (fr.item).ToString (),
					fr.item.name, NGUITools.GetHierarchy (fr.db.gameObject), true)) {
										SkillDatabaseInspector.SelectIndex (fr.db, fr.item);
										Selection.activeGameObject = fr.db.gameObject;
										EditorUtility.SetDirty (Selection.activeGameObject);
								}
						}
				}
		}

		/// <summary>
		/// Helper function used to print things in columns.
		/// </summary>

		bool Print3 (string a, string b, string c, bool button)
		{
				bool retVal = false;

				GUILayout.BeginHorizontal ();
				{
						GUILayout.Label (a, GUILayout.Width (80f));
						GUILayout.Label (b, GUILayout.Width (160f));
						GUILayout.Label (c);

						if (button) {
								retVal = GUILayout.Button ("Select", GUILayout.Width (60f));
						} else {
								GUILayout.Space (60f);
						}
				}
				GUILayout.EndHorizontal ();
				return retVal;
		}

		/// <summary>
		/// Find items by name.
		/// </summary>

		void FindAllByName (string partial)
		{
				partial = partial.ToLower ();
				mResults.Clear ();

				// Exact match comes first
				foreach (SkillDatabase db in SkillDatabase.list) {
						foreach (SkillBaseSkill item in db.items) {
								if (item.name.Equals (partial, System.StringComparison.OrdinalIgnoreCase)) {
										FindResult fr = new FindResult ();
										fr.db = db;
										fr.item = item;
										mResults.Add (fr);
								}
						}
				}

				// Next come partial matches that begin with the specified string
				foreach (SkillDatabase db in SkillDatabase.list) {
						foreach (SkillBaseSkill item in db.items) {
								if (item.name.StartsWith (partial, System.StringComparison.OrdinalIgnoreCase)) {
										bool exists = false;

										foreach (FindResult res in mResults) {
												if (res.item == item) {
														exists = true;
														break;
												}
										}

										if (!exists) {
												FindResult fr = new FindResult ();
												fr.db = db;
												fr.item = item;
												mResults.Add (fr);
										}
								}
						}
				}

				// Other partial matches come last
				foreach (SkillDatabase db in SkillDatabase.list) {
						foreach (SkillBaseSkill item in db.items) {
								if (item.name.ToLower ().Contains (partial)) {
										bool exists = false;

										foreach (FindResult res in mResults) {
												if (res.item == item) {
														exists = true;
														break;
												}
										}

										if (!exists) {
												FindResult fr = new FindResult ();
												fr.db = db;
												fr.item = item;
												mResults.Add (fr);
										}
								}
						}
				}
		}
}
