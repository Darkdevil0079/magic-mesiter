﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

public static class Helper
{
        public const float deadZone = 0.01f;

		public struct ClipPlanePoints
		{
				public Vector3 UpperLeft;
				public Vector3 UpperRight;
				public Vector3 LowerLeft;
				public Vector3 LowerRight;
		}

		public static float ClampAngle (float angle, float min, float max)
		{
				do {

						if (angle < -360)
								angle += 360;

						if (angle > 360)
								angle -= 360;



				} while (angle < -360 || angle > 360);

				return Mathf.Clamp (angle, min, max);

		}

		public static ClipPlanePoints ClipPlaneAtNear (Vector3 pos)
		{

				var clipPlanePoints = new ClipPlanePoints ();

				if (Camera.main == null)
						return clipPlanePoints;

				var transform = Camera.main.transform;
				var halfFOV = ((Camera.main.fieldOfView) / 2) * Mathf.Deg2Rad;
				var aspect = Camera.main.aspect;
				var distance = Camera.main.nearClipPlane;
				var height = distance * Mathf.Tan (halfFOV);
				var width = height * aspect;

				clipPlanePoints.LowerRight = pos + transform.right * width;
				clipPlanePoints.LowerRight -= (transform.up * height);
				clipPlanePoints.LowerRight += transform.forward * distance;

				clipPlanePoints.LowerLeft = pos - transform.right * width;
				clipPlanePoints.LowerLeft -= (transform.up * height);
				clipPlanePoints.LowerLeft += transform.forward * distance;

				clipPlanePoints.UpperRight = pos + transform.right * width;
				clipPlanePoints.UpperRight += (transform.up * height);
				clipPlanePoints.UpperRight += transform.forward * distance;

				clipPlanePoints.UpperLeft = pos - transform.right * width;
				clipPlanePoints.UpperLeft += (transform.up * height);
				clipPlanePoints.UpperLeft += transform.forward * distance;

				return clipPlanePoints;

		}

		internal static void DisplayBattleText (GameObject Holder, string displayText, emElementType element, int size)
		{
				GameObject uiRoot = GameObject.FindObjectOfType<UIRoot> ().gameObject as GameObject;
				GameObject FTPrefab = Resources.Load ("UI/FloatingText") as GameObject;
				FloatingText FT = NGUITools.AddChild (uiRoot, FTPrefab).GetComponent<FloatingText> ();
				Color TextColor = Color.black;

				if (Holder == null) {
						Debug.LogWarning ("No Battle Text Holder found");
						return;
				}

				FT.SpawnAt (Holder, size);
				FT.FollowTarget = false;
				FT.Following ();

				switch (element) {
				case emElementType.Ember:
						TextColor = NGUIText.ParseColor ("cf1212", 0);
						break;
				}

				FT.Init (displayText, TextColor);


				TweenPosition tweenPosition = FT.TweenPosition;
				TweenRotation tweenRotation = FT.TweenRotation;
				TweenColor tweenColor = FT.TweenColor;
				tweenRotation.duration = 0.2f;
				tweenRotation.from = new Vector3 (0f, 0f, 0f);
				tweenRotation.to = new Vector3 (0f, 0f, 30f);
				tweenRotation.style = UITweener.Style.Once;
				tweenPosition.duration = 1f;
				tweenPosition.from = FT.transform.localPosition + new Vector3 (1f, 1f, 1f);
				tweenPosition.to = tweenPosition.from + new Vector3 (-60f, 100f, 0f);
				tweenPosition.eventReceiver = FT.transform.gameObject;
				tweenPosition.callWhenFinished = "DestoryMe";
				tweenColor.from = FT.ElementColour;
				tweenColor.to = new Color (1f, 1f, 1f, 0f);
				tweenColor.duration = 1.3f;
		}

        internal static float GetLevelXP()
        {
            if (GameSettings.Instance != null)
            {
                return (100 * PlayerDataStorage.Instance.CurrentLevel) * GameSettings.Instance.LevelXPAmount;
            }

            return 0;
        }

}
 
public class ClipboardHelper
{
    private static PropertyInfo m_systemCopyBufferProperty = null;
    private static PropertyInfo GetSystemCopyBufferProperty()
    {
       if (m_systemCopyBufferProperty == null)
       {
         Type T = typeof(GUIUtility);
         m_systemCopyBufferProperty = T.GetProperty("systemCopyBuffer", BindingFlags.Static | BindingFlags.NonPublic);
         if (m_systemCopyBufferProperty == null)
          throw new Exception("Can't access internal member 'GUIUtility.systemCopyBuffer' it may have been removed / renamed");
       }
       return m_systemCopyBufferProperty;
    }
    public static string clipBoard
    {
       get 
       {
         PropertyInfo P = GetSystemCopyBufferProperty();
         return (string)P.GetValue(null,null);
       }
       set
       {
         PropertyInfo P = GetSystemCopyBufferProperty();
         P.SetValue(null,value,null);
       }
    }
}

