using UnityEngine;
using System.Collections;

public enum MeisterCharacterState : int
{
    Idle = 0,
    Walking = 1,
    Running = 2,
    WalkingBackwards = 3,
    Turn = 4,
    Jumping = 5,
    StrafingLeft = 6,
    StrafingRight = 7,
    Casting = 8,
    ChargeCasting = 9,
    Dead = -1,
    Falling = 10,
    Landing = 11,
    Blocking = 12,
    Swimming = 13,
    Customerizer = 14,
    Dodging = 15,
    Emote = 16,
    Talking = 17,
}

public enum Turn
{
		left = -1,
		none = 0,
		right = 1
}
	
public enum Forward
{
		back = -1,
		none = 0,
		forward = 1
}

public enum emElementType : int
{
		Ember =1,
		Frost = 2,
		Vale =3,
		Shadow =4,
		Spirt =5 ,
		None= 0
}

public enum emItemType : int
{
		Weapon = 1,
		Item = 2,
		QuestItem =3,
		Cloths=4
}

public enum emAttackType : int
{
		Melee = 1,
		Ranged = 2
}




