﻿using UnityEngine;
using System.Collections;

public class DestroyAfter : MonoBehaviour {

    public float DestoryTimer;

	// Use this for initialization
	void Start () {

        if (GetComponent<AudioSource>() != null)
        {
            this.GetComponent<AudioSource>().Play();
            Destroy(this.gameObject, GetComponent<AudioSource>().clip.length);
        }
        else
            Destroy(this.gameObject, DestoryTimer);
       
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
