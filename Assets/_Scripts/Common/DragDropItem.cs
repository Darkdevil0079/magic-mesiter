﻿using UnityEngine;
using System.Collections;
using System.Linq;

public class DragDropItem : MonoBehaviour
{

    public enum Restriction
    {
        None,
        Horizontal,
        Vertical,
        PressAndHold,
    }

    /// <summary>
    /// What kind of restriction is applied to the drag & drop logic before dragging is made possible.
    /// </summary>

    public Restriction restriction = Restriction.None;

    /// <summary>
    /// Whether a copy of the item will be dragged instead of the item itself.
    /// </summary>

    public bool cloneOnDrag = false;
    public GameObject ItemToClone;
    public object ItemData;

    #region Common functionality

    protected Transform mTrans;
    protected Transform mParent;
    protected Collider mCollider;
    protected UIRoot mRoot;
    protected UIGrid mGrid;
    protected UITable mTable;
    protected int mTouchID = int.MinValue;
    protected float mPressTime = 0f;
    protected UIDragScrollView mDragScrollView = null;

    /// <summary>
    /// Cache the transform.
    /// </summary>

    protected virtual void Start()
    {
        mTrans = transform;
        mCollider = collider;
        mDragScrollView = GetComponent<UIDragScrollView>();
    }

    /// <summary>
    /// Record the time the item was pressed on.
    /// </summary>

    void OnPress(bool isPressed) { if (isPressed) mPressTime = RealTime.time; }

    /// <summary>
    /// Start the dragging operation.
    /// </summary>

    void OnDragStart()
    {
        if (!enabled || mTouchID != int.MinValue) return;

        // If we have a restriction, check to see if its condition has been met first
        if (restriction != Restriction.None)
        {
            if (restriction == Restriction.Horizontal)
            {
                Vector2 delta = UICamera.currentTouch.totalDelta;
                if (Mathf.Abs(delta.x) < Mathf.Abs(delta.y)) return;
            }
            else if (restriction == Restriction.Vertical)
            {
                Vector2 delta = UICamera.currentTouch.totalDelta;
                if (Mathf.Abs(delta.x) > Mathf.Abs(delta.y)) return;
            }
            else if (restriction == Restriction.PressAndHold)
            {
                if (mPressTime + 1f > RealTime.time) return;
            }
        }

        if (cloneOnDrag)
        {
            GameObject clone = NGUITools.AddChild(transform.parent.gameObject, ItemToClone);
            clone.transform.localPosition = transform.localPosition;
            clone.transform.localRotation = transform.localRotation;
            clone.transform.localScale = transform.localScale;

            UIButtonColor bc = clone.GetComponent<UIButtonColor>();
            if (bc != null) bc.defaultColor = GetComponent<UIButtonColor>().defaultColor;

            UICamera.Notify(UICamera.currentTouch.pressed, "OnPress", false);

            UICamera.currentTouch.pressed = clone;
            UICamera.currentTouch.dragged = clone;

            DragDropItem item = clone.GetComponent<DragDropItem>();

            if (item == null)
                item = clone.AddComponent<DragDropItem>();

            item.Start();
            item.OnDragDropStart();
            item.ItemData = ItemData;

        }
        else OnDragDropStart();
    }

    /// <summary>
    /// Perform the dragging.
    /// </summary>

    void OnDrag(Vector2 delta)
    {
        if (!enabled || mTouchID != UICamera.currentTouchID) return;
        OnDragDropMove((Vector3)delta * mRoot.pixelSizeAdjustment);
    }

    /// <summary>
    /// Notification sent when the drag event has ended.
    /// </summary>

    void OnDragEnd()
    {
        if (!enabled || mTouchID != UICamera.currentTouchID) return;
        OnDragDropRelease(UICamera.hoveredObject);
    }

    #endregion

    /// <summary>
    /// Perform any logic related to starting the drag & drop operation.
    /// </summary>

    protected virtual void OnDragDropStart()
    {
        // Automatically disable the scroll view
        if (mDragScrollView != null) mDragScrollView.enabled = false;

        // Disable the collider so that it doesn't intercept events
        if (mCollider != null) mCollider.enabled = false;

        mTouchID = UICamera.currentTouchID;
        mParent = mTrans.parent;
        mRoot = NGUITools.FindInParents<UIRoot>(mParent);
        mGrid = NGUITools.FindInParents<UIGrid>(mParent);
        mTable = NGUITools.FindInParents<UITable>(mParent);

        // Re-parent the item
        if (UIDragDropRoot.root != null)
            mTrans.parent = UIDragDropRoot.root;

        Vector3 pos = mTrans.localPosition;
        pos.z = 0f;
        mTrans.localPosition = pos;

        TweenPosition tp = GetComponent<TweenPosition>();
        if (tp != null) tp.enabled = false;

        SpringPosition sp = GetComponent<SpringPosition>();
        if (sp != null) sp.enabled = false;

        // Notify the widgets that the parent has changed
        NGUITools.MarkParentAsChanged(gameObject);

        if (mTable != null) mTable.repositionNow = true;
        if (mGrid != null) mGrid.repositionNow = true;
    }

    /// <summary>
    /// Adjust the dragged object's position.
    /// </summary>

    protected virtual void OnDragDropMove(Vector3 delta)
    {
        mTrans.localPosition += delta;
    }

    /// <summary>
    /// Drop the item onto the specified object.
    /// </summary>

    protected virtual void OnDragDropRelease(GameObject droptarget)
    {
        if (droptarget == null)
        {
            NGUITools.Destroy(gameObject);
            return;
        }
            
        if (droptarget.GetComponent<ItemEquipmentSlot>() != null)
        {
            if (gameObject.GetComponent<DragDropItem>().ItemData.GetType() == typeof(PlayerInventory))
            {
                ItemEquipmentSlot EquipSlot = droptarget.GetComponent<ItemEquipmentSlot>();
                PlayerInventory slotItem = (PlayerInventory)gameObject.GetComponent<DragDropItem>().ItemData;
                InvBaseItem item = InvDatabase.FindByID(slotItem.ItemID);

                if (EquipSlot.SlotItem != null)
                {
                    PlayerInventory oldItem = new PlayerInventory();
                    oldItem.ItemID = EquipSlot.SlotItem.ItemID;
                    oldItem.ItemGrade = EquipSlot.SlotItem.Grade;
                    oldItem.ItemLevel = EquipSlot.SlotItem.Level;
                    oldItem.Slot = ItemStorage.LocalItemStorage.FindFirstFreeSlot();

                    ItemStorage.LocalItemStorage.AddItemToSlot(oldItem);
                }

                if (item.slot == EquipSlot.slot)
                {
                    PlayerItemEquiped itemToEquip = new PlayerItemEquiped();
                    itemToEquip.Grade = slotItem.ItemGrade;
                    itemToEquip.Level = slotItem.ItemLevel;
                    itemToEquip.Slot = item.slot;
                    itemToEquip.ItemID = slotItem.ItemID;

                    GameManager.LocalPlayer.Equipment.UpdateSlotItem(itemToEquip);
                    ItemStorage.LocalItemStorage.RemoveItemFromStorage(slotItem);

                }
                else
                    DialogBox.Instance.LoadDialogText("This item can not be added to this slot");

            }

            GameManager.LocalPlayer.SetupPlayerAppearance();
        }
        else if (droptarget.GetComponent<ItemStorageSlot>() != null)
        {

            ItemStorageSlot StorageSlot = droptarget.GetComponent<ItemStorageSlot>();

            if (gameObject.GetComponent<DragDropItem>().ItemData.GetType() == typeof(PlayerItemEquiped))
            {
                PlayerItemEquiped SlotItem = (PlayerItemEquiped)gameObject.GetComponent<DragDropItem>().ItemData;
                StorageSlot.MoveItemToStorage(SlotItem);
                GameManager.LocalPlayer.Equipment.UpdateSlotItem(SlotItem);
            }
            else
            {
                PlayerInventory SlotItem = (PlayerInventory)gameObject.GetComponent<DragDropItem>().ItemData;
                ItemStorage.LocalItemStorage.SwapStorageSlot(SlotItem , StorageSlot);
                ItemStorage.LocalItemStorage.UpdateInvGrid();
            }

            GameManager.LocalPlayer.SetupPlayerAppearance();
        }
        else if (droptarget.GetComponent<UISkillIcon>() != null)
        {
            UISkillIcon SkillLink = droptarget.GetComponent<UISkillIcon>();
            int SkillID16 = (int)gameObject.GetComponent<DragDropItem>().ItemData;

            SkillLink.AddSkillToHotBar(SkillID16);

        }

        NGUITools.Destroy(gameObject);
    }
}
