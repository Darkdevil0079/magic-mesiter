using UnityEngine;
using System.Collections;

public class CustomizerPart : MonoBehaviour
{

		public Vector3 PosOffset;
		public Vector3 RotOffset;
		public Vector3 ScaleOffset;

		private Transform _partTrans;

		// Use this for initialization
		void Start ()
		{
				_partTrans = this.transform;
		}

}
