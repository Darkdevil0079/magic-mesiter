﻿using PixelCrushers.DialogueSystem;
using UnityEngine;
[System.Serializable]
public class PlayerInventory
{

    public int Slot;
    public int ItemID;
    public int ItemLevel;
    public InvGameItem.Quality ItemGrade;
    public int Qty;

}

[System.Serializable]
public class PlayerItemEquiped
{
    public int ItemID;
    public InvBaseItem.Slot Slot;
    public InvGameItem.Quality Grade;
    public int Level;

}

[System.Serializable]
public class PlayerSkill
{

    public int SkillId;
    public int SkillLevel;
    public int HotBarId;
    public int PosId;

}

[System.Serializable]
public class PlayerQuest
{
    public int QuestID;
    public string Name;
    public string questSource;
    public string Description;
    public string[] Entries;
    public QuestState[] EntryStates;
    public bool Tracking;
    public bool Available;
    public bool Abandonable;


}


