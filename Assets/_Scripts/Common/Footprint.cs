﻿using UnityEngine;
using System.Collections;

public class Footprint : MonoBehaviour {

    public float DestoryTimer = 5f;
    public bool TurnOnFlame = false;

    public GameObject FlameEffect;


    void Start()
    {
        Destroy(this.gameObject, DestoryTimer);
    }

    void Update()
    {
        FlameEffect.SetActive(TurnOnFlame);
    }

    void OnDestroy()
    {
        GameManager.LocalPlayer.Skills.PlayerFootPrints.Remove(this.gameObject);
    }
}
